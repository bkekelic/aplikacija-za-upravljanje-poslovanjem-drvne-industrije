﻿namespace Zavrsin_rad_app
{
    partial class Form_Administrator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Administrator));
            this.pnl_Head = new System.Windows.Forms.Panel();
            this.pb_iconMain = new System.Windows.Forms.PictureBox();
            this.pb_logOut = new System.Windows.Forms.PictureBox();
            this.pb_minimize = new System.Windows.Forms.PictureBox();
            this.pb_exit = new System.Windows.Forms.PictureBox();
            this.lb_naslov = new System.Windows.Forms.Label();
            this.lb_info = new System.Windows.Forms.Label();
            this.pnl_Izbornik_Direktor = new System.Windows.Forms.Panel();
            this.btn_IZB_prikaziRadniNalog_direktor = new System.Windows.Forms.Button();
            this.btn_IZB_napisiRadniNalog_direktor = new System.Windows.Forms.Button();
            this.btn_IZB_pretraziPopratnice_direktor = new System.Windows.Forms.Button();
            this.btn_IZB_pretraziSkladiste_direktor = new System.Windows.Forms.Button();
            this.btn_IZB_prikaziStatistikuTPT_direktor = new System.Windows.Forms.Button();
            this.btn_IZB_izmjeniPodatkeOZaposlenima = new System.Windows.Forms.Button();
            this.btn_kreirajNovogZaposlenika = new System.Windows.Forms.Button();
            this.pnl_RP_kreirajNovogZaposlenika = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_KorRacun = new System.Windows.Forms.GroupBox();
            this.tb_lozinka = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_generirajLozinku = new System.Windows.Forms.Button();
            this.tb_korIme = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_spremiNovogZaposlenika = new System.Windows.Forms.Button();
            this.gb_PoslovniDetalji = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbox_pozicijaRada = new System.Windows.Forms.ComboBox();
            this.gb_spol = new System.Windows.Forms.GroupBox();
            this.rb_zensko = new System.Windows.Forms.RadioButton();
            this.rb_musko = new System.Windows.Forms.RadioButton();
            this.gb_dodatneInfo = new System.Windows.Forms.GroupBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_adresa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_mob = new System.Windows.Forms.TextBox();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gb_osobniPodaci = new System.Windows.Forms.GroupBox();
            this.tb_ime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_OIB = new System.Windows.Forms.TextBox();
            this.tb_prezime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnl_RP_upisiNoviKamion = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_spremiNoviKamion = new System.Windows.Forms.Button();
            this.gb_podaciOVozilu = new System.Windows.Forms.GroupBox();
            this.tb_oibVozača = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_prezimeVozača = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_regOznakaKamiona = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_imeVozača = new System.Windows.Forms.TextBox();
            this.tb_regOznakaPrikolice = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.pnl_RP_prikazTrenutnihKamiona = new System.Windows.Forms.Panel();
            this.btn_odlazakKamiona = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gb_trenutniKamioni = new System.Windows.Forms.GroupBox();
            this.dgw_trenutniKamioni = new System.Windows.Forms.DataGridView();
            this.pnl_RP_prikaziOstaleKamione = new System.Windows.Forms.Panel();
            this.gb_kamioniPretraziPo = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbox_pretraziPo = new System.Windows.Forms.ComboBox();
            this.tb_kamioniPretraziPo = new System.Windows.Forms.TextBox();
            this.dgv_ostaliKamioni = new System.Windows.Forms.DataGridView();
            this.btn_pretraziOstaleKamione = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gb_opcijePrikaziOstaleKamione = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.datePicker_up = new System.Windows.Forms.DateTimePicker();
            this.datePicker_down = new System.Windows.Forms.DateTimePicker();
            this.pnl_Izbornik_Portir = new System.Windows.Forms.Panel();
            this.btn_IZB_prikaziOstaleKamione = new System.Windows.Forms.Button();
            this.btn_IZB_PrikaziTrenutneKamione = new System.Windows.Forms.Button();
            this.btn_IZB_UpisiNoviKamion = new System.Windows.Forms.Button();
            this.pnl_info = new System.Windows.Forms.Panel();
            this.lb_errorTekst = new System.Windows.Forms.Label();
            this.timerPrikaziBrojUpita = new System.Windows.Forms.Timer(this.components);
            this.pnl_Izbornik_PoslovodaPilane = new System.Windows.Forms.Panel();
            this.btn_IZB_pretragaSkladista = new System.Windows.Forms.Button();
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT = new System.Windows.Forms.Button();
            this.btn_IZB_poslovodaPrikaziRadniNalog = new System.Windows.Forms.Button();
            this.btn_IZB_NapisiRadniNalog = new System.Windows.Forms.Button();
            this.btn_IZB_UpisiKlade = new System.Windows.Forms.Button();
            this.pnl_RP_upisiKlade = new System.Windows.Forms.Panel();
            this.gb_podaciOSkaldistu = new System.Windows.Forms.GroupBox();
            this.cmbox_odaberiteSkladiste = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.gb_podaciOKladama = new System.Windows.Forms.GroupBox();
            this.dgv_podaciOKladama = new System.Windows.Forms.DataGridView();
            this.col_brPlocice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sifraDrveta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_duljinaKlade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_debljinaKlade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_obujamKlade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_spremiUpisaneKlade = new System.Windows.Forms.Button();
            this.gb_podaciOPopratnici = new System.Windows.Forms.GroupBox();
            this.tb_mjestoUtovara = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tb_sumarija = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbox_odaberiteKamion = new System.Windows.Forms.ComboBox();
            this.datePicker_datumOtpremanja = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_imePrezimeOtpremnika = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_vrijemeOtpremanja = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.pnl_RP_napisiRadniNalog = new System.Windows.Forms.Panel();
            this.gb_radniNalogHead = new System.Windows.Forms.GroupBox();
            this.tb_RadniNalogHead = new System.Windows.Forms.TextBox();
            this.gb_tekstRadnogNaloga = new System.Windows.Forms.GroupBox();
            this.tb_radniNalogTekst = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_spremiRadniNalog = new System.Windows.Forms.Button();
            this.gb_PodaciORadnomNalogu = new System.Windows.Forms.GroupBox();
            this.cmbox_nalogRadnik = new System.Windows.Forms.ComboBox();
            this.cmbox_nalogPozicijaRada = new System.Windows.Forms.ComboBox();
            this.datePicker_nalogZaDatum = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.pnl_RP_poslovodaPrikaziRadniNalog = new System.Windows.Forms.Panel();
            this.gb_RN_dodatneOpcije = new System.Windows.Forms.GroupBox();
            this.btn_poslovodaIsprintajRN = new System.Windows.Forms.Button();
            this.gb_PozicijaRadaTxt = new System.Windows.Forms.GroupBox();
            this.rtb_RadniNalogTxt = new System.Windows.Forms.RichTextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.gb_pozicijaRadaPretraziPo = new System.Windows.Forms.GroupBox();
            this.cmbox_vrijemePisanjaRN = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btn_poslovodaPrikaziRadniNalog = new System.Windows.Forms.Button();
            this.cmbox_poslovodaRNRadnik = new System.Windows.Forms.ComboBox();
            this.cmbox_poslovodaRNPozicijaRada = new System.Windows.Forms.ComboBox();
            this.datePicker_RadniNalogDatum = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.printDocument_RadniNalog = new System.Drawing.Printing.PrintDocument();
            this.printDialog_RadniNalog = new System.Windows.Forms.PrintDialog();
            this.pnl_Izbornik_ZaOneSaSamoRN = new System.Windows.Forms.Panel();
            this.btn_IZB_PrikaziRadniNalog = new System.Windows.Forms.Button();
            this.pnl_RP_PrikaziRadniNalogSAMO_RN = new System.Windows.Forms.Panel();
            this.gb_txtRadnogNaloga_SAMO_RN = new System.Windows.Forms.GroupBox();
            this.rtb_txtRN_SAMO_RN = new System.Windows.Forms.RichTextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gb_PretraziPo_RN_SAMO_RN = new System.Windows.Forms.GroupBox();
            this.btn_isprintajRN_SAMO_RN = new System.Windows.Forms.Button();
            this.cmbox_vrijemePisanjaRN_SAMO_RN = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btn_prikaziRN_SAMO_RN = new System.Windows.Forms.Button();
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            this.printDocument_SAMO_RN = new System.Drawing.Printing.PrintDocument();
            this.printDialog_SAMO_RN = new System.Windows.Forms.PrintDialog();
            this.pnl_Izbornik_PomocniRadnikPrijePilane = new System.Windows.Forms.Panel();
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane = new System.Windows.Forms.Button();
            this.btn_IZB_UpisDugackihKladaZaPiljenje = new System.Windows.Forms.Button();
            this.btn_IZB_upisiKladeKojeCeSeIzrezati = new System.Windows.Forms.Button();
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati = new System.Windows.Forms.Panel();
            this.gb_brojplocica_KladeKojeCeSeRezati = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.dgv_brojPlocicaZaRezanje = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada = new System.Windows.Forms.ComboBox();
            this.btn_upisiIzrezanePlocice = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pnl_RP_upisDugackihKladaZaPiljenje = new System.Windows.Forms.Panel();
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.gb_podaciODrugojPoloviciKlade = new System.Windows.Forms.GroupBox();
            this.tb_sifra2Klade = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tb_obujam2klade = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tb_debljina2klade = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tb_duljina2klade = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.gb_podaciOPrvojPoloviciKlade = new System.Windows.Forms.GroupBox();
            this.tb_sifra1Klade = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tb_obujam1klade = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_debljina1klade = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tb_duljina1klade = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_upisiKladuZaPiljenje = new System.Windows.Forms.Button();
            this.tb_brojPlociceZaPiljenje = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.pnl_Izbornik_MajstorNaTPT = new System.Windows.Forms.Panel();
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT = new System.Windows.Forms.Button();
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT = new System.Windows.Forms.Button();
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT = new System.Windows.Forms.Panel();
            this.lb_granice_MajstorTPT = new System.Windows.Forms.Label();
            this.chart_Statistika_MajstorNaTPT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.gb_StatistikaKubikaIKlada = new System.Windows.Forms.GroupBox();
            this.lb_IzrezanihKubika_MajstorNaTPT = new System.Windows.Forms.Label();
            this.lb_IzrezanihKlada_MajstorNaTPT = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT = new System.Windows.Forms.GroupBox();
            this.lb_odaberiteRadnika_TPT = new System.Windows.Forms.Label();
            this.cmbox_odaberiteRadnika_statistikaTPT = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.cmbox_RazdobljeStatistike_MajstorNaTPT = new System.Windows.Forms.ComboBox();
            this.btn_prikaziStatistiku_MajstorNaTPT = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pnl_Izbornik_RadnikNaViličaru = new System.Windows.Forms.Panel();
            this.btn_IZB_upisiPaketUSusaru = new System.Windows.Forms.Button();
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru = new System.Windows.Forms.Button();
            this.pnl_RP_upisiPaketUSkladiste = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_IsprintajMarkicuPaketa = new System.Windows.Forms.Button();
            this.rtb_markicaPaketa_susara = new System.Windows.Forms.RichTextBox();
            this.gb_podaciOPaketu_susara = new System.Windows.Forms.GroupBox();
            this.cmbox_klasaDrveta_paket = new System.Windows.Forms.ComboBox();
            this.cmbox_vrstaDrveta_paket = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.cmbox_odaberiteSkladiste_paket = new System.Windows.Forms.ComboBox();
            this.btn_spremiPaketUSkladiste = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tb_težinaPaketa = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.tb_količinaDasaka = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.tb_debljinaDaske = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tb_sirinaDaske = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.tb_duljinaDaske = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.printDialog_MarkicaPaketa_Susara = new System.Windows.Forms.PrintDialog();
            this.printDocument_MarkicaPaketa_Susara = new System.Drawing.Printing.PrintDocument();
            this.pnl_Izbornik_PoslovodaElementare = new System.Windows.Forms.Panel();
            this.btn_IZB_prikaziPopratnice = new System.Windows.Forms.Button();
            this.btn_IZB_pretraziSkladiste_elem = new System.Windows.Forms.Button();
            this.btn_IZB_NapisiPopratnicu = new System.Windows.Forms.Button();
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare = new System.Windows.Forms.Button();
            this.btn_IZB_NapisiRadniNalog_Elementara = new System.Windows.Forms.Button();
            this.pnl_Izbornik_RadnikNaViselisnomRezacu = new System.Windows.Forms.Panel();
            this.btn_IZB_prikaziRadniNalog_Viselisni = new System.Windows.Forms.Button();
            this.btn_IZB_upisiPaket_Elementara = new System.Windows.Forms.Button();
            this.pnl_RP_upisiPaket_RadnikNaViselisnom = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.cmbox_odaberiteSkladiste_viselisni = new System.Windows.Forms.ComboBox();
            this.btn_upisiPaket_viselisni = new System.Windows.Forms.Button();
            this.tb_brojPaketa_viselisni = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.pnl_Izbornik_RadnikSGotovomRobom = new System.Windows.Forms.Panel();
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba = new System.Windows.Forms.Button();
            this.btn_IZB_upisiPaket_GotovaRoba = new System.Windows.Forms.Button();
            this.pnl_RP_napisiPopratnicu = new System.Windows.Forms.Panel();
            this.gb_upisitePakete_popratnica = new System.Windows.Forms.GroupBox();
            this.dgv_brojPaketa_popratnica = new System.Windows.Forms.DataGridView();
            this.col_BrojPaketa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_spremiNovuPopratnicu = new System.Windows.Forms.Button();
            this.gb_izgledPopratnice_popratnica = new System.Windows.Forms.GroupBox();
            this.btn_ispisiPopratnicu = new System.Windows.Forms.Button();
            this.rtb_izgledPopratnice = new System.Windows.Forms.RichTextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.gb_podaciOPopratnici_popratnica = new System.Windows.Forms.GroupBox();
            this.tb_uplatnicaNalog_popratnica = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.tb_mjestoIstovara_popratnica = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.cmbox_odaberitePrikolicu_popratnica = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.tb_kupac_popratnica = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.tb_mjestoUtovara_popratnica = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.cmbox_odaberiteKamion_popratnica = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.printDialog_Popratnica = new System.Windows.Forms.PrintDialog();
            this.printDocument_popratnica = new System.Drawing.Printing.PrintDocument();
            this.pnl_RP_pretraziSkladiste = new System.Windows.Forms.Panel();
            this.gb_prog_rezultati = new System.Windows.Forms.GroupBox();
            this.dgv_prog_rezultati = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lb_prog_skl_spec = new System.Windows.Forms.Label();
            this.cmbox_prog_skla_spec = new System.Windows.Forms.ComboBox();
            this.lb_prog_naziv = new System.Windows.Forms.Label();
            this.btn_prog_Pretrazi = new System.Windows.Forms.Button();
            this.tb_prog_nazivi = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label67 = new System.Windows.Forms.Label();
            this.cmbox_PertraziPo_pp = new System.Windows.Forms.ComboBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.pnl_RP_izmjeniPodatkeOZaposlenima = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label78 = new System.Windows.Forms.Label();
            this.cmbox_iz_odaberiteZaposlenika = new System.Windows.Forms.ComboBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btn_iz_spremiIzmjeneKorRacuna = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label70 = new System.Windows.Forms.Label();
            this.cmbox_iz_pozRada = new System.Windows.Forms.ComboBox();
            this.gbox_iz_spol = new System.Windows.Forms.GroupBox();
            this.rb_iz_zensko = new System.Windows.Forms.RadioButton();
            this.rb_iz_musko = new System.Windows.Forms.RadioButton();
            this.gb_iz_dodatneInfo = new System.Windows.Forms.GroupBox();
            this.tb_iz_email = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.tb_iz_adresa = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.tb_iz_mob = new System.Windows.Forms.TextBox();
            this.tb_iz_tel = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.gb_iz_osobniPodaci = new System.Windows.Forms.GroupBox();
            this.tb_iz_ime = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.tb_iz_oib = new System.Windows.Forms.TextBox();
            this.tb_iz_prezime = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.pnl_RP_pretraziPopratnice = new System.Windows.Forms.Panel();
            this.rtb_prikaziPopratnice = new System.Windows.Forms.RichTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label69 = new System.Windows.Forms.Label();
            this.cmbox_prikaziPopratnicu_datumUpisa = new System.Windows.Forms.ComboBox();
            this.datePicker_prikaziPopratnicu = new System.Windows.Forms.DateTimePicker();
            this.label68 = new System.Windows.Forms.Label();
            this.btn_prikaziPopratnicu = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.pnl_Head.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_iconMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_logOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).BeginInit();
            this.pnl_Izbornik_Direktor.SuspendLayout();
            this.pnl_RP_kreirajNovogZaposlenika.SuspendLayout();
            this.gb_KorRacun.SuspendLayout();
            this.gb_PoslovniDetalji.SuspendLayout();
            this.gb_spol.SuspendLayout();
            this.gb_dodatneInfo.SuspendLayout();
            this.gb_osobniPodaci.SuspendLayout();
            this.pnl_RP_upisiNoviKamion.SuspendLayout();
            this.gb_podaciOVozilu.SuspendLayout();
            this.pnl_RP_prikazTrenutnihKamiona.SuspendLayout();
            this.gb_trenutniKamioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_trenutniKamioni)).BeginInit();
            this.pnl_RP_prikaziOstaleKamione.SuspendLayout();
            this.gb_kamioniPretraziPo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ostaliKamioni)).BeginInit();
            this.gb_opcijePrikaziOstaleKamione.SuspendLayout();
            this.pnl_Izbornik_Portir.SuspendLayout();
            this.pnl_info.SuspendLayout();
            this.pnl_Izbornik_PoslovodaPilane.SuspendLayout();
            this.pnl_RP_upisiKlade.SuspendLayout();
            this.gb_podaciOSkaldistu.SuspendLayout();
            this.gb_podaciOKladama.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_podaciOKladama)).BeginInit();
            this.gb_podaciOPopratnici.SuspendLayout();
            this.pnl_RP_napisiRadniNalog.SuspendLayout();
            this.gb_radniNalogHead.SuspendLayout();
            this.gb_tekstRadnogNaloga.SuspendLayout();
            this.gb_PodaciORadnomNalogu.SuspendLayout();
            this.pnl_RP_poslovodaPrikaziRadniNalog.SuspendLayout();
            this.gb_RN_dodatneOpcije.SuspendLayout();
            this.gb_PozicijaRadaTxt.SuspendLayout();
            this.gb_pozicijaRadaPretraziPo.SuspendLayout();
            this.pnl_Izbornik_ZaOneSaSamoRN.SuspendLayout();
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.SuspendLayout();
            this.gb_txtRadnogNaloga_SAMO_RN.SuspendLayout();
            this.gb_PretraziPo_RN_SAMO_RN.SuspendLayout();
            this.pnl_Izbornik_PomocniRadnikPrijePilane.SuspendLayout();
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.SuspendLayout();
            this.gb_brojplocica_KladeKojeCeSeRezati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_brojPlocicaZaRezanje)).BeginInit();
            this.pnl_RP_upisDugackihKladaZaPiljenje.SuspendLayout();
            this.gb_podaciODrugojPoloviciKlade.SuspendLayout();
            this.gb_podaciOPrvojPoloviciKlade.SuspendLayout();
            this.pnl_Izbornik_MajstorNaTPT.SuspendLayout();
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_Statistika_MajstorNaTPT)).BeginInit();
            this.gb_StatistikaKubikaIKlada.SuspendLayout();
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.SuspendLayout();
            this.pnl_Izbornik_RadnikNaViličaru.SuspendLayout();
            this.pnl_RP_upisiPaketUSkladiste.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gb_podaciOPaketu_susara.SuspendLayout();
            this.pnl_Izbornik_PoslovodaElementare.SuspendLayout();
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.SuspendLayout();
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.pnl_Izbornik_RadnikSGotovomRobom.SuspendLayout();
            this.pnl_RP_napisiPopratnicu.SuspendLayout();
            this.gb_upisitePakete_popratnica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_brojPaketa_popratnica)).BeginInit();
            this.gb_izgledPopratnice_popratnica.SuspendLayout();
            this.gb_podaciOPopratnici_popratnica.SuspendLayout();
            this.pnl_RP_pretraziSkladiste.SuspendLayout();
            this.gb_prog_rezultati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prog_rezultati)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.pnl_RP_izmjeniPodatkeOZaposlenima.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.gbox_iz_spol.SuspendLayout();
            this.gb_iz_dodatneInfo.SuspendLayout();
            this.gb_iz_osobniPodaci.SuspendLayout();
            this.pnl_RP_pretraziPopratnice.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_Head
            // 
            this.pnl_Head.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnl_Head.Controls.Add(this.pb_iconMain);
            this.pnl_Head.Controls.Add(this.pb_logOut);
            this.pnl_Head.Controls.Add(this.pb_minimize);
            this.pnl_Head.Controls.Add(this.pb_exit);
            this.pnl_Head.Controls.Add(this.lb_naslov);
            this.pnl_Head.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Head.Location = new System.Drawing.Point(0, 0);
            this.pnl_Head.Name = "pnl_Head";
            this.pnl_Head.Size = new System.Drawing.Size(1271, 54);
            this.pnl_Head.TabIndex = 0;
            this.pnl_Head.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_Head_MouseDown);
            this.pnl_Head.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_Head_MouseMove);
            this.pnl_Head.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_Head_MouseUp);
            // 
            // pb_iconMain
            // 
            this.pb_iconMain.Location = new System.Drawing.Point(12, 10);
            this.pb_iconMain.Name = "pb_iconMain";
            this.pb_iconMain.Size = new System.Drawing.Size(34, 34);
            this.pb_iconMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_iconMain.TabIndex = 37;
            this.pb_iconMain.TabStop = false;
            // 
            // pb_logOut
            // 
            this.pb_logOut.Location = new System.Drawing.Point(1144, 10);
            this.pb_logOut.Name = "pb_logOut";
            this.pb_logOut.Size = new System.Drawing.Size(34, 34);
            this.pb_logOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_logOut.TabIndex = 36;
            this.pb_logOut.TabStop = false;
            this.pb_logOut.Click += new System.EventHandler(this.pb_logOut_Click);
            this.pb_logOut.MouseEnter += new System.EventHandler(this.pb_logOut_MouseEnter);
            this.pb_logOut.MouseLeave += new System.EventHandler(this.pb_logOut_MouseLeave);
            // 
            // pb_minimize
            // 
            this.pb_minimize.Location = new System.Drawing.Point(1184, 10);
            this.pb_minimize.Name = "pb_minimize";
            this.pb_minimize.Size = new System.Drawing.Size(34, 34);
            this.pb_minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_minimize.TabIndex = 19;
            this.pb_minimize.TabStop = false;
            this.pb_minimize.Click += new System.EventHandler(this.pb_minimize_Click);
            this.pb_minimize.MouseEnter += new System.EventHandler(this.pb_minimize_MouseEnter);
            this.pb_minimize.MouseLeave += new System.EventHandler(this.pb_minimize_MouseLeave);
            // 
            // pb_exit
            // 
            this.pb_exit.Location = new System.Drawing.Point(1224, 10);
            this.pb_exit.Name = "pb_exit";
            this.pb_exit.Size = new System.Drawing.Size(34, 34);
            this.pb_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_exit.TabIndex = 18;
            this.pb_exit.TabStop = false;
            this.pb_exit.Click += new System.EventHandler(this.pb_exit_Click);
            this.pb_exit.MouseEnter += new System.EventHandler(this.pb_exit_MouseEnter);
            this.pb_exit.MouseLeave += new System.EventHandler(this.pb_exit_MouseLeave);
            // 
            // lb_naslov
            // 
            this.lb_naslov.AutoSize = true;
            this.lb_naslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_naslov.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(50)))));
            this.lb_naslov.Location = new System.Drawing.Point(50, 19);
            this.lb_naslov.Name = "lb_naslov";
            this.lb_naslov.Size = new System.Drawing.Size(471, 25);
            this.lb_naslov.TabIndex = 17;
            this.lb_naslov.Text = "Aplikacija za upravljanje poslovanjem drvne industrije";
            // 
            // lb_info
            // 
            this.lb_info.AutoSize = true;
            this.lb_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_info.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_info.Location = new System.Drawing.Point(20, 10);
            this.lb_info.Name = "lb_info";
            this.lb_info.Size = new System.Drawing.Size(223, 20);
            this.lb_info.TabIndex = 13;
            this.lb_info.Text = "Ime i prezime - Pozicija rada";
            // 
            // pnl_Izbornik_Direktor
            // 
            this.pnl_Izbornik_Direktor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_prikaziRadniNalog_direktor);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_napisiRadniNalog_direktor);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_pretraziPopratnice_direktor);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_pretraziSkladiste_direktor);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_prikaziStatistikuTPT_direktor);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_IZB_izmjeniPodatkeOZaposlenima);
            this.pnl_Izbornik_Direktor.Controls.Add(this.btn_kreirajNovogZaposlenika);
            this.pnl_Izbornik_Direktor.Location = new System.Drawing.Point(1, 60);
            this.pnl_Izbornik_Direktor.Name = "pnl_Izbornik_Direktor";
            this.pnl_Izbornik_Direktor.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_Direktor.TabIndex = 1;
            this.pnl_Izbornik_Direktor.Visible = false;
            // 
            // btn_IZB_prikaziRadniNalog_direktor
            // 
            this.btn_IZB_prikaziRadniNalog_direktor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziRadniNalog_direktor.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziRadniNalog_direktor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziRadniNalog_direktor.Location = new System.Drawing.Point(15, 387);
            this.btn_IZB_prikaziRadniNalog_direktor.Name = "btn_IZB_prikaziRadniNalog_direktor";
            this.btn_IZB_prikaziRadniNalog_direktor.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_prikaziRadniNalog_direktor.TabIndex = 8;
            this.btn_IZB_prikaziRadniNalog_direktor.Text = "Prikaži radni nalog";
            this.btn_IZB_prikaziRadniNalog_direktor.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziRadniNalog_direktor.Click += new System.EventHandler(this.btn_IZB_prikaziRadniNalog_direktor_Click);
            // 
            // btn_IZB_napisiRadniNalog_direktor
            // 
            this.btn_IZB_napisiRadniNalog_direktor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_napisiRadniNalog_direktor.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_napisiRadniNalog_direktor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_napisiRadniNalog_direktor.Location = new System.Drawing.Point(15, 337);
            this.btn_IZB_napisiRadniNalog_direktor.Name = "btn_IZB_napisiRadniNalog_direktor";
            this.btn_IZB_napisiRadniNalog_direktor.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_napisiRadniNalog_direktor.TabIndex = 7;
            this.btn_IZB_napisiRadniNalog_direktor.Text = "Napiši radni nalog";
            this.btn_IZB_napisiRadniNalog_direktor.UseVisualStyleBackColor = false;
            this.btn_IZB_napisiRadniNalog_direktor.Click += new System.EventHandler(this.btn_IZB_napisiRadniNalog_direktor_Click);
            // 
            // btn_IZB_pretraziPopratnice_direktor
            // 
            this.btn_IZB_pretraziPopratnice_direktor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_pretraziPopratnice_direktor.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_pretraziPopratnice_direktor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_pretraziPopratnice_direktor.Location = new System.Drawing.Point(15, 287);
            this.btn_IZB_pretraziPopratnice_direktor.Name = "btn_IZB_pretraziPopratnice_direktor";
            this.btn_IZB_pretraziPopratnice_direktor.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_pretraziPopratnice_direktor.TabIndex = 6;
            this.btn_IZB_pretraziPopratnice_direktor.Text = "Pretraži popratnice";
            this.btn_IZB_pretraziPopratnice_direktor.UseVisualStyleBackColor = false;
            this.btn_IZB_pretraziPopratnice_direktor.Click += new System.EventHandler(this.btn_IZB_pretraziPopratnice_direktor_Click);
            // 
            // btn_IZB_pretraziSkladiste_direktor
            // 
            this.btn_IZB_pretraziSkladiste_direktor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_pretraziSkladiste_direktor.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_pretraziSkladiste_direktor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_pretraziSkladiste_direktor.Location = new System.Drawing.Point(15, 236);
            this.btn_IZB_pretraziSkladiste_direktor.Name = "btn_IZB_pretraziSkladiste_direktor";
            this.btn_IZB_pretraziSkladiste_direktor.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_pretraziSkladiste_direktor.TabIndex = 5;
            this.btn_IZB_pretraziSkladiste_direktor.Text = "Pretraži skladište";
            this.btn_IZB_pretraziSkladiste_direktor.UseVisualStyleBackColor = false;
            this.btn_IZB_pretraziSkladiste_direktor.Click += new System.EventHandler(this.btn_IZB_pretraziSkladiste_direktor_Click);
            // 
            // btn_IZB_prikaziStatistikuTPT_direktor
            // 
            this.btn_IZB_prikaziStatistikuTPT_direktor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziStatistikuTPT_direktor.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziStatistikuTPT_direktor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziStatistikuTPT_direktor.Location = new System.Drawing.Point(15, 186);
            this.btn_IZB_prikaziStatistikuTPT_direktor.Name = "btn_IZB_prikaziStatistikuTPT_direktor";
            this.btn_IZB_prikaziStatistikuTPT_direktor.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_prikaziStatistikuTPT_direktor.TabIndex = 4;
            this.btn_IZB_prikaziStatistikuTPT_direktor.Text = "Prikaži statistiku majstora na TPT-u";
            this.btn_IZB_prikaziStatistikuTPT_direktor.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziStatistikuTPT_direktor.Click += new System.EventHandler(this.btn_IZB_prikaziStatistikuTPT_direktor_Click);
            // 
            // btn_IZB_izmjeniPodatkeOZaposlenima
            // 
            this.btn_IZB_izmjeniPodatkeOZaposlenima.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_izmjeniPodatkeOZaposlenima.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_izmjeniPodatkeOZaposlenima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_izmjeniPodatkeOZaposlenima.Location = new System.Drawing.Point(15, 136);
            this.btn_IZB_izmjeniPodatkeOZaposlenima.Name = "btn_IZB_izmjeniPodatkeOZaposlenima";
            this.btn_IZB_izmjeniPodatkeOZaposlenima.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_izmjeniPodatkeOZaposlenima.TabIndex = 3;
            this.btn_IZB_izmjeniPodatkeOZaposlenima.Text = "Izmijeni podatke o zaposlenima";
            this.btn_IZB_izmjeniPodatkeOZaposlenima.UseVisualStyleBackColor = false;
            this.btn_IZB_izmjeniPodatkeOZaposlenima.Click += new System.EventHandler(this.btn_IZB_izmjeniPodatkeOZaposlenima_Click);
            // 
            // btn_kreirajNovogZaposlenika
            // 
            this.btn_kreirajNovogZaposlenika.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_kreirajNovogZaposlenika.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_kreirajNovogZaposlenika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_kreirajNovogZaposlenika.Location = new System.Drawing.Point(15, 87);
            this.btn_kreirajNovogZaposlenika.Name = "btn_kreirajNovogZaposlenika";
            this.btn_kreirajNovogZaposlenika.Size = new System.Drawing.Size(285, 42);
            this.btn_kreirajNovogZaposlenika.TabIndex = 2;
            this.btn_kreirajNovogZaposlenika.Text = "Kreiraj novog zaposlenika";
            this.btn_kreirajNovogZaposlenika.UseVisualStyleBackColor = false;
            this.btn_kreirajNovogZaposlenika.Click += new System.EventHandler(this.btn_IZB_kreirajNovogZaposlenika_Click);
            // 
            // pnl_RP_kreirajNovogZaposlenika
            // 
            this.pnl_RP_kreirajNovogZaposlenika.BackColor = System.Drawing.Color.White;
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.panel1);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.gb_KorRacun);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.btn_spremiNovogZaposlenika);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.gb_PoslovniDetalji);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.gb_spol);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.gb_dodatneInfo);
            this.pnl_RP_kreirajNovogZaposlenika.Controls.Add(this.gb_osobniPodaci);
            this.pnl_RP_kreirajNovogZaposlenika.Location = new System.Drawing.Point(317, 60);
            this.pnl_RP_kreirajNovogZaposlenika.Name = "pnl_RP_kreirajNovogZaposlenika";
            this.pnl_RP_kreirajNovogZaposlenika.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_kreirajNovogZaposlenika.TabIndex = 2;
            this.pnl_RP_kreirajNovogZaposlenika.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 620);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 42);
            this.panel1.TabIndex = 3;
            // 
            // gb_KorRacun
            // 
            this.gb_KorRacun.Controls.Add(this.tb_lozinka);
            this.gb_KorRacun.Controls.Add(this.label9);
            this.gb_KorRacun.Controls.Add(this.btn_generirajLozinku);
            this.gb_KorRacun.Controls.Add(this.tb_korIme);
            this.gb_KorRacun.Controls.Add(this.label8);
            this.gb_KorRacun.Location = new System.Drawing.Point(495, 134);
            this.gb_KorRacun.Name = "gb_KorRacun";
            this.gb_KorRacun.Size = new System.Drawing.Size(441, 195);
            this.gb_KorRacun.TabIndex = 7;
            this.gb_KorRacun.TabStop = false;
            this.gb_KorRacun.Text = "Korisnički račun";
            // 
            // tb_lozinka
            // 
            this.tb_lozinka.Location = new System.Drawing.Point(157, 132);
            this.tb_lozinka.Name = "tb_lozinka";
            this.tb_lozinka.Size = new System.Drawing.Size(208, 22);
            this.tb_lozinka.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(79, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 20);
            this.label9.TabIndex = 14;
            this.label9.Text = "Lozinka:";
            // 
            // btn_generirajLozinku
            // 
            this.btn_generirajLozinku.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_generirajLozinku.Location = new System.Drawing.Point(157, 82);
            this.btn_generirajLozinku.Name = "btn_generirajLozinku";
            this.btn_generirajLozinku.Size = new System.Drawing.Size(208, 30);
            this.btn_generirajLozinku.TabIndex = 13;
            this.btn_generirajLozinku.Text = "Generiraj lozinku";
            this.btn_generirajLozinku.UseVisualStyleBackColor = true;
            this.btn_generirajLozinku.Click += new System.EventHandler(this.btn_generirajLozinku_Click);
            // 
            // tb_korIme
            // 
            this.tb_korIme.Location = new System.Drawing.Point(157, 38);
            this.tb_korIme.Name = "tb_korIme";
            this.tb_korIme.Size = new System.Drawing.Size(208, 22);
            this.tb_korIme.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 20);
            this.label8.TabIndex = 6;
            this.label8.Text = "Korisničko ime:";
            // 
            // btn_spremiNovogZaposlenika
            // 
            this.btn_spremiNovogZaposlenika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiNovogZaposlenika.Location = new System.Drawing.Point(778, 575);
            this.btn_spremiNovogZaposlenika.Name = "btn_spremiNovogZaposlenika";
            this.btn_spremiNovogZaposlenika.Size = new System.Drawing.Size(158, 30);
            this.btn_spremiNovogZaposlenika.TabIndex = 12;
            this.btn_spremiNovogZaposlenika.Text = "Spremi";
            this.btn_spremiNovogZaposlenika.UseVisualStyleBackColor = true;
            this.btn_spremiNovogZaposlenika.Click += new System.EventHandler(this.btn_spremiNovogZaposlenika_Click);
            // 
            // gb_PoslovniDetalji
            // 
            this.gb_PoslovniDetalji.Controls.Add(this.label4);
            this.gb_PoslovniDetalji.Controls.Add(this.cmbox_pozicijaRada);
            this.gb_PoslovniDetalji.Location = new System.Drawing.Point(495, 17);
            this.gb_PoslovniDetalji.Name = "gb_PoslovniDetalji";
            this.gb_PoslovniDetalji.Size = new System.Drawing.Size(441, 102);
            this.gb_PoslovniDetalji.TabIndex = 6;
            this.gb_PoslovniDetalji.TabStop = false;
            this.gb_PoslovniDetalji.Text = "Poslovni detalji";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Pozicija rada:";
            // 
            // cmbox_pozicijaRada
            // 
            this.cmbox_pozicijaRada.FormattingEnabled = true;
            this.cmbox_pozicijaRada.Location = new System.Drawing.Point(153, 41);
            this.cmbox_pozicijaRada.Name = "cmbox_pozicijaRada";
            this.cmbox_pozicijaRada.Size = new System.Drawing.Size(212, 24);
            this.cmbox_pozicijaRada.TabIndex = 0;
            // 
            // gb_spol
            // 
            this.gb_spol.Controls.Add(this.rb_zensko);
            this.gb_spol.Controls.Add(this.rb_musko);
            this.gb_spol.Location = new System.Drawing.Point(24, 441);
            this.gb_spol.Name = "gb_spol";
            this.gb_spol.Size = new System.Drawing.Size(174, 111);
            this.gb_spol.TabIndex = 11;
            this.gb_spol.TabStop = false;
            this.gb_spol.Text = "Spol";
            // 
            // rb_zensko
            // 
            this.rb_zensko.AutoSize = true;
            this.rb_zensko.Location = new System.Drawing.Point(50, 62);
            this.rb_zensko.Name = "rb_zensko";
            this.rb_zensko.Size = new System.Drawing.Size(76, 21);
            this.rb_zensko.TabIndex = 6;
            this.rb_zensko.TabStop = true;
            this.rb_zensko.Text = "Žensko";
            this.rb_zensko.UseVisualStyleBackColor = true;
            // 
            // rb_musko
            // 
            this.rb_musko.AutoSize = true;
            this.rb_musko.Checked = true;
            this.rb_musko.Location = new System.Drawing.Point(50, 35);
            this.rb_musko.Name = "rb_musko";
            this.rb_musko.Size = new System.Drawing.Size(70, 21);
            this.rb_musko.TabIndex = 5;
            this.rb_musko.TabStop = true;
            this.rb_musko.Text = "Muško";
            this.rb_musko.UseVisualStyleBackColor = true;
            // 
            // gb_dodatneInfo
            // 
            this.gb_dodatneInfo.Controls.Add(this.tb_email);
            this.gb_dodatneInfo.Controls.Add(this.label11);
            this.gb_dodatneInfo.Controls.Add(this.tb_adresa);
            this.gb_dodatneInfo.Controls.Add(this.label5);
            this.gb_dodatneInfo.Controls.Add(this.label6);
            this.gb_dodatneInfo.Controls.Add(this.tb_mob);
            this.gb_dodatneInfo.Controls.Add(this.tb_tel);
            this.gb_dodatneInfo.Controls.Add(this.label7);
            this.gb_dodatneInfo.Location = new System.Drawing.Point(24, 216);
            this.gb_dodatneInfo.Name = "gb_dodatneInfo";
            this.gb_dodatneInfo.Size = new System.Drawing.Size(452, 202);
            this.gb_dodatneInfo.TabIndex = 9;
            this.gb_dodatneInfo.TabStop = false;
            this.gb_dodatneInfo.Text = "Dodatne informacije";
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(93, 155);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(208, 22);
            this.tb_email.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(29, 155);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 20);
            this.label11.TabIndex = 6;
            this.label11.Text = "e-mail:";
            // 
            // tb_adresa
            // 
            this.tb_adresa.Location = new System.Drawing.Point(93, 41);
            this.tb_adresa.Multiline = true;
            this.tb_adresa.Name = "tb_adresa";
            this.tb_adresa.Size = new System.Drawing.Size(208, 22);
            this.tb_adresa.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Adresa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(50, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tel:";
            // 
            // tb_mob
            // 
            this.tb_mob.Location = new System.Drawing.Point(93, 117);
            this.tb_mob.Name = "tb_mob";
            this.tb_mob.Size = new System.Drawing.Size(208, 22);
            this.tb_mob.TabIndex = 5;
            // 
            // tb_tel
            // 
            this.tb_tel.Location = new System.Drawing.Point(93, 80);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(208, 22);
            this.tb_tel.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(41, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Mob:";
            // 
            // gb_osobniPodaci
            // 
            this.gb_osobniPodaci.Controls.Add(this.tb_ime);
            this.gb_osobniPodaci.Controls.Add(this.label1);
            this.gb_osobniPodaci.Controls.Add(this.label2);
            this.gb_osobniPodaci.Controls.Add(this.tb_OIB);
            this.gb_osobniPodaci.Controls.Add(this.tb_prezime);
            this.gb_osobniPodaci.Controls.Add(this.label3);
            this.gb_osobniPodaci.Location = new System.Drawing.Point(24, 17);
            this.gb_osobniPodaci.Name = "gb_osobniPodaci";
            this.gb_osobniPodaci.Size = new System.Drawing.Size(452, 175);
            this.gb_osobniPodaci.TabIndex = 8;
            this.gb_osobniPodaci.TabStop = false;
            this.gb_osobniPodaci.Text = "Osobni podatci";
            // 
            // tb_ime
            // 
            this.tb_ime.Location = new System.Drawing.Point(93, 41);
            this.tb_ime.Name = "tb_ime";
            this.tb_ime.Size = new System.Drawing.Size(208, 22);
            this.tb_ime.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prezime:";
            // 
            // tb_OIB
            // 
            this.tb_OIB.Location = new System.Drawing.Point(93, 117);
            this.tb_OIB.Name = "tb_OIB";
            this.tb_OIB.Size = new System.Drawing.Size(208, 22);
            this.tb_OIB.TabIndex = 5;
            // 
            // tb_prezime
            // 
            this.tb_prezime.Location = new System.Drawing.Point(93, 80);
            this.tb_prezime.Name = "tb_prezime";
            this.tb_prezime.Size = new System.Drawing.Size(208, 22);
            this.tb_prezime.TabIndex = 3;
            this.tb_prezime.Leave += new System.EventHandler(this.tb_prezime_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "OIB:";
            // 
            // pnl_RP_upisiNoviKamion
            // 
            this.pnl_RP_upisiNoviKamion.BackColor = System.Drawing.Color.White;
            this.pnl_RP_upisiNoviKamion.Controls.Add(this.panel3);
            this.pnl_RP_upisiNoviKamion.Controls.Add(this.btn_spremiNoviKamion);
            this.pnl_RP_upisiNoviKamion.Controls.Add(this.gb_podaciOVozilu);
            this.pnl_RP_upisiNoviKamion.Location = new System.Drawing.Point(1602, 66);
            this.pnl_RP_upisiNoviKamion.Name = "pnl_RP_upisiNoviKamion";
            this.pnl_RP_upisiNoviKamion.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_upisiNoviKamion.TabIndex = 13;
            this.pnl_RP_upisiNoviKamion.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(3, 620);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(960, 42);
            this.panel3.TabIndex = 3;
            // 
            // btn_spremiNoviKamion
            // 
            this.btn_spremiNoviKamion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiNoviKamion.Location = new System.Drawing.Point(383, 297);
            this.btn_spremiNoviKamion.Name = "btn_spremiNoviKamion";
            this.btn_spremiNoviKamion.Size = new System.Drawing.Size(158, 30);
            this.btn_spremiNoviKamion.TabIndex = 12;
            this.btn_spremiNoviKamion.Text = "Spremi";
            this.btn_spremiNoviKamion.UseVisualStyleBackColor = true;
            this.btn_spremiNoviKamion.Click += new System.EventHandler(this.btn_spremiNoviKamion_Click);
            // 
            // gb_podaciOVozilu
            // 
            this.gb_podaciOVozilu.Controls.Add(this.tb_oibVozača);
            this.gb_podaciOVozilu.Controls.Add(this.label12);
            this.gb_podaciOVozilu.Controls.Add(this.tb_prezimeVozača);
            this.gb_podaciOVozilu.Controls.Add(this.label10);
            this.gb_podaciOVozilu.Controls.Add(this.tb_regOznakaKamiona);
            this.gb_podaciOVozilu.Controls.Add(this.label18);
            this.gb_podaciOVozilu.Controls.Add(this.label19);
            this.gb_podaciOVozilu.Controls.Add(this.tb_imeVozača);
            this.gb_podaciOVozilu.Controls.Add(this.tb_regOznakaPrikolice);
            this.gb_podaciOVozilu.Controls.Add(this.label20);
            this.gb_podaciOVozilu.Location = new System.Drawing.Point(24, 17);
            this.gb_podaciOVozilu.Name = "gb_podaciOVozilu";
            this.gb_podaciOVozilu.Size = new System.Drawing.Size(517, 259);
            this.gb_podaciOVozilu.TabIndex = 8;
            this.gb_podaciOVozilu.TabStop = false;
            this.gb_podaciOVozilu.Text = "Podaci o vozilu";
            // 
            // tb_oibVozača
            // 
            this.tb_oibVozača.Location = new System.Drawing.Point(245, 205);
            this.tb_oibVozača.Name = "tb_oibVozača";
            this.tb_oibVozača.Size = new System.Drawing.Size(208, 22);
            this.tb_oibVozača.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "OIB vozača:";
            // 
            // tb_prezimeVozača
            // 
            this.tb_prezimeVozača.Location = new System.Drawing.Point(245, 160);
            this.tb_prezimeVozača.Name = "tb_prezimeVozača";
            this.tb_prezimeVozača.Size = new System.Drawing.Size(208, 22);
            this.tb_prezimeVozača.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Prezime vozača:";
            // 
            // tb_regOznakaKamiona
            // 
            this.tb_regOznakaKamiona.Location = new System.Drawing.Point(245, 39);
            this.tb_regOznakaKamiona.Name = "tb_regOznakaKamiona";
            this.tb_regOznakaKamiona.Size = new System.Drawing.Size(208, 22);
            this.tb_regOznakaKamiona.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(228, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Registarska oznaka kamiona:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 80);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(228, 20);
            this.label19.TabIndex = 2;
            this.label19.Text = "Registarska oznaka prikolice:";
            // 
            // tb_imeVozača
            // 
            this.tb_imeVozača.Location = new System.Drawing.Point(245, 119);
            this.tb_imeVozača.Name = "tb_imeVozača";
            this.tb_imeVozača.Size = new System.Drawing.Size(208, 22);
            this.tb_imeVozača.TabIndex = 5;
            // 
            // tb_regOznakaPrikolice
            // 
            this.tb_regOznakaPrikolice.Location = new System.Drawing.Point(245, 80);
            this.tb_regOznakaPrikolice.Name = "tb_regOznakaPrikolice";
            this.tb_regOznakaPrikolice.Size = new System.Drawing.Size(208, 22);
            this.tb_regOznakaPrikolice.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(11, 119);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 20);
            this.label20.TabIndex = 4;
            this.label20.Text = "Ime vozača:";
            // 
            // pnl_RP_prikazTrenutnihKamiona
            // 
            this.pnl_RP_prikazTrenutnihKamiona.BackColor = System.Drawing.Color.White;
            this.pnl_RP_prikazTrenutnihKamiona.Controls.Add(this.btn_odlazakKamiona);
            this.pnl_RP_prikazTrenutnihKamiona.Controls.Add(this.panel4);
            this.pnl_RP_prikazTrenutnihKamiona.Controls.Add(this.gb_trenutniKamioni);
            this.pnl_RP_prikazTrenutnihKamiona.Location = new System.Drawing.Point(1587, 75);
            this.pnl_RP_prikazTrenutnihKamiona.Name = "pnl_RP_prikazTrenutnihKamiona";
            this.pnl_RP_prikazTrenutnihKamiona.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_prikazTrenutnihKamiona.TabIndex = 14;
            this.pnl_RP_prikazTrenutnihKamiona.Visible = false;
            // 
            // btn_odlazakKamiona
            // 
            this.btn_odlazakKamiona.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_odlazakKamiona.Location = new System.Drawing.Point(24, 438);
            this.btn_odlazakKamiona.Name = "btn_odlazakKamiona";
            this.btn_odlazakKamiona.Size = new System.Drawing.Size(740, 42);
            this.btn_odlazakKamiona.TabIndex = 13;
            this.btn_odlazakKamiona.Text = "Unesi odlazak odabranog kamiona";
            this.btn_odlazakKamiona.UseVisualStyleBackColor = true;
            this.btn_odlazakKamiona.Click += new System.EventHandler(this.btn_odlazakKamiona_Click);
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(3, 620);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(960, 42);
            this.panel4.TabIndex = 3;
            // 
            // gb_trenutniKamioni
            // 
            this.gb_trenutniKamioni.Controls.Add(this.dgw_trenutniKamioni);
            this.gb_trenutniKamioni.Location = new System.Drawing.Point(23, 17);
            this.gb_trenutniKamioni.Name = "gb_trenutniKamioni";
            this.gb_trenutniKamioni.Size = new System.Drawing.Size(741, 400);
            this.gb_trenutniKamioni.TabIndex = 8;
            this.gb_trenutniKamioni.TabStop = false;
            this.gb_trenutniKamioni.Text = "Trenutni kamioni u firmi";
            // 
            // dgw_trenutniKamioni
            // 
            this.dgw_trenutniKamioni.AllowUserToAddRows = false;
            this.dgw_trenutniKamioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_trenutniKamioni.Location = new System.Drawing.Point(6, 35);
            this.dgw_trenutniKamioni.Name = "dgw_trenutniKamioni";
            this.dgw_trenutniKamioni.ReadOnly = true;
            this.dgw_trenutniKamioni.RowTemplate.Height = 24;
            this.dgw_trenutniKamioni.Size = new System.Drawing.Size(727, 353);
            this.dgw_trenutniKamioni.TabIndex = 0;
            // 
            // pnl_RP_prikaziOstaleKamione
            // 
            this.pnl_RP_prikaziOstaleKamione.BackColor = System.Drawing.Color.White;
            this.pnl_RP_prikaziOstaleKamione.Controls.Add(this.gb_kamioniPretraziPo);
            this.pnl_RP_prikaziOstaleKamione.Controls.Add(this.dgv_ostaliKamioni);
            this.pnl_RP_prikaziOstaleKamione.Controls.Add(this.btn_pretraziOstaleKamione);
            this.pnl_RP_prikaziOstaleKamione.Controls.Add(this.panel5);
            this.pnl_RP_prikaziOstaleKamione.Controls.Add(this.gb_opcijePrikaziOstaleKamione);
            this.pnl_RP_prikaziOstaleKamione.Location = new System.Drawing.Point(1573, 89);
            this.pnl_RP_prikaziOstaleKamione.Name = "pnl_RP_prikaziOstaleKamione";
            this.pnl_RP_prikaziOstaleKamione.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_prikaziOstaleKamione.TabIndex = 15;
            this.pnl_RP_prikaziOstaleKamione.Visible = false;
            // 
            // gb_kamioniPretraziPo
            // 
            this.gb_kamioniPretraziPo.Controls.Add(this.label13);
            this.gb_kamioniPretraziPo.Controls.Add(this.cmbox_pretraziPo);
            this.gb_kamioniPretraziPo.Controls.Add(this.tb_kamioniPretraziPo);
            this.gb_kamioniPretraziPo.Location = new System.Drawing.Point(274, 17);
            this.gb_kamioniPretraziPo.Name = "gb_kamioniPretraziPo";
            this.gb_kamioniPretraziPo.Size = new System.Drawing.Size(236, 118);
            this.gb_kamioniPretraziPo.TabIndex = 20;
            this.gb_kamioniPretraziPo.TabStop = false;
            this.gb_kamioniPretraziPo.Text = "Pretraži po:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "Vrijednost:";
            // 
            // cmbox_pretraziPo
            // 
            this.cmbox_pretraziPo.FormattingEnabled = true;
            this.cmbox_pretraziPo.Location = new System.Drawing.Point(6, 30);
            this.cmbox_pretraziPo.Name = "cmbox_pretraziPo";
            this.cmbox_pretraziPo.Size = new System.Drawing.Size(224, 24);
            this.cmbox_pretraziPo.TabIndex = 19;
            this.cmbox_pretraziPo.SelectedIndexChanged += new System.EventHandler(this.cmbox_pretraziPo_SelectedIndexChanged);
            // 
            // tb_kamioniPretraziPo
            // 
            this.tb_kamioniPretraziPo.Location = new System.Drawing.Point(101, 78);
            this.tb_kamioniPretraziPo.Name = "tb_kamioniPretraziPo";
            this.tb_kamioniPretraziPo.Size = new System.Drawing.Size(129, 22);
            this.tb_kamioniPretraziPo.TabIndex = 18;
            // 
            // dgv_ostaliKamioni
            // 
            this.dgv_ostaliKamioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ostaliKamioni.Location = new System.Drawing.Point(23, 146);
            this.dgv_ostaliKamioni.Name = "dgv_ostaliKamioni";
            this.dgv_ostaliKamioni.RowTemplate.Height = 24;
            this.dgv_ostaliKamioni.Size = new System.Drawing.Size(904, 407);
            this.dgv_ostaliKamioni.TabIndex = 19;
            // 
            // btn_pretraziOstaleKamione
            // 
            this.btn_pretraziOstaleKamione.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pretraziOstaleKamione.Location = new System.Drawing.Point(769, 103);
            this.btn_pretraziOstaleKamione.Name = "btn_pretraziOstaleKamione";
            this.btn_pretraziOstaleKamione.Size = new System.Drawing.Size(158, 30);
            this.btn_pretraziOstaleKamione.TabIndex = 16;
            this.btn_pretraziOstaleKamione.Text = "Pretraži";
            this.btn_pretraziOstaleKamione.UseVisualStyleBackColor = true;
            this.btn_pretraziOstaleKamione.Click += new System.EventHandler(this.btn_pretraziOstaleKamione_Click);
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(3, 620);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(960, 42);
            this.panel5.TabIndex = 3;
            // 
            // gb_opcijePrikaziOstaleKamione
            // 
            this.gb_opcijePrikaziOstaleKamione.Controls.Add(this.label15);
            this.gb_opcijePrikaziOstaleKamione.Controls.Add(this.label14);
            this.gb_opcijePrikaziOstaleKamione.Controls.Add(this.datePicker_up);
            this.gb_opcijePrikaziOstaleKamione.Controls.Add(this.datePicker_down);
            this.gb_opcijePrikaziOstaleKamione.Location = new System.Drawing.Point(23, 17);
            this.gb_opcijePrikaziOstaleKamione.Name = "gb_opcijePrikaziOstaleKamione";
            this.gb_opcijePrikaziOstaleKamione.Size = new System.Drawing.Size(236, 118);
            this.gb_opcijePrikaziOstaleKamione.TabIndex = 8;
            this.gb_opcijePrikaziOstaleKamione.TabStop = false;
            this.gb_opcijePrikaziOstaleKamione.Text = "Datum ulaska kamiona:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(18, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 20);
            this.label15.TabIndex = 19;
            this.label15.Text = "Do:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(18, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 20);
            this.label14.TabIndex = 18;
            this.label14.Text = "Od:";
            // 
            // datePicker_up
            // 
            this.datePicker_up.Checked = false;
            this.datePicker_up.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_up.Location = new System.Drawing.Point(60, 78);
            this.datePicker_up.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.datePicker_up.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.datePicker_up.Name = "datePicker_up";
            this.datePicker_up.ShowCheckBox = true;
            this.datePicker_up.Size = new System.Drawing.Size(151, 22);
            this.datePicker_up.TabIndex = 1;
            // 
            // datePicker_down
            // 
            this.datePicker_down.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_down.Location = new System.Drawing.Point(60, 32);
            this.datePicker_down.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.datePicker_down.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.datePicker_down.Name = "datePicker_down";
            this.datePicker_down.ShowCheckBox = true;
            this.datePicker_down.Size = new System.Drawing.Size(151, 22);
            this.datePicker_down.TabIndex = 0;
            // 
            // pnl_Izbornik_Portir
            // 
            this.pnl_Izbornik_Portir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_Portir.Controls.Add(this.btn_IZB_prikaziOstaleKamione);
            this.pnl_Izbornik_Portir.Controls.Add(this.btn_IZB_PrikaziTrenutneKamione);
            this.pnl_Izbornik_Portir.Controls.Add(this.btn_IZB_UpisiNoviKamion);
            this.pnl_Izbornik_Portir.Location = new System.Drawing.Point(1564, 98);
            this.pnl_Izbornik_Portir.Name = "pnl_Izbornik_Portir";
            this.pnl_Izbornik_Portir.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_Portir.TabIndex = 3;
            this.pnl_Izbornik_Portir.Visible = false;
            // 
            // btn_IZB_prikaziOstaleKamione
            // 
            this.btn_IZB_prikaziOstaleKamione.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziOstaleKamione.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziOstaleKamione.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziOstaleKamione.Location = new System.Drawing.Point(15, 183);
            this.btn_IZB_prikaziOstaleKamione.Name = "btn_IZB_prikaziOstaleKamione";
            this.btn_IZB_prikaziOstaleKamione.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_prikaziOstaleKamione.TabIndex = 4;
            this.btn_IZB_prikaziOstaleKamione.Text = "Prikaži ostale kamione";
            this.btn_IZB_prikaziOstaleKamione.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziOstaleKamione.Click += new System.EventHandler(this.btn_IZB_prikaziOstaleKamione_Click);
            // 
            // btn_IZB_PrikaziTrenutneKamione
            // 
            this.btn_IZB_PrikaziTrenutneKamione.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziTrenutneKamione.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziTrenutneKamione.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziTrenutneKamione.Location = new System.Drawing.Point(15, 135);
            this.btn_IZB_PrikaziTrenutneKamione.Name = "btn_IZB_PrikaziTrenutneKamione";
            this.btn_IZB_PrikaziTrenutneKamione.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziTrenutneKamione.TabIndex = 3;
            this.btn_IZB_PrikaziTrenutneKamione.Text = "Prikaži trenutne kamione";
            this.btn_IZB_PrikaziTrenutneKamione.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziTrenutneKamione.Click += new System.EventHandler(this.btn_IZB_PrikaziTrenutneKamione_Click);
            // 
            // btn_IZB_UpisiNoviKamion
            // 
            this.btn_IZB_UpisiNoviKamion.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_UpisiNoviKamion.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_UpisiNoviKamion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_UpisiNoviKamion.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_UpisiNoviKamion.Name = "btn_IZB_UpisiNoviKamion";
            this.btn_IZB_UpisiNoviKamion.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_UpisiNoviKamion.TabIndex = 2;
            this.btn_IZB_UpisiNoviKamion.Text = "Unesi dolazak kamiona";
            this.btn_IZB_UpisiNoviKamion.UseVisualStyleBackColor = false;
            this.btn_IZB_UpisiNoviKamion.Click += new System.EventHandler(this.btn_IZB_UpisiNoviKamion_Click);
            // 
            // pnl_info
            // 
            this.pnl_info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(224)))));
            this.pnl_info.Controls.Add(this.lb_errorTekst);
            this.pnl_info.Controls.Add(this.lb_info);
            this.pnl_info.Location = new System.Drawing.Point(314, 683);
            this.pnl_info.Name = "pnl_info";
            this.pnl_info.Size = new System.Drawing.Size(956, 37);
            this.pnl_info.TabIndex = 0;
            // 
            // lb_errorTekst
            // 
            this.lb_errorTekst.AutoSize = true;
            this.lb_errorTekst.BackColor = System.Drawing.Color.Red;
            this.lb_errorTekst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_errorTekst.ForeColor = System.Drawing.Color.White;
            this.lb_errorTekst.Location = new System.Drawing.Point(744, 10);
            this.lb_errorTekst.Name = "lb_errorTekst";
            this.lb_errorTekst.Size = new System.Drawing.Size(200, 20);
            this.lb_errorTekst.TabIndex = 18;
            this.lb_errorTekst.Text = "Nesupješan pokušaj upita";
            this.lb_errorTekst.Visible = false;
            this.lb_errorTekst.Click += new System.EventHandler(this.lb_errorTekst_Click);
            // 
            // timerPrikaziBrojUpita
            // 
            this.timerPrikaziBrojUpita.Interval = 10000;
            this.timerPrikaziBrojUpita.Tick += new System.EventHandler(this.timerPrikaziBrojUpita_Tick);
            // 
            // pnl_Izbornik_PoslovodaPilane
            // 
            this.pnl_Izbornik_PoslovodaPilane.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_PoslovodaPilane.Controls.Add(this.btn_IZB_pretragaSkladista);
            this.pnl_Izbornik_PoslovodaPilane.Controls.Add(this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT);
            this.pnl_Izbornik_PoslovodaPilane.Controls.Add(this.btn_IZB_poslovodaPrikaziRadniNalog);
            this.pnl_Izbornik_PoslovodaPilane.Controls.Add(this.btn_IZB_NapisiRadniNalog);
            this.pnl_Izbornik_PoslovodaPilane.Controls.Add(this.btn_IZB_UpisiKlade);
            this.pnl_Izbornik_PoslovodaPilane.Location = new System.Drawing.Point(1520, 147);
            this.pnl_Izbornik_PoslovodaPilane.Name = "pnl_Izbornik_PoslovodaPilane";
            this.pnl_Izbornik_PoslovodaPilane.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_PoslovodaPilane.TabIndex = 15;
            this.pnl_Izbornik_PoslovodaPilane.Visible = false;
            // 
            // btn_IZB_pretragaSkladista
            // 
            this.btn_IZB_pretragaSkladista.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_pretragaSkladista.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_pretragaSkladista.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_pretragaSkladista.Location = new System.Drawing.Point(15, 281);
            this.btn_IZB_pretragaSkladista.Name = "btn_IZB_pretragaSkladista";
            this.btn_IZB_pretragaSkladista.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_pretragaSkladista.TabIndex = 6;
            this.btn_IZB_pretragaSkladista.Text = "Pretraži skladište";
            this.btn_IZB_pretragaSkladista.UseVisualStyleBackColor = false;
            this.btn_IZB_pretragaSkladista.Click += new System.EventHandler(this.btn_IZB_pretragaSkladista_Click);
            // 
            // btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT
            // 
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.Location = new System.Drawing.Point(16, 232);
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.Name = "btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT";
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.TabIndex = 5;
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.Text = "Prikaži statistiku majstora na TPT-u";
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.UseVisualStyleBackColor = false;
            this.btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT.Click += new System.EventHandler(this.btn_polovodaPilane_PrikaziStatistikuMajstoraNaTPT_Click);
            // 
            // btn_IZB_poslovodaPrikaziRadniNalog
            // 
            this.btn_IZB_poslovodaPrikaziRadniNalog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_poslovodaPrikaziRadniNalog.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_poslovodaPrikaziRadniNalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_poslovodaPrikaziRadniNalog.Location = new System.Drawing.Point(15, 184);
            this.btn_IZB_poslovodaPrikaziRadniNalog.Name = "btn_IZB_poslovodaPrikaziRadniNalog";
            this.btn_IZB_poslovodaPrikaziRadniNalog.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_poslovodaPrikaziRadniNalog.TabIndex = 4;
            this.btn_IZB_poslovodaPrikaziRadniNalog.Text = "Prikaži radni nalog";
            this.btn_IZB_poslovodaPrikaziRadniNalog.UseVisualStyleBackColor = false;
            this.btn_IZB_poslovodaPrikaziRadniNalog.Click += new System.EventHandler(this.btn_IZB_poslovodaPrikaziRadniNalog_Click);
            // 
            // btn_IZB_NapisiRadniNalog
            // 
            this.btn_IZB_NapisiRadniNalog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_NapisiRadniNalog.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_NapisiRadniNalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_NapisiRadniNalog.Location = new System.Drawing.Point(15, 136);
            this.btn_IZB_NapisiRadniNalog.Name = "btn_IZB_NapisiRadniNalog";
            this.btn_IZB_NapisiRadniNalog.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_NapisiRadniNalog.TabIndex = 3;
            this.btn_IZB_NapisiRadniNalog.Text = "Napiši radni nalog";
            this.btn_IZB_NapisiRadniNalog.UseVisualStyleBackColor = false;
            this.btn_IZB_NapisiRadniNalog.Click += new System.EventHandler(this.btn_IZB_NapisiRadniNalog_Click);
            // 
            // btn_IZB_UpisiKlade
            // 
            this.btn_IZB_UpisiKlade.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_UpisiKlade.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_UpisiKlade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_UpisiKlade.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_UpisiKlade.Name = "btn_IZB_UpisiKlade";
            this.btn_IZB_UpisiKlade.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_UpisiKlade.TabIndex = 2;
            this.btn_IZB_UpisiKlade.Text = "Upiši klade";
            this.btn_IZB_UpisiKlade.UseVisualStyleBackColor = false;
            this.btn_IZB_UpisiKlade.Click += new System.EventHandler(this.btn_IZB_UpisiKlade_Click);
            // 
            // pnl_RP_upisiKlade
            // 
            this.pnl_RP_upisiKlade.BackColor = System.Drawing.Color.White;
            this.pnl_RP_upisiKlade.Controls.Add(this.gb_podaciOSkaldistu);
            this.pnl_RP_upisiKlade.Controls.Add(this.gb_podaciOKladama);
            this.pnl_RP_upisiKlade.Controls.Add(this.panel7);
            this.pnl_RP_upisiKlade.Controls.Add(this.btn_spremiUpisaneKlade);
            this.pnl_RP_upisiKlade.Controls.Add(this.gb_podaciOPopratnici);
            this.pnl_RP_upisiKlade.Location = new System.Drawing.Point(1550, 109);
            this.pnl_RP_upisiKlade.Name = "pnl_RP_upisiKlade";
            this.pnl_RP_upisiKlade.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_upisiKlade.TabIndex = 16;
            this.pnl_RP_upisiKlade.Visible = false;
            // 
            // gb_podaciOSkaldistu
            // 
            this.gb_podaciOSkaldistu.Controls.Add(this.cmbox_odaberiteSkladiste);
            this.gb_podaciOSkaldistu.Controls.Add(this.label28);
            this.gb_podaciOSkaldistu.Location = new System.Drawing.Point(24, 377);
            this.gb_podaciOSkaldistu.Name = "gb_podaciOSkaldistu";
            this.gb_podaciOSkaldistu.Size = new System.Drawing.Size(340, 167);
            this.gb_podaciOSkaldistu.TabIndex = 14;
            this.gb_podaciOSkaldistu.TabStop = false;
            this.gb_podaciOSkaldistu.Text = "Podaci o skladištu";
            // 
            // cmbox_odaberiteSkladiste
            // 
            this.cmbox_odaberiteSkladiste.FormattingEnabled = true;
            this.cmbox_odaberiteSkladiste.Location = new System.Drawing.Point(170, 37);
            this.cmbox_odaberiteSkladiste.Name = "cmbox_odaberiteSkladiste";
            this.cmbox_odaberiteSkladiste.Size = new System.Drawing.Size(156, 24);
            this.cmbox_odaberiteSkladiste.TabIndex = 12;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(11, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(158, 20);
            this.label28.TabIndex = 0;
            this.label28.Text = "Odaberite skladište:";
            // 
            // gb_podaciOKladama
            // 
            this.gb_podaciOKladama.Controls.Add(this.dgv_podaciOKladama);
            this.gb_podaciOKladama.Location = new System.Drawing.Point(382, 17);
            this.gb_podaciOKladama.Name = "gb_podaciOKladama";
            this.gb_podaciOKladama.Size = new System.Drawing.Size(555, 527);
            this.gb_podaciOKladama.TabIndex = 13;
            this.gb_podaciOKladama.TabStop = false;
            this.gb_podaciOKladama.Text = "Podaci o kladama";
            // 
            // dgv_podaciOKladama
            // 
            this.dgv_podaciOKladama.AllowUserToOrderColumns = true;
            this.dgv_podaciOKladama.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_podaciOKladama.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_brPlocice,
            this.col_sifraDrveta,
            this.col_duljinaKlade,
            this.col_debljinaKlade,
            this.col_obujamKlade});
            this.dgv_podaciOKladama.Location = new System.Drawing.Point(6, 26);
            this.dgv_podaciOKladama.Name = "dgv_podaciOKladama";
            this.dgv_podaciOKladama.RowTemplate.Height = 24;
            this.dgv_podaciOKladama.Size = new System.Drawing.Size(543, 487);
            this.dgv_podaciOKladama.TabIndex = 0;
            // 
            // col_brPlocice
            // 
            this.col_brPlocice.HeaderText = "Broj Pločice";
            this.col_brPlocice.Name = "col_brPlocice";
            // 
            // col_sifraDrveta
            // 
            this.col_sifraDrveta.HeaderText = "Šifra";
            this.col_sifraDrveta.Name = "col_sifraDrveta";
            // 
            // col_duljinaKlade
            // 
            this.col_duljinaKlade.HeaderText = "Duljina (cm)";
            this.col_duljinaKlade.Name = "col_duljinaKlade";
            // 
            // col_debljinaKlade
            // 
            this.col_debljinaKlade.HeaderText = "Debljina (cm)";
            this.col_debljinaKlade.Name = "col_debljinaKlade";
            // 
            // col_obujamKlade
            // 
            this.col_obujamKlade.HeaderText = "Obujam (m3)";
            this.col_obujamKlade.Name = "col_obujamKlade";
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(3, 620);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(960, 42);
            this.panel7.TabIndex = 3;
            // 
            // btn_spremiUpisaneKlade
            // 
            this.btn_spremiUpisaneKlade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiUpisaneKlade.Location = new System.Drawing.Point(779, 567);
            this.btn_spremiUpisaneKlade.Name = "btn_spremiUpisaneKlade";
            this.btn_spremiUpisaneKlade.Size = new System.Drawing.Size(158, 30);
            this.btn_spremiUpisaneKlade.TabIndex = 12;
            this.btn_spremiUpisaneKlade.Text = "Spremi";
            this.btn_spremiUpisaneKlade.UseVisualStyleBackColor = true;
            this.btn_spremiUpisaneKlade.Click += new System.EventHandler(this.btn_spremiUpisaneKlade_Click);
            // 
            // gb_podaciOPopratnici
            // 
            this.gb_podaciOPopratnici.Controls.Add(this.tb_mjestoUtovara);
            this.gb_podaciOPopratnici.Controls.Add(this.label24);
            this.gb_podaciOPopratnici.Controls.Add(this.tb_sumarija);
            this.gb_podaciOPopratnici.Controls.Add(this.label16);
            this.gb_podaciOPopratnici.Controls.Add(this.cmbox_odaberiteKamion);
            this.gb_podaciOPopratnici.Controls.Add(this.datePicker_datumOtpremanja);
            this.gb_podaciOPopratnici.Controls.Add(this.label17);
            this.gb_podaciOPopratnici.Controls.Add(this.tb_imePrezimeOtpremnika);
            this.gb_podaciOPopratnici.Controls.Add(this.label21);
            this.gb_podaciOPopratnici.Controls.Add(this.label22);
            this.gb_podaciOPopratnici.Controls.Add(this.tb_vrijemeOtpremanja);
            this.gb_podaciOPopratnici.Controls.Add(this.label23);
            this.gb_podaciOPopratnici.Location = new System.Drawing.Point(24, 17);
            this.gb_podaciOPopratnici.Name = "gb_podaciOPopratnici";
            this.gb_podaciOPopratnici.Size = new System.Drawing.Size(340, 342);
            this.gb_podaciOPopratnici.TabIndex = 8;
            this.gb_podaciOPopratnici.TabStop = false;
            this.gb_podaciOPopratnici.Text = "Podaci o popratnici";
            // 
            // tb_mjestoUtovara
            // 
            this.tb_mjestoUtovara.Location = new System.Drawing.Point(170, 293);
            this.tb_mjestoUtovara.Name = "tb_mjestoUtovara";
            this.tb_mjestoUtovara.Size = new System.Drawing.Size(156, 22);
            this.tb_mjestoUtovara.TabIndex = 15;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(11, 293);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(129, 20);
            this.label24.TabIndex = 14;
            this.label24.Text = "Mjesto utovara: ";
            // 
            // tb_sumarija
            // 
            this.tb_sumarija.Location = new System.Drawing.Point(170, 246);
            this.tb_sumarija.Name = "tb_sumarija";
            this.tb_sumarija.Size = new System.Drawing.Size(156, 22);
            this.tb_sumarija.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 248);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 20);
            this.label16.TabIndex = 12;
            this.label16.Text = "Šumarija:";
            // 
            // cmbox_odaberiteKamion
            // 
            this.cmbox_odaberiteKamion.FormattingEnabled = true;
            this.cmbox_odaberiteKamion.Location = new System.Drawing.Point(172, 199);
            this.cmbox_odaberiteKamion.Name = "cmbox_odaberiteKamion";
            this.cmbox_odaberiteKamion.Size = new System.Drawing.Size(131, 24);
            this.cmbox_odaberiteKamion.TabIndex = 11;
            // 
            // datePicker_datumOtpremanja
            // 
            this.datePicker_datumOtpremanja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_datumOtpremanja.Location = new System.Drawing.Point(170, 80);
            this.datePicker_datumOtpremanja.Name = "datePicker_datumOtpremanja";
            this.datePicker_datumOtpremanja.Size = new System.Drawing.Size(133, 22);
            this.datePicker_datumOtpremanja.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(13, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 20);
            this.label17.TabIndex = 6;
            this.label17.Text = "Odaberite kamion:";
            // 
            // tb_imePrezimeOtpremnika
            // 
            this.tb_imePrezimeOtpremnika.Location = new System.Drawing.Point(170, 39);
            this.tb_imePrezimeOtpremnika.Name = "tb_imePrezimeOtpremnika";
            this.tb_imePrezimeOtpremnika.Size = new System.Drawing.Size(156, 22);
            this.tb_imePrezimeOtpremnika.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 41);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "Otpremnik:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(152, 20);
            this.label22.TabIndex = 2;
            this.label22.Text = "Datum otpremanja:";
            // 
            // tb_vrijemeOtpremanja
            // 
            this.tb_vrijemeOtpremanja.Location = new System.Drawing.Point(170, 119);
            this.tb_vrijemeOtpremanja.Name = "tb_vrijemeOtpremanja";
            this.tb_vrijemeOtpremanja.Size = new System.Drawing.Size(156, 22);
            this.tb_vrijemeOtpremanja.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(11, 119);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(159, 20);
            this.label23.TabIndex = 4;
            this.label23.Text = "Vrijeme otpremanja:";
            // 
            // pnl_RP_napisiRadniNalog
            // 
            this.pnl_RP_napisiRadniNalog.BackColor = System.Drawing.Color.White;
            this.pnl_RP_napisiRadniNalog.Controls.Add(this.gb_radniNalogHead);
            this.pnl_RP_napisiRadniNalog.Controls.Add(this.gb_tekstRadnogNaloga);
            this.pnl_RP_napisiRadniNalog.Controls.Add(this.panel6);
            this.pnl_RP_napisiRadniNalog.Controls.Add(this.btn_spremiRadniNalog);
            this.pnl_RP_napisiRadniNalog.Controls.Add(this.gb_PodaciORadnomNalogu);
            this.pnl_RP_napisiRadniNalog.Location = new System.Drawing.Point(1534, 138);
            this.pnl_RP_napisiRadniNalog.Name = "pnl_RP_napisiRadniNalog";
            this.pnl_RP_napisiRadniNalog.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_napisiRadniNalog.TabIndex = 17;
            this.pnl_RP_napisiRadniNalog.Visible = false;
            // 
            // gb_radniNalogHead
            // 
            this.gb_radniNalogHead.Controls.Add(this.tb_RadniNalogHead);
            this.gb_radniNalogHead.Enabled = false;
            this.gb_radniNalogHead.Location = new System.Drawing.Point(401, 17);
            this.gb_radniNalogHead.Name = "gb_radniNalogHead";
            this.gb_radniNalogHead.Size = new System.Drawing.Size(529, 170);
            this.gb_radniNalogHead.TabIndex = 15;
            this.gb_radniNalogHead.TabStop = false;
            this.gb_radniNalogHead.Text = "Zaglavlje";
            // 
            // tb_RadniNalogHead
            // 
            this.tb_RadniNalogHead.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_RadniNalogHead.Location = new System.Drawing.Point(6, 21);
            this.tb_RadniNalogHead.Multiline = true;
            this.tb_RadniNalogHead.Name = "tb_RadniNalogHead";
            this.tb_RadniNalogHead.Size = new System.Drawing.Size(523, 143);
            this.tb_RadniNalogHead.TabIndex = 13;
            // 
            // gb_tekstRadnogNaloga
            // 
            this.gb_tekstRadnogNaloga.Controls.Add(this.tb_radniNalogTekst);
            this.gb_tekstRadnogNaloga.Location = new System.Drawing.Point(23, 193);
            this.gb_tekstRadnogNaloga.Name = "gb_tekstRadnogNaloga";
            this.gb_tekstRadnogNaloga.Size = new System.Drawing.Size(913, 363);
            this.gb_tekstRadnogNaloga.TabIndex = 14;
            this.gb_tekstRadnogNaloga.TabStop = false;
            this.gb_tekstRadnogNaloga.Text = "Tekst";
            // 
            // tb_radniNalogTekst
            // 
            this.tb_radniNalogTekst.AcceptsTab = true;
            this.tb_radniNalogTekst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_radniNalogTekst.Location = new System.Drawing.Point(6, 21);
            this.tb_radniNalogTekst.Multiline = true;
            this.tb_radniNalogTekst.Name = "tb_radniNalogTekst";
            this.tb_radniNalogTekst.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_radniNalogTekst.Size = new System.Drawing.Size(901, 336);
            this.tb_radniNalogTekst.TabIndex = 13;
            this.tb_radniNalogTekst.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tb_radniNalogTekst_MouseClick);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(3, 620);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(960, 42);
            this.panel6.TabIndex = 3;
            // 
            // btn_spremiRadniNalog
            // 
            this.btn_spremiRadniNalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiRadniNalog.Location = new System.Drawing.Point(778, 571);
            this.btn_spremiRadniNalog.Name = "btn_spremiRadniNalog";
            this.btn_spremiRadniNalog.Size = new System.Drawing.Size(158, 30);
            this.btn_spremiRadniNalog.TabIndex = 12;
            this.btn_spremiRadniNalog.Text = "Spremi";
            this.btn_spremiRadniNalog.UseVisualStyleBackColor = true;
            this.btn_spremiRadniNalog.Click += new System.EventHandler(this.btn_spremiRadniNalog_Click);
            // 
            // gb_PodaciORadnomNalogu
            // 
            this.gb_PodaciORadnomNalogu.Controls.Add(this.cmbox_nalogRadnik);
            this.gb_PodaciORadnomNalogu.Controls.Add(this.cmbox_nalogPozicijaRada);
            this.gb_PodaciORadnomNalogu.Controls.Add(this.datePicker_nalogZaDatum);
            this.gb_PodaciORadnomNalogu.Controls.Add(this.label30);
            this.gb_PodaciORadnomNalogu.Controls.Add(this.label31);
            this.gb_PodaciORadnomNalogu.Controls.Add(this.label32);
            this.gb_PodaciORadnomNalogu.Location = new System.Drawing.Point(24, 17);
            this.gb_PodaciORadnomNalogu.Name = "gb_PodaciORadnomNalogu";
            this.gb_PodaciORadnomNalogu.Size = new System.Drawing.Size(346, 170);
            this.gb_PodaciORadnomNalogu.TabIndex = 8;
            this.gb_PodaciORadnomNalogu.TabStop = false;
            this.gb_PodaciORadnomNalogu.Text = "Pojedinosti radnog naloga";
            // 
            // cmbox_nalogRadnik
            // 
            this.cmbox_nalogRadnik.FormattingEnabled = true;
            this.cmbox_nalogRadnik.Location = new System.Drawing.Point(128, 80);
            this.cmbox_nalogRadnik.Name = "cmbox_nalogRadnik";
            this.cmbox_nalogRadnik.Size = new System.Drawing.Size(176, 24);
            this.cmbox_nalogRadnik.TabIndex = 12;
            // 
            // cmbox_nalogPozicijaRada
            // 
            this.cmbox_nalogPozicijaRada.FormattingEnabled = true;
            this.cmbox_nalogPozicijaRada.Location = new System.Drawing.Point(128, 37);
            this.cmbox_nalogPozicijaRada.Name = "cmbox_nalogPozicijaRada";
            this.cmbox_nalogPozicijaRada.Size = new System.Drawing.Size(176, 24);
            this.cmbox_nalogPozicijaRada.TabIndex = 11;
            this.cmbox_nalogPozicijaRada.SelectedIndexChanged += new System.EventHandler(this.cmbox_nalogPozicijaRada_SelectedIndexChanged);
            // 
            // datePicker_nalogZaDatum
            // 
            this.datePicker_nalogZaDatum.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_nalogZaDatum.Location = new System.Drawing.Point(128, 120);
            this.datePicker_nalogZaDatum.Name = "datePicker_nalogZaDatum";
            this.datePicker_nalogZaDatum.Size = new System.Drawing.Size(133, 22);
            this.datePicker_nalogZaDatum.TabIndex = 10;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(11, 41);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(111, 20);
            this.label30.TabIndex = 0;
            this.label30.Text = "Pozicija rada:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(11, 80);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 20);
            this.label31.TabIndex = 2;
            this.label31.Text = "Radnik:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(11, 119);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 20);
            this.label32.TabIndex = 4;
            this.label32.Text = "Za datum:";
            // 
            // pnl_RP_poslovodaPrikaziRadniNalog
            // 
            this.pnl_RP_poslovodaPrikaziRadniNalog.BackColor = System.Drawing.Color.White;
            this.pnl_RP_poslovodaPrikaziRadniNalog.Controls.Add(this.gb_RN_dodatneOpcije);
            this.pnl_RP_poslovodaPrikaziRadniNalog.Controls.Add(this.gb_PozicijaRadaTxt);
            this.pnl_RP_poslovodaPrikaziRadniNalog.Controls.Add(this.panel8);
            this.pnl_RP_poslovodaPrikaziRadniNalog.Controls.Add(this.gb_pozicijaRadaPretraziPo);
            this.pnl_RP_poslovodaPrikaziRadniNalog.Location = new System.Drawing.Point(1351, 324);
            this.pnl_RP_poslovodaPrikaziRadniNalog.Name = "pnl_RP_poslovodaPrikaziRadniNalog";
            this.pnl_RP_poslovodaPrikaziRadniNalog.Size = new System.Drawing.Size(955, 617);
            this.pnl_RP_poslovodaPrikaziRadniNalog.TabIndex = 18;
            this.pnl_RP_poslovodaPrikaziRadniNalog.Visible = false;
            // 
            // gb_RN_dodatneOpcije
            // 
            this.gb_RN_dodatneOpcije.Controls.Add(this.btn_poslovodaIsprintajRN);
            this.gb_RN_dodatneOpcije.Location = new System.Drawing.Point(637, 17);
            this.gb_RN_dodatneOpcije.Name = "gb_RN_dodatneOpcije";
            this.gb_RN_dodatneOpcije.Size = new System.Drawing.Size(299, 112);
            this.gb_RN_dodatneOpcije.TabIndex = 15;
            this.gb_RN_dodatneOpcije.TabStop = false;
            this.gb_RN_dodatneOpcije.Text = "Dodatne opcije";
            // 
            // btn_poslovodaIsprintajRN
            // 
            this.btn_poslovodaIsprintajRN.Enabled = false;
            this.btn_poslovodaIsprintajRN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_poslovodaIsprintajRN.Location = new System.Drawing.Point(6, 68);
            this.btn_poslovodaIsprintajRN.Name = "btn_poslovodaIsprintajRN";
            this.btn_poslovodaIsprintajRN.Size = new System.Drawing.Size(284, 30);
            this.btn_poslovodaIsprintajRN.TabIndex = 16;
            this.btn_poslovodaIsprintajRN.Text = "Isprintaj";
            this.btn_poslovodaIsprintajRN.UseVisualStyleBackColor = true;
            this.btn_poslovodaIsprintajRN.Click += new System.EventHandler(this.btn_poslovodaIsprintajRN_Click);
            // 
            // gb_PozicijaRadaTxt
            // 
            this.gb_PozicijaRadaTxt.Controls.Add(this.rtb_RadniNalogTxt);
            this.gb_PozicijaRadaTxt.Location = new System.Drawing.Point(23, 133);
            this.gb_PozicijaRadaTxt.Name = "gb_PozicijaRadaTxt";
            this.gb_PozicijaRadaTxt.Size = new System.Drawing.Size(913, 474);
            this.gb_PozicijaRadaTxt.TabIndex = 14;
            this.gb_PozicijaRadaTxt.TabStop = false;
            this.gb_PozicijaRadaTxt.Text = "Tekst";
            // 
            // rtb_RadniNalogTxt
            // 
            this.rtb_RadniNalogTxt.AcceptsTab = true;
            this.rtb_RadniNalogTxt.Font = new System.Drawing.Font("Arial", 9F);
            this.rtb_RadniNalogTxt.Location = new System.Drawing.Point(6, 20);
            this.rtb_RadniNalogTxt.Name = "rtb_RadniNalogTxt";
            this.rtb_RadniNalogTxt.ReadOnly = true;
            this.rtb_RadniNalogTxt.Size = new System.Drawing.Size(901, 448);
            this.rtb_RadniNalogTxt.TabIndex = 5;
            this.rtb_RadniNalogTxt.Text = "";
            // 
            // panel8
            // 
            this.panel8.Location = new System.Drawing.Point(3, 620);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(960, 42);
            this.panel8.TabIndex = 3;
            // 
            // gb_pozicijaRadaPretraziPo
            // 
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.cmbox_vrijemePisanjaRN);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.label29);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.btn_poslovodaPrikaziRadniNalog);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.cmbox_poslovodaRNRadnik);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.cmbox_poslovodaRNPozicijaRada);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.datePicker_RadniNalogDatum);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.label25);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.label26);
            this.gb_pozicijaRadaPretraziPo.Controls.Add(this.label27);
            this.gb_pozicijaRadaPretraziPo.Location = new System.Drawing.Point(24, 17);
            this.gb_pozicijaRadaPretraziPo.Name = "gb_pozicijaRadaPretraziPo";
            this.gb_pozicijaRadaPretraziPo.Size = new System.Drawing.Size(608, 112);
            this.gb_pozicijaRadaPretraziPo.TabIndex = 8;
            this.gb_pozicijaRadaPretraziPo.TabStop = false;
            this.gb_pozicijaRadaPretraziPo.Text = "Pretraži po";
            // 
            // cmbox_vrijemePisanjaRN
            // 
            this.cmbox_vrijemePisanjaRN.FormattingEnabled = true;
            this.cmbox_vrijemePisanjaRN.Location = new System.Drawing.Point(445, 20);
            this.cmbox_vrijemePisanjaRN.Name = "cmbox_vrijemePisanjaRN";
            this.cmbox_vrijemePisanjaRN.Size = new System.Drawing.Size(157, 24);
            this.cmbox_vrijemePisanjaRN.TabIndex = 17;
            this.cmbox_vrijemePisanjaRN.Click += new System.EventHandler(this.cmbox_vrijemePisanjaRN_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(314, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(139, 20);
            this.label29.TabIndex = 16;
            this.label29.Text = "Vrijeme pisanja : ";
            // 
            // btn_poslovodaPrikaziRadniNalog
            // 
            this.btn_poslovodaPrikaziRadniNalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_poslovodaPrikaziRadniNalog.Location = new System.Drawing.Point(318, 68);
            this.btn_poslovodaPrikaziRadniNalog.Name = "btn_poslovodaPrikaziRadniNalog";
            this.btn_poslovodaPrikaziRadniNalog.Size = new System.Drawing.Size(284, 30);
            this.btn_poslovodaPrikaziRadniNalog.TabIndex = 15;
            this.btn_poslovodaPrikaziRadniNalog.Text = "Prikaži";
            this.btn_poslovodaPrikaziRadniNalog.UseVisualStyleBackColor = true;
            this.btn_poslovodaPrikaziRadniNalog.Click += new System.EventHandler(this.btn_poslovodaPrikaziRadniNalog_Click_1);
            // 
            // cmbox_poslovodaRNRadnik
            // 
            this.cmbox_poslovodaRNRadnik.FormattingEnabled = true;
            this.cmbox_poslovodaRNRadnik.Location = new System.Drawing.Point(123, 49);
            this.cmbox_poslovodaRNRadnik.Name = "cmbox_poslovodaRNRadnik";
            this.cmbox_poslovodaRNRadnik.Size = new System.Drawing.Size(176, 24);
            this.cmbox_poslovodaRNRadnik.TabIndex = 12;
            // 
            // cmbox_poslovodaRNPozicijaRada
            // 
            this.cmbox_poslovodaRNPozicijaRada.FormattingEnabled = true;
            this.cmbox_poslovodaRNPozicijaRada.Location = new System.Drawing.Point(123, 20);
            this.cmbox_poslovodaRNPozicijaRada.Name = "cmbox_poslovodaRNPozicijaRada";
            this.cmbox_poslovodaRNPozicijaRada.Size = new System.Drawing.Size(176, 24);
            this.cmbox_poslovodaRNPozicijaRada.TabIndex = 11;
            this.cmbox_poslovodaRNPozicijaRada.SelectedIndexChanged += new System.EventHandler(this.cmbox_poslovodaRNPozicijaRada_SelectedIndexChanged);
            // 
            // datePicker_RadniNalogDatum
            // 
            this.datePicker_RadniNalogDatum.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_RadniNalogDatum.Location = new System.Drawing.Point(166, 79);
            this.datePicker_RadniNalogDatum.Name = "datePicker_RadniNalogDatum";
            this.datePicker_RadniNalogDatum.Size = new System.Drawing.Size(133, 22);
            this.datePicker_RadniNalogDatum.TabIndex = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(111, 20);
            this.label25.TabIndex = 0;
            this.label25.Text = "Pozicija rada:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 53);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 20);
            this.label26.TabIndex = 2;
            this.label26.Text = "Radnik:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 81);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(153, 20);
            this.label27.TabIndex = 4;
            this.label27.Text = "Datum izvršavanja:";
            // 
            // printDocument_RadniNalog
            // 
            this.printDocument_RadniNalog.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_RadniNalog_PrintPage);
            // 
            // printDialog_RadniNalog
            // 
            this.printDialog_RadniNalog.Document = this.printDocument_RadniNalog;
            this.printDialog_RadniNalog.UseEXDialog = true;
            // 
            // pnl_Izbornik_ZaOneSaSamoRN
            // 
            this.pnl_Izbornik_ZaOneSaSamoRN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_ZaOneSaSamoRN.Controls.Add(this.btn_IZB_PrikaziRadniNalog);
            this.pnl_Izbornik_ZaOneSaSamoRN.Location = new System.Drawing.Point(1544, 122);
            this.pnl_Izbornik_ZaOneSaSamoRN.Name = "pnl_Izbornik_ZaOneSaSamoRN";
            this.pnl_Izbornik_ZaOneSaSamoRN.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_ZaOneSaSamoRN.TabIndex = 19;
            this.pnl_Izbornik_ZaOneSaSamoRN.Visible = false;
            // 
            // btn_IZB_PrikaziRadniNalog
            // 
            this.btn_IZB_PrikaziRadniNalog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziRadniNalog.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziRadniNalog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziRadniNalog.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_PrikaziRadniNalog.Name = "btn_IZB_PrikaziRadniNalog";
            this.btn_IZB_PrikaziRadniNalog.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziRadniNalog.TabIndex = 2;
            this.btn_IZB_PrikaziRadniNalog.Text = "Prikaži radni nalog";
            this.btn_IZB_PrikaziRadniNalog.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziRadniNalog.Click += new System.EventHandler(this.btn_IZB_PrikaziRadniNalog_Click);
            // 
            // pnl_RP_PrikaziRadniNalogSAMO_RN
            // 
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.BackColor = System.Drawing.Color.White;
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Controls.Add(this.gb_txtRadnogNaloga_SAMO_RN);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Controls.Add(this.panel9);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Controls.Add(this.gb_PretraziPo_RN_SAMO_RN);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Location = new System.Drawing.Point(1511, 156);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Name = "pnl_RP_PrikaziRadniNalogSAMO_RN";
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.TabIndex = 20;
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = false;
            // 
            // gb_txtRadnogNaloga_SAMO_RN
            // 
            this.gb_txtRadnogNaloga_SAMO_RN.Controls.Add(this.rtb_txtRN_SAMO_RN);
            this.gb_txtRadnogNaloga_SAMO_RN.Location = new System.Drawing.Point(23, 133);
            this.gb_txtRadnogNaloga_SAMO_RN.Name = "gb_txtRadnogNaloga_SAMO_RN";
            this.gb_txtRadnogNaloga_SAMO_RN.Size = new System.Drawing.Size(913, 474);
            this.gb_txtRadnogNaloga_SAMO_RN.TabIndex = 14;
            this.gb_txtRadnogNaloga_SAMO_RN.TabStop = false;
            this.gb_txtRadnogNaloga_SAMO_RN.Text = "Tekst";
            // 
            // rtb_txtRN_SAMO_RN
            // 
            this.rtb_txtRN_SAMO_RN.AcceptsTab = true;
            this.rtb_txtRN_SAMO_RN.Font = new System.Drawing.Font("Arial", 9F);
            this.rtb_txtRN_SAMO_RN.Location = new System.Drawing.Point(6, 20);
            this.rtb_txtRN_SAMO_RN.Name = "rtb_txtRN_SAMO_RN";
            this.rtb_txtRN_SAMO_RN.ReadOnly = true;
            this.rtb_txtRN_SAMO_RN.Size = new System.Drawing.Size(901, 448);
            this.rtb_txtRN_SAMO_RN.TabIndex = 5;
            this.rtb_txtRN_SAMO_RN.Text = "";
            // 
            // panel9
            // 
            this.panel9.Location = new System.Drawing.Point(3, 620);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(960, 42);
            this.panel9.TabIndex = 3;
            // 
            // gb_PretraziPo_RN_SAMO_RN
            // 
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.btn_isprintajRN_SAMO_RN);
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.cmbox_vrijemePisanjaRN_SAMO_RN);
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.label33);
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.btn_prikaziRN_SAMO_RN);
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.datePicker_datumIzvrsavanjaRN_SAMO_RN);
            this.gb_PretraziPo_RN_SAMO_RN.Controls.Add(this.label36);
            this.gb_PretraziPo_RN_SAMO_RN.Location = new System.Drawing.Point(24, 17);
            this.gb_PretraziPo_RN_SAMO_RN.Name = "gb_PretraziPo_RN_SAMO_RN";
            this.gb_PretraziPo_RN_SAMO_RN.Size = new System.Drawing.Size(584, 112);
            this.gb_PretraziPo_RN_SAMO_RN.TabIndex = 8;
            this.gb_PretraziPo_RN_SAMO_RN.TabStop = false;
            this.gb_PretraziPo_RN_SAMO_RN.Text = "Pretraži po";
            // 
            // btn_isprintajRN_SAMO_RN
            // 
            this.btn_isprintajRN_SAMO_RN.Enabled = false;
            this.btn_isprintajRN_SAMO_RN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_isprintajRN_SAMO_RN.Location = new System.Drawing.Point(350, 64);
            this.btn_isprintajRN_SAMO_RN.Name = "btn_isprintajRN_SAMO_RN";
            this.btn_isprintajRN_SAMO_RN.Size = new System.Drawing.Size(215, 30);
            this.btn_isprintajRN_SAMO_RN.TabIndex = 18;
            this.btn_isprintajRN_SAMO_RN.Text = "Isprintaj";
            this.btn_isprintajRN_SAMO_RN.UseVisualStyleBackColor = true;
            this.btn_isprintajRN_SAMO_RN.Click += new System.EventHandler(this.btn_isprintajRN_SAMO_RN_Click);
            // 
            // cmbox_vrijemePisanjaRN_SAMO_RN
            // 
            this.cmbox_vrijemePisanjaRN_SAMO_RN.FormattingEnabled = true;
            this.cmbox_vrijemePisanjaRN_SAMO_RN.Location = new System.Drawing.Point(151, 70);
            this.cmbox_vrijemePisanjaRN_SAMO_RN.Name = "cmbox_vrijemePisanjaRN_SAMO_RN";
            this.cmbox_vrijemePisanjaRN_SAMO_RN.Size = new System.Drawing.Size(157, 24);
            this.cmbox_vrijemePisanjaRN_SAMO_RN.TabIndex = 17;
            this.cmbox_vrijemePisanjaRN_SAMO_RN.Click += new System.EventHandler(this.cmbox_vrijemePisanjaRN_SAMO_RN_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 74);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 20);
            this.label33.TabIndex = 16;
            this.label33.Text = "Vrijeme pisanja : ";
            // 
            // btn_prikaziRN_SAMO_RN
            // 
            this.btn_prikaziRN_SAMO_RN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_prikaziRN_SAMO_RN.Location = new System.Drawing.Point(350, 21);
            this.btn_prikaziRN_SAMO_RN.Name = "btn_prikaziRN_SAMO_RN";
            this.btn_prikaziRN_SAMO_RN.Size = new System.Drawing.Size(215, 30);
            this.btn_prikaziRN_SAMO_RN.TabIndex = 15;
            this.btn_prikaziRN_SAMO_RN.Text = "Prikaži";
            this.btn_prikaziRN_SAMO_RN.UseVisualStyleBackColor = true;
            this.btn_prikaziRN_SAMO_RN.Click += new System.EventHandler(this.btn_prikaziRN_SAMO_RN_Click);
            // 
            // datePicker_datumIzvrsavanjaRN_SAMO_RN
            // 
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN.Location = new System.Drawing.Point(175, 31);
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN.Name = "datePicker_datumIzvrsavanjaRN_SAMO_RN";
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN.Size = new System.Drawing.Size(133, 22);
            this.datePicker_datumIzvrsavanjaRN_SAMO_RN.TabIndex = 10;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 31);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(153, 20);
            this.label36.TabIndex = 4;
            this.label36.Text = "Datum izvršavanja:";
            // 
            // printDocument_SAMO_RN
            // 
            this.printDocument_SAMO_RN.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_SAMO_RN_PrintPage);
            // 
            // printDialog_SAMO_RN
            // 
            this.printDialog_SAMO_RN.Document = this.printDocument_SAMO_RN;
            this.printDialog_SAMO_RN.UseEXDialog = true;
            // 
            // pnl_Izbornik_PomocniRadnikPrijePilane
            // 
            this.pnl_Izbornik_PomocniRadnikPrijePilane.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Controls.Add(this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane);
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Controls.Add(this.btn_IZB_UpisDugackihKladaZaPiljenje);
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Controls.Add(this.btn_IZB_upisiKladeKojeCeSeIzrezati);
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Location = new System.Drawing.Point(1490, 182);
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Name = "pnl_Izbornik_PomocniRadnikPrijePilane";
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_PomocniRadnikPrijePilane.TabIndex = 21;
            this.pnl_Izbornik_PomocniRadnikPrijePilane.Visible = false;
            // 
            // btn_IZB_PrikaziRadniNalogRadnikPrijePilane
            // 
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.Location = new System.Drawing.Point(15, 187);
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.Name = "btn_IZB_PrikaziRadniNalogRadnikPrijePilane";
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.TabIndex = 4;
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.Text = "Prikaži radni nalog";
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane.Click += new System.EventHandler(this.btn_IZB_PrikaziRadniNalogRadnikPrijePilane_Click);
            // 
            // btn_IZB_UpisDugackihKladaZaPiljenje
            // 
            this.btn_IZB_UpisDugackihKladaZaPiljenje.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_UpisDugackihKladaZaPiljenje.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_UpisDugackihKladaZaPiljenje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_UpisDugackihKladaZaPiljenje.Location = new System.Drawing.Point(15, 136);
            this.btn_IZB_UpisDugackihKladaZaPiljenje.Name = "btn_IZB_UpisDugackihKladaZaPiljenje";
            this.btn_IZB_UpisDugackihKladaZaPiljenje.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_UpisDugackihKladaZaPiljenje.TabIndex = 3;
            this.btn_IZB_UpisDugackihKladaZaPiljenje.Text = "Upis dugačkih klada za piljenje";
            this.btn_IZB_UpisDugackihKladaZaPiljenje.UseVisualStyleBackColor = false;
            this.btn_IZB_UpisDugackihKladaZaPiljenje.Click += new System.EventHandler(this.btn_IZB_UpisDugackihKladaZaPiljenje_Click);
            // 
            // btn_IZB_upisiKladeKojeCeSeIzrezati
            // 
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.Name = "btn_IZB_upisiKladeKojeCeSeIzrezati";
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.TabIndex = 2;
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.Text = "Upis klada za rezanje TPT-om";
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.UseVisualStyleBackColor = false;
            this.btn_IZB_upisiKladeKojeCeSeIzrezati.Click += new System.EventHandler(this.btn_IZB_upisiKladeKojeCeSeIzrezati_Click);
            // 
            // pnl_RP_UpisiKladeKojeCeSeIzrezati
            // 
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.BackColor = System.Drawing.Color.White;
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Controls.Add(this.gb_brojplocica_KladeKojeCeSeRezati);
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Controls.Add(this.panel10);
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Location = new System.Drawing.Point(1505, 164);
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Name = "pnl_RP_UpisiKladeKojeCeSeIzrezati";
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.TabIndex = 22;
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.Visible = false;
            // 
            // gb_brojplocica_KladeKojeCeSeRezati
            // 
            this.gb_brojplocica_KladeKojeCeSeRezati.Controls.Add(this.label35);
            this.gb_brojplocica_KladeKojeCeSeRezati.Controls.Add(this.dgv_brojPlocicaZaRezanje);
            this.gb_brojplocica_KladeKojeCeSeRezati.Controls.Add(this.cmbox_OdaberiteMajstoraZaRezanjeKlada);
            this.gb_brojplocica_KladeKojeCeSeRezati.Controls.Add(this.btn_upisiIzrezanePlocice);
            this.gb_brojplocica_KladeKojeCeSeRezati.Location = new System.Drawing.Point(17, 18);
            this.gb_brojplocica_KladeKojeCeSeRezati.Name = "gb_brojplocica_KladeKojeCeSeRezati";
            this.gb_brojplocica_KladeKojeCeSeRezati.Size = new System.Drawing.Size(490, 304);
            this.gb_brojplocica_KladeKojeCeSeRezati.TabIndex = 13;
            this.gb_brojplocica_KladeKojeCeSeRezati.TabStop = false;
            this.gb_brojplocica_KladeKojeCeSeRezati.Text = "Podaci o kladama";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(165, 171);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(157, 20);
            this.label35.TabIndex = 15;
            this.label35.Text = "Odaberite majstora:";
            // 
            // dgv_brojPlocicaZaRezanje
            // 
            this.dgv_brojPlocicaZaRezanje.AllowUserToOrderColumns = true;
            this.dgv_brojPlocicaZaRezanje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_brojPlocicaZaRezanje.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgv_brojPlocicaZaRezanje.Location = new System.Drawing.Point(16, 28);
            this.dgv_brojPlocicaZaRezanje.Name = "dgv_brojPlocicaZaRezanje";
            this.dgv_brojPlocicaZaRezanje.RowTemplate.Height = 24;
            this.dgv_brojPlocicaZaRezanje.Size = new System.Drawing.Size(143, 259);
            this.dgv_brojPlocicaZaRezanje.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Broj Pločice";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // cmbox_OdaberiteMajstoraZaRezanjeKlada
            // 
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada.FormattingEnabled = true;
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada.Location = new System.Drawing.Point(328, 171);
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada.Name = "cmbox_OdaberiteMajstoraZaRezanjeKlada";
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada.Size = new System.Drawing.Size(149, 24);
            this.cmbox_OdaberiteMajstoraZaRezanjeKlada.TabIndex = 13;
            // 
            // btn_upisiIzrezanePlocice
            // 
            this.btn_upisiIzrezanePlocice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_upisiIzrezanePlocice.Location = new System.Drawing.Point(173, 215);
            this.btn_upisiIzrezanePlocice.Name = "btn_upisiIzrezanePlocice";
            this.btn_upisiIzrezanePlocice.Size = new System.Drawing.Size(304, 30);
            this.btn_upisiIzrezanePlocice.TabIndex = 12;
            this.btn_upisiIzrezanePlocice.Text = "Upiši";
            this.btn_upisiIzrezanePlocice.UseVisualStyleBackColor = true;
            this.btn_upisiIzrezanePlocice.Click += new System.EventHandler(this.btn_upisiIzrezanePlocice_Click);
            // 
            // panel10
            // 
            this.panel10.Location = new System.Drawing.Point(3, 620);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(960, 42);
            this.panel10.TabIndex = 3;
            // 
            // pnl_RP_upisDugackihKladaZaPiljenje
            // 
            this.pnl_RP_upisDugackihKladaZaPiljenje.BackColor = System.Drawing.Color.White;
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.cmbox_odaberiteSkaldisteZaPiljenjeKlada);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.label45);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.gb_podaciODrugojPoloviciKlade);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.gb_podaciOPrvojPoloviciKlade);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.panel11);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.btn_upisiKladuZaPiljenje);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.tb_brojPlociceZaPiljenje);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Controls.Add(this.label34);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Location = new System.Drawing.Point(1479, 202);
            this.pnl_RP_upisDugackihKladaZaPiljenje.Name = "pnl_RP_upisDugackihKladaZaPiljenje";
            this.pnl_RP_upisDugackihKladaZaPiljenje.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_upisDugackihKladaZaPiljenje.TabIndex = 23;
            this.pnl_RP_upisDugackihKladaZaPiljenje.Visible = false;
            // 
            // cmbox_odaberiteSkaldisteZaPiljenjeKlada
            // 
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada.FormattingEnabled = true;
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada.Location = new System.Drawing.Point(197, 84);
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada.Name = "cmbox_odaberiteSkaldisteZaPiljenjeKlada";
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada.Size = new System.Drawing.Size(156, 24);
            this.cmbox_odaberiteSkaldisteZaPiljenjeKlada.TabIndex = 23;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(29, 86);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(158, 20);
            this.label45.TabIndex = 22;
            this.label45.Text = "Odaberite skladište:";
            // 
            // gb_podaciODrugojPoloviciKlade
            // 
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.tb_sifra2Klade);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.label44);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.tb_obujam2klade);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.label40);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.tb_debljina2klade);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.label41);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.tb_duljina2klade);
            this.gb_podaciODrugojPoloviciKlade.Controls.Add(this.label42);
            this.gb_podaciODrugojPoloviciKlade.Location = new System.Drawing.Point(429, 147);
            this.gb_podaciODrugojPoloviciKlade.Name = "gb_podaciODrugojPoloviciKlade";
            this.gb_podaciODrugojPoloviciKlade.Size = new System.Drawing.Size(368, 350);
            this.gb_podaciODrugojPoloviciKlade.TabIndex = 21;
            this.gb_podaciODrugojPoloviciKlade.TabStop = false;
            this.gb_podaciODrugojPoloviciKlade.Text = "Podaci o drugoj polovici klade";
            // 
            // tb_sifra2Klade
            // 
            this.tb_sifra2Klade.Location = new System.Drawing.Point(102, 169);
            this.tb_sifra2Klade.Name = "tb_sifra2Klade";
            this.tb_sifra2Klade.Size = new System.Drawing.Size(179, 22);
            this.tb_sifra2Klade.TabIndex = 28;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(12, 169);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 20);
            this.label44.TabIndex = 27;
            this.label44.Text = "Šifra:";
            // 
            // tb_obujam2klade
            // 
            this.tb_obujam2klade.Location = new System.Drawing.Point(102, 134);
            this.tb_obujam2klade.Name = "tb_obujam2klade";
            this.tb_obujam2klade.Size = new System.Drawing.Size(179, 22);
            this.tb_obujam2klade.TabIndex = 26;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(12, 134);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 20);
            this.label40.TabIndex = 25;
            this.label40.Text = "Obujam:";
            // 
            // tb_debljina2klade
            // 
            this.tb_debljina2klade.Location = new System.Drawing.Point(102, 98);
            this.tb_debljina2klade.Name = "tb_debljina2klade";
            this.tb_debljina2klade.Size = new System.Drawing.Size(179, 22);
            this.tb_debljina2klade.TabIndex = 24;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(12, 98);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(75, 20);
            this.label41.TabIndex = 23;
            this.label41.Text = "Debljina:";
            // 
            // tb_duljina2klade
            // 
            this.tb_duljina2klade.Location = new System.Drawing.Point(102, 62);
            this.tb_duljina2klade.Name = "tb_duljina2klade";
            this.tb_duljina2klade.Size = new System.Drawing.Size(179, 22);
            this.tb_duljina2klade.TabIndex = 22;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(12, 62);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(66, 20);
            this.label42.TabIndex = 21;
            this.label42.Text = "Duljina:";
            // 
            // gb_podaciOPrvojPoloviciKlade
            // 
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.tb_sifra1Klade);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.label43);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.tb_obujam1klade);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.label39);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.tb_debljina1klade);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.label38);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.tb_duljina1klade);
            this.gb_podaciOPrvojPoloviciKlade.Controls.Add(this.label37);
            this.gb_podaciOPrvojPoloviciKlade.Location = new System.Drawing.Point(33, 147);
            this.gb_podaciOPrvojPoloviciKlade.Name = "gb_podaciOPrvojPoloviciKlade";
            this.gb_podaciOPrvojPoloviciKlade.Size = new System.Drawing.Size(368, 350);
            this.gb_podaciOPrvojPoloviciKlade.TabIndex = 13;
            this.gb_podaciOPrvojPoloviciKlade.TabStop = false;
            this.gb_podaciOPrvojPoloviciKlade.Text = "Podaci o prvoj polovici klade";
            // 
            // tb_sifra1Klade
            // 
            this.tb_sifra1Klade.Location = new System.Drawing.Point(102, 169);
            this.tb_sifra1Klade.Name = "tb_sifra1Klade";
            this.tb_sifra1Klade.Size = new System.Drawing.Size(179, 22);
            this.tb_sifra1Klade.TabIndex = 28;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(12, 169);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(49, 20);
            this.label43.TabIndex = 27;
            this.label43.Text = "Šifra:";
            // 
            // tb_obujam1klade
            // 
            this.tb_obujam1klade.Location = new System.Drawing.Point(102, 132);
            this.tb_obujam1klade.Name = "tb_obujam1klade";
            this.tb_obujam1klade.Size = new System.Drawing.Size(179, 22);
            this.tb_obujam1klade.TabIndex = 26;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(12, 132);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(72, 20);
            this.label39.TabIndex = 25;
            this.label39.Text = "Obujam:";
            // 
            // tb_debljina1klade
            // 
            this.tb_debljina1klade.Location = new System.Drawing.Point(102, 96);
            this.tb_debljina1klade.Name = "tb_debljina1klade";
            this.tb_debljina1klade.Size = new System.Drawing.Size(179, 22);
            this.tb_debljina1klade.TabIndex = 24;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(12, 96);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(75, 20);
            this.label38.TabIndex = 23;
            this.label38.Text = "Debljina:";
            // 
            // tb_duljina1klade
            // 
            this.tb_duljina1klade.Location = new System.Drawing.Point(102, 62);
            this.tb_duljina1klade.Name = "tb_duljina1klade";
            this.tb_duljina1klade.Size = new System.Drawing.Size(179, 22);
            this.tb_duljina1klade.TabIndex = 22;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(12, 62);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(66, 20);
            this.label37.TabIndex = 21;
            this.label37.Text = "Duljina:";
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(3, 620);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(960, 42);
            this.panel11.TabIndex = 3;
            // 
            // btn_upisiKladuZaPiljenje
            // 
            this.btn_upisiKladuZaPiljenje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_upisiKladuZaPiljenje.Location = new System.Drawing.Point(429, 510);
            this.btn_upisiKladuZaPiljenje.Name = "btn_upisiKladuZaPiljenje";
            this.btn_upisiKladuZaPiljenje.Size = new System.Drawing.Size(368, 30);
            this.btn_upisiKladuZaPiljenje.TabIndex = 12;
            this.btn_upisiKladuZaPiljenje.Text = "Upiši";
            this.btn_upisiKladuZaPiljenje.UseVisualStyleBackColor = true;
            this.btn_upisiKladuZaPiljenje.Click += new System.EventHandler(this.btn_upisiKladuZaPiljenje_Click);
            // 
            // tb_brojPlociceZaPiljenje
            // 
            this.tb_brojPlociceZaPiljenje.Location = new System.Drawing.Point(197, 41);
            this.tb_brojPlociceZaPiljenje.Name = "tb_brojPlociceZaPiljenje";
            this.tb_brojPlociceZaPiljenje.Size = new System.Drawing.Size(179, 22);
            this.tb_brojPlociceZaPiljenje.TabIndex = 20;
            this.tb_brojPlociceZaPiljenje.Leave += new System.EventHandler(this.tb_brojPlociceZaPiljenje_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(29, 43);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(162, 20);
            this.label34.TabIndex = 15;
            this.label34.Text = "Unesite broj pločice:";
            // 
            // pnl_Izbornik_MajstorNaTPT
            // 
            this.pnl_Izbornik_MajstorNaTPT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_MajstorNaTPT.Controls.Add(this.btn_IZB_PrikaziStatistiku_MajstorNaTPT);
            this.pnl_Izbornik_MajstorNaTPT.Controls.Add(this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT);
            this.pnl_Izbornik_MajstorNaTPT.Location = new System.Drawing.Point(1464, 216);
            this.pnl_Izbornik_MajstorNaTPT.Name = "pnl_Izbornik_MajstorNaTPT";
            this.pnl_Izbornik_MajstorNaTPT.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_MajstorNaTPT.TabIndex = 24;
            this.pnl_Izbornik_MajstorNaTPT.Visible = false;
            // 
            // btn_IZB_PrikaziStatistiku_MajstorNaTPT
            // 
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.Location = new System.Drawing.Point(15, 136);
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.Name = "btn_IZB_PrikaziStatistiku_MajstorNaTPT";
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.TabIndex = 3;
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.Text = "Prikaži statistiku";
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziStatistiku_MajstorNaTPT.Click += new System.EventHandler(this.btn_IZB_PrikaziStatistiku_MajstorNaTPT_Click);
            // 
            // btn_IZB_PrikaziRadniNalog_MajstorNaTPT
            // 
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.Name = "btn_IZB_PrikaziRadniNalog_MajstorNaTPT";
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.TabIndex = 2;
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.Text = "Prikaži radni nalog";
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT.Click += new System.EventHandler(this.btn_IZB_PrikaziRadniNalog_MajstorNaTPT_Click);
            // 
            // pnl_RP_PrikaziStatistiku_MajstorNaTPT
            // 
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.BackColor = System.Drawing.Color.White;
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Controls.Add(this.lb_granice_MajstorTPT);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Controls.Add(this.chart_Statistika_MajstorNaTPT);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Controls.Add(this.gb_StatistikaKubikaIKlada);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Controls.Add(this.gb_PojedinostiZaStatistiku_MajstorNaTPT);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Controls.Add(this.panel12);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Location = new System.Drawing.Point(1433, 250);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Name = "pnl_RP_PrikaziStatistiku_MajstorNaTPT";
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.TabIndex = 25;
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.Visible = false;
            // 
            // lb_granice_MajstorTPT
            // 
            this.lb_granice_MajstorTPT.AutoSize = true;
            this.lb_granice_MajstorTPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_granice_MajstorTPT.Location = new System.Drawing.Point(350, 187);
            this.lb_granice_MajstorTPT.Name = "lb_granice_MajstorTPT";
            this.lb_granice_MajstorTPT.Size = new System.Drawing.Size(0, 18);
            this.lb_granice_MajstorTPT.TabIndex = 24;
            // 
            // chart_Statistika_MajstorNaTPT
            // 
            chartArea1.Name = "ChartArea1";
            this.chart_Statistika_MajstorNaTPT.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart_Statistika_MajstorNaTPT.Legends.Add(legend1);
            this.chart_Statistika_MajstorNaTPT.Location = new System.Drawing.Point(21, 198);
            this.chart_Statistika_MajstorNaTPT.Name = "chart_Statistika_MajstorNaTPT";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Kubika";
            this.chart_Statistika_MajstorNaTPT.Series.Add(series1);
            this.chart_Statistika_MajstorNaTPT.Size = new System.Drawing.Size(882, 381);
            this.chart_Statistika_MajstorNaTPT.TabIndex = 24;
            this.chart_Statistika_MajstorNaTPT.Text = "chart1";
            this.chart_Statistika_MajstorNaTPT.Visible = false;
            // 
            // gb_StatistikaKubikaIKlada
            // 
            this.gb_StatistikaKubikaIKlada.Controls.Add(this.lb_IzrezanihKubika_MajstorNaTPT);
            this.gb_StatistikaKubikaIKlada.Controls.Add(this.lb_IzrezanihKlada_MajstorNaTPT);
            this.gb_StatistikaKubikaIKlada.Controls.Add(this.label48);
            this.gb_StatistikaKubikaIKlada.Controls.Add(this.label47);
            this.gb_StatistikaKubikaIKlada.Location = new System.Drawing.Point(506, 15);
            this.gb_StatistikaKubikaIKlada.Name = "gb_StatistikaKubikaIKlada";
            this.gb_StatistikaKubikaIKlada.Size = new System.Drawing.Size(429, 168);
            this.gb_StatistikaKubikaIKlada.TabIndex = 23;
            this.gb_StatistikaKubikaIKlada.TabStop = false;
            // 
            // lb_IzrezanihKubika_MajstorNaTPT
            // 
            this.lb_IzrezanihKubika_MajstorNaTPT.AutoSize = true;
            this.lb_IzrezanihKubika_MajstorNaTPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_IzrezanihKubika_MajstorNaTPT.ForeColor = System.Drawing.Color.Gray;
            this.lb_IzrezanihKubika_MajstorNaTPT.Location = new System.Drawing.Point(161, 78);
            this.lb_IzrezanihKubika_MajstorNaTPT.Name = "lb_IzrezanihKubika_MajstorNaTPT";
            this.lb_IzrezanihKubika_MajstorNaTPT.Size = new System.Drawing.Size(48, 25);
            this.lb_IzrezanihKubika_MajstorNaTPT.TabIndex = 25;
            this.lb_IzrezanihKubika_MajstorNaTPT.Text = "Nan";
            // 
            // lb_IzrezanihKlada_MajstorNaTPT
            // 
            this.lb_IzrezanihKlada_MajstorNaTPT.AutoSize = true;
            this.lb_IzrezanihKlada_MajstorNaTPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_IzrezanihKlada_MajstorNaTPT.ForeColor = System.Drawing.Color.Gray;
            this.lb_IzrezanihKlada_MajstorNaTPT.Location = new System.Drawing.Point(161, 34);
            this.lb_IzrezanihKlada_MajstorNaTPT.Name = "lb_IzrezanihKlada_MajstorNaTPT";
            this.lb_IzrezanihKlada_MajstorNaTPT.Size = new System.Drawing.Size(48, 25);
            this.lb_IzrezanihKlada_MajstorNaTPT.TabIndex = 24;
            this.lb_IzrezanihKlada_MajstorNaTPT.Text = "Nan";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(6, 78);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(159, 25);
            this.label48.TabIndex = 23;
            this.label48.Text = "Izrezanih kubika:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(6, 34);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(149, 25);
            this.label47.TabIndex = 22;
            this.label47.Text = "Izrezanih klada:";
            // 
            // gb_PojedinostiZaStatistiku_MajstorNaTPT
            // 
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Controls.Add(this.lb_odaberiteRadnika_TPT);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Controls.Add(this.cmbox_odaberiteRadnika_statistikaTPT);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Controls.Add(this.label46);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Controls.Add(this.cmbox_RazdobljeStatistike_MajstorNaTPT);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Controls.Add(this.btn_prikaziStatistiku_MajstorNaTPT);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Location = new System.Drawing.Point(21, 15);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Name = "gb_PojedinostiZaStatistiku_MajstorNaTPT";
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.Size = new System.Drawing.Size(431, 168);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.TabIndex = 13;
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.TabStop = false;
            // 
            // lb_odaberiteRadnika_TPT
            // 
            this.lb_odaberiteRadnika_TPT.AutoSize = true;
            this.lb_odaberiteRadnika_TPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_odaberiteRadnika_TPT.Location = new System.Drawing.Point(6, 75);
            this.lb_odaberiteRadnika_TPT.Name = "lb_odaberiteRadnika_TPT";
            this.lb_odaberiteRadnika_TPT.Size = new System.Drawing.Size(146, 20);
            this.lb_odaberiteRadnika_TPT.TabIndex = 24;
            this.lb_odaberiteRadnika_TPT.Text = "Odaberite radnika:";
            this.lb_odaberiteRadnika_TPT.Visible = false;
            // 
            // cmbox_odaberiteRadnika_statistikaTPT
            // 
            this.cmbox_odaberiteRadnika_statistikaTPT.FormattingEnabled = true;
            this.cmbox_odaberiteRadnika_statistikaTPT.Location = new System.Drawing.Point(245, 71);
            this.cmbox_odaberiteRadnika_statistikaTPT.Name = "cmbox_odaberiteRadnika_statistikaTPT";
            this.cmbox_odaberiteRadnika_statistikaTPT.Size = new System.Drawing.Size(156, 24);
            this.cmbox_odaberiteRadnika_statistikaTPT.TabIndex = 25;
            this.cmbox_odaberiteRadnika_statistikaTPT.Visible = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(6, 38);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(233, 20);
            this.label46.TabIndex = 22;
            this.label46.Text = "Odaberite razdoblje za prikaz:";
            // 
            // cmbox_RazdobljeStatistike_MajstorNaTPT
            // 
            this.cmbox_RazdobljeStatistike_MajstorNaTPT.FormattingEnabled = true;
            this.cmbox_RazdobljeStatistike_MajstorNaTPT.Location = new System.Drawing.Point(245, 34);
            this.cmbox_RazdobljeStatistike_MajstorNaTPT.Name = "cmbox_RazdobljeStatistike_MajstorNaTPT";
            this.cmbox_RazdobljeStatistike_MajstorNaTPT.Size = new System.Drawing.Size(156, 24);
            this.cmbox_RazdobljeStatistike_MajstorNaTPT.TabIndex = 23;
            // 
            // btn_prikaziStatistiku_MajstorNaTPT
            // 
            this.btn_prikaziStatistiku_MajstorNaTPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_prikaziStatistiku_MajstorNaTPT.Location = new System.Drawing.Point(10, 123);
            this.btn_prikaziStatistiku_MajstorNaTPT.Name = "btn_prikaziStatistiku_MajstorNaTPT";
            this.btn_prikaziStatistiku_MajstorNaTPT.Size = new System.Drawing.Size(391, 30);
            this.btn_prikaziStatistiku_MajstorNaTPT.TabIndex = 12;
            this.btn_prikaziStatistiku_MajstorNaTPT.Text = "Prikaži";
            this.btn_prikaziStatistiku_MajstorNaTPT.UseVisualStyleBackColor = true;
            this.btn_prikaziStatistiku_MajstorNaTPT.Click += new System.EventHandler(this.btn_prikaziStatistiku_MajstorNaTPT_Click);
            // 
            // panel12
            // 
            this.panel12.Location = new System.Drawing.Point(3, 620);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(960, 42);
            this.panel12.TabIndex = 3;
            // 
            // pnl_Izbornik_RadnikNaViličaru
            // 
            this.pnl_Izbornik_RadnikNaViličaru.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_RadnikNaViličaru.Controls.Add(this.btn_IZB_upisiPaketUSusaru);
            this.pnl_Izbornik_RadnikNaViličaru.Controls.Add(this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru);
            this.pnl_Izbornik_RadnikNaViličaru.Location = new System.Drawing.Point(1441, 243);
            this.pnl_Izbornik_RadnikNaViličaru.Name = "pnl_Izbornik_RadnikNaViličaru";
            this.pnl_Izbornik_RadnikNaViličaru.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_RadnikNaViličaru.TabIndex = 26;
            this.pnl_Izbornik_RadnikNaViličaru.Visible = false;
            // 
            // btn_IZB_upisiPaketUSusaru
            // 
            this.btn_IZB_upisiPaketUSusaru.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_upisiPaketUSusaru.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_upisiPaketUSusaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_upisiPaketUSusaru.Location = new System.Drawing.Point(15, 138);
            this.btn_IZB_upisiPaketUSusaru.Name = "btn_IZB_upisiPaketUSusaru";
            this.btn_IZB_upisiPaketUSusaru.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_upisiPaketUSusaru.TabIndex = 3;
            this.btn_IZB_upisiPaketUSusaru.Text = "Upiši paket u sušaru";
            this.btn_IZB_upisiPaketUSusaru.UseVisualStyleBackColor = false;
            this.btn_IZB_upisiPaketUSusaru.Click += new System.EventHandler(this.btn_IZB_upisiPaketUSusaru_Click);
            // 
            // btn_IZB_PrikaziRadniNalog_RadnikNaViličaru
            // 
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.Name = "btn_IZB_PrikaziRadniNalog_RadnikNaViličaru";
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.TabIndex = 2;
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.Text = "Prikaži radni nalog";
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru.Click += new System.EventHandler(this.btn_IZB_PrikaziRadniNalog_RadnikNaViličaru_Click);
            // 
            // pnl_RP_upisiPaketUSkladiste
            // 
            this.pnl_RP_upisiPaketUSkladiste.BackColor = System.Drawing.Color.White;
            this.pnl_RP_upisiPaketUSkladiste.Controls.Add(this.groupBox1);
            this.pnl_RP_upisiPaketUSkladiste.Controls.Add(this.gb_podaciOPaketu_susara);
            this.pnl_RP_upisiPaketUSkladiste.Controls.Add(this.panel13);
            this.pnl_RP_upisiPaketUSkladiste.Location = new System.Drawing.Point(1412, 266);
            this.pnl_RP_upisiPaketUSkladiste.Name = "pnl_RP_upisiPaketUSkladiste";
            this.pnl_RP_upisiPaketUSkladiste.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_upisiPaketUSkladiste.TabIndex = 27;
            this.pnl_RP_upisiPaketUSkladiste.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_IsprintajMarkicuPaketa);
            this.groupBox1.Controls.Add(this.rtb_markicaPaketa_susara);
            this.groupBox1.Location = new System.Drawing.Point(467, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 425);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Izgled markice";
            // 
            // btn_IsprintajMarkicuPaketa
            // 
            this.btn_IsprintajMarkicuPaketa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IsprintajMarkicuPaketa.Location = new System.Drawing.Point(21, 387);
            this.btn_IsprintajMarkicuPaketa.Name = "btn_IsprintajMarkicuPaketa";
            this.btn_IsprintajMarkicuPaketa.Size = new System.Drawing.Size(394, 30);
            this.btn_IsprintajMarkicuPaketa.TabIndex = 15;
            this.btn_IsprintajMarkicuPaketa.Text = "Isprintaj";
            this.btn_IsprintajMarkicuPaketa.UseVisualStyleBackColor = true;
            this.btn_IsprintajMarkicuPaketa.Click += new System.EventHandler(this.btn_IsprintajMarkicuPaketa_Click);
            // 
            // rtb_markicaPaketa_susara
            // 
            this.rtb_markicaPaketa_susara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_markicaPaketa_susara.Location = new System.Drawing.Point(21, 26);
            this.rtb_markicaPaketa_susara.Name = "rtb_markicaPaketa_susara";
            this.rtb_markicaPaketa_susara.ReadOnly = true;
            this.rtb_markicaPaketa_susara.Size = new System.Drawing.Size(394, 320);
            this.rtb_markicaPaketa_susara.TabIndex = 14;
            this.rtb_markicaPaketa_susara.Text = "";
            // 
            // gb_podaciOPaketu_susara
            // 
            this.gb_podaciOPaketu_susara.Controls.Add(this.cmbox_klasaDrveta_paket);
            this.gb_podaciOPaketu_susara.Controls.Add(this.cmbox_vrstaDrveta_paket);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label57);
            this.gb_podaciOPaketu_susara.Controls.Add(this.cmbox_odaberiteSkladiste_paket);
            this.gb_podaciOPaketu_susara.Controls.Add(this.btn_spremiPaketUSkladiste);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label56);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label55);
            this.gb_podaciOPaketu_susara.Controls.Add(this.tb_težinaPaketa);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label53);
            this.gb_podaciOPaketu_susara.Controls.Add(this.tb_količinaDasaka);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label52);
            this.gb_podaciOPaketu_susara.Controls.Add(this.tb_debljinaDaske);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label51);
            this.gb_podaciOPaketu_susara.Controls.Add(this.tb_sirinaDaske);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label50);
            this.gb_podaciOPaketu_susara.Controls.Add(this.tb_duljinaDaske);
            this.gb_podaciOPaketu_susara.Controls.Add(this.label49);
            this.gb_podaciOPaketu_susara.Location = new System.Drawing.Point(21, 15);
            this.gb_podaciOPaketu_susara.Name = "gb_podaciOPaketu_susara";
            this.gb_podaciOPaketu_susara.Size = new System.Drawing.Size(377, 427);
            this.gb_podaciOPaketu_susara.TabIndex = 13;
            this.gb_podaciOPaketu_susara.TabStop = false;
            this.gb_podaciOPaketu_susara.Text = "Unesite podatke o paketu";
            // 
            // cmbox_klasaDrveta_paket
            // 
            this.cmbox_klasaDrveta_paket.FormattingEnabled = true;
            this.cmbox_klasaDrveta_paket.Location = new System.Drawing.Point(195, 250);
            this.cmbox_klasaDrveta_paket.Name = "cmbox_klasaDrveta_paket";
            this.cmbox_klasaDrveta_paket.Size = new System.Drawing.Size(156, 24);
            this.cmbox_klasaDrveta_paket.TabIndex = 40;
            // 
            // cmbox_vrstaDrveta_paket
            // 
            this.cmbox_vrstaDrveta_paket.FormattingEnabled = true;
            this.cmbox_vrstaDrveta_paket.Location = new System.Drawing.Point(195, 218);
            this.cmbox_vrstaDrveta_paket.Name = "cmbox_vrstaDrveta_paket";
            this.cmbox_vrstaDrveta_paket.Size = new System.Drawing.Size(156, 24);
            this.cmbox_vrstaDrveta_paket.TabIndex = 39;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 325);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(158, 20);
            this.label57.TabIndex = 37;
            this.label57.Text = "Odaberite skladište:";
            // 
            // cmbox_odaberiteSkladiste_paket
            // 
            this.cmbox_odaberiteSkladiste_paket.FormattingEnabled = true;
            this.cmbox_odaberiteSkladiste_paket.Location = new System.Drawing.Point(195, 325);
            this.cmbox_odaberiteSkladiste_paket.Name = "cmbox_odaberiteSkladiste_paket";
            this.cmbox_odaberiteSkladiste_paket.Size = new System.Drawing.Size(156, 24);
            this.cmbox_odaberiteSkladiste_paket.TabIndex = 38;
            // 
            // btn_spremiPaketUSkladiste
            // 
            this.btn_spremiPaketUSkladiste.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiPaketUSkladiste.Location = new System.Drawing.Point(10, 386);
            this.btn_spremiPaketUSkladiste.Name = "btn_spremiPaketUSkladiste";
            this.btn_spremiPaketUSkladiste.Size = new System.Drawing.Size(341, 30);
            this.btn_spremiPaketUSkladiste.TabIndex = 12;
            this.btn_spremiPaketUSkladiste.Text = "Spremi";
            this.btn_spremiPaketUSkladiste.UseVisualStyleBackColor = true;
            this.btn_spremiPaketUSkladiste.Click += new System.EventHandler(this.btn_spremiPaketUSkladiste_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(6, 255);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(107, 20);
            this.label56.TabIndex = 35;
            this.label56.Text = "Klasa drveta:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(6, 218);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(105, 20);
            this.label55.TabIndex = 33;
            this.label55.Text = "Vrsta drveta:";
            // 
            // tb_težinaPaketa
            // 
            this.tb_težinaPaketa.Location = new System.Drawing.Point(172, 184);
            this.tb_težinaPaketa.Name = "tb_težinaPaketa";
            this.tb_težinaPaketa.Size = new System.Drawing.Size(179, 22);
            this.tb_težinaPaketa.TabIndex = 32;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(6, 184);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(118, 20);
            this.label53.TabIndex = 31;
            this.label53.Text = "Težina paketa:";
            // 
            // tb_količinaDasaka
            // 
            this.tb_količinaDasaka.Location = new System.Drawing.Point(172, 129);
            this.tb_količinaDasaka.Name = "tb_količinaDasaka";
            this.tb_količinaDasaka.Size = new System.Drawing.Size(179, 22);
            this.tb_količinaDasaka.TabIndex = 30;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(6, 129);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(131, 20);
            this.label52.TabIndex = 29;
            this.label52.Text = "Količina dasaka:";
            // 
            // tb_debljinaDaske
            // 
            this.tb_debljinaDaske.Location = new System.Drawing.Point(172, 97);
            this.tb_debljinaDaske.Name = "tb_debljinaDaske";
            this.tb_debljinaDaske.Size = new System.Drawing.Size(179, 22);
            this.tb_debljinaDaske.TabIndex = 28;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(6, 97);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(162, 20);
            this.label51.TabIndex = 27;
            this.label51.Text = "Debljina daske [cm]:";
            // 
            // tb_sirinaDaske
            // 
            this.tb_sirinaDaske.Location = new System.Drawing.Point(172, 66);
            this.tb_sirinaDaske.Name = "tb_sirinaDaske";
            this.tb_sirinaDaske.Size = new System.Drawing.Size(179, 22);
            this.tb_sirinaDaske.TabIndex = 26;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(6, 66);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(144, 20);
            this.label50.TabIndex = 25;
            this.label50.Text = "Širina daske [cm]:";
            // 
            // tb_duljinaDaske
            // 
            this.tb_duljinaDaske.Location = new System.Drawing.Point(172, 33);
            this.tb_duljinaDaske.Name = "tb_duljinaDaske";
            this.tb_duljinaDaske.Size = new System.Drawing.Size(179, 22);
            this.tb_duljinaDaske.TabIndex = 24;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(6, 33);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(153, 20);
            this.label49.TabIndex = 23;
            this.label49.Text = "Duljina daske [cm]:";
            // 
            // panel13
            // 
            this.panel13.Location = new System.Drawing.Point(3, 620);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(960, 42);
            this.panel13.TabIndex = 3;
            // 
            // printDialog_MarkicaPaketa_Susara
            // 
            this.printDialog_MarkicaPaketa_Susara.Document = this.printDocument_MarkicaPaketa_Susara;
            this.printDialog_MarkicaPaketa_Susara.UseEXDialog = true;
            // 
            // printDocument_MarkicaPaketa_Susara
            // 
            this.printDocument_MarkicaPaketa_Susara.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_MarkicaPaketa_Susara_PrintPage);
            // 
            // pnl_Izbornik_PoslovodaElementare
            // 
            this.pnl_Izbornik_PoslovodaElementare.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_PoslovodaElementare.Controls.Add(this.btn_IZB_prikaziPopratnice);
            this.pnl_Izbornik_PoslovodaElementare.Controls.Add(this.btn_IZB_pretraziSkladiste_elem);
            this.pnl_Izbornik_PoslovodaElementare.Controls.Add(this.btn_IZB_NapisiPopratnicu);
            this.pnl_Izbornik_PoslovodaElementare.Controls.Add(this.btn_IZB_prikaziRadniNalogPoslovodaElementare);
            this.pnl_Izbornik_PoslovodaElementare.Controls.Add(this.btn_IZB_NapisiRadniNalog_Elementara);
            this.pnl_Izbornik_PoslovodaElementare.Location = new System.Drawing.Point(1363, 314);
            this.pnl_Izbornik_PoslovodaElementare.Name = "pnl_Izbornik_PoslovodaElementare";
            this.pnl_Izbornik_PoslovodaElementare.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_PoslovodaElementare.TabIndex = 28;
            this.pnl_Izbornik_PoslovodaElementare.Visible = false;
            // 
            // btn_IZB_prikaziPopratnice
            // 
            this.btn_IZB_prikaziPopratnice.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziPopratnice.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziPopratnice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziPopratnice.Location = new System.Drawing.Point(15, 240);
            this.btn_IZB_prikaziPopratnice.Name = "btn_IZB_prikaziPopratnice";
            this.btn_IZB_prikaziPopratnice.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_prikaziPopratnice.TabIndex = 7;
            this.btn_IZB_prikaziPopratnice.Text = "Pretraži popratnice";
            this.btn_IZB_prikaziPopratnice.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziPopratnice.Click += new System.EventHandler(this.btn_IZB_prikaziPopratnice_Click);
            // 
            // btn_IZB_pretraziSkladiste_elem
            // 
            this.btn_IZB_pretraziSkladiste_elem.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_pretraziSkladiste_elem.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_pretraziSkladiste_elem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_pretraziSkladiste_elem.Location = new System.Drawing.Point(15, 292);
            this.btn_IZB_pretraziSkladiste_elem.Name = "btn_IZB_pretraziSkladiste_elem";
            this.btn_IZB_pretraziSkladiste_elem.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_pretraziSkladiste_elem.TabIndex = 5;
            this.btn_IZB_pretraziSkladiste_elem.Text = "Pretraži skladište";
            this.btn_IZB_pretraziSkladiste_elem.UseVisualStyleBackColor = false;
            this.btn_IZB_pretraziSkladiste_elem.Click += new System.EventHandler(this.btn_IZB_pretraziSkladiste_elem_Click);
            // 
            // btn_IZB_NapisiPopratnicu
            // 
            this.btn_IZB_NapisiPopratnicu.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_NapisiPopratnicu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_NapisiPopratnicu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_NapisiPopratnicu.Location = new System.Drawing.Point(15, 189);
            this.btn_IZB_NapisiPopratnicu.Name = "btn_IZB_NapisiPopratnicu";
            this.btn_IZB_NapisiPopratnicu.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_NapisiPopratnicu.TabIndex = 4;
            this.btn_IZB_NapisiPopratnicu.Text = "Napiši popratnicu";
            this.btn_IZB_NapisiPopratnicu.UseVisualStyleBackColor = false;
            this.btn_IZB_NapisiPopratnicu.Click += new System.EventHandler(this.btn_NapisiPopratnicu_Click);
            // 
            // btn_IZB_prikaziRadniNalogPoslovodaElementare
            // 
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.Location = new System.Drawing.Point(16, 138);
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.Name = "btn_IZB_prikaziRadniNalogPoslovodaElementare";
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.Size = new System.Drawing.Size(284, 42);
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.TabIndex = 3;
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.Text = "Prikaži radni nalog";
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziRadniNalogPoslovodaElementare.Click += new System.EventHandler(this.btn_IZB_prikaziRadniNalogPoslovodaElementare_Click);
            // 
            // btn_IZB_NapisiRadniNalog_Elementara
            // 
            this.btn_IZB_NapisiRadniNalog_Elementara.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_NapisiRadniNalog_Elementara.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_NapisiRadniNalog_Elementara.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_NapisiRadniNalog_Elementara.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_NapisiRadniNalog_Elementara.Name = "btn_IZB_NapisiRadniNalog_Elementara";
            this.btn_IZB_NapisiRadniNalog_Elementara.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_NapisiRadniNalog_Elementara.TabIndex = 2;
            this.btn_IZB_NapisiRadniNalog_Elementara.Text = "Napiši radni nalog";
            this.btn_IZB_NapisiRadniNalog_Elementara.UseVisualStyleBackColor = false;
            this.btn_IZB_NapisiRadniNalog_Elementara.Click += new System.EventHandler(this.btn_IZB_NapisiRadniNalog_Elementara_Click);
            // 
            // pnl_Izbornik_RadnikNaViselisnomRezacu
            // 
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Controls.Add(this.btn_IZB_prikaziRadniNalog_Viselisni);
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Controls.Add(this.btn_IZB_upisiPaket_Elementara);
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Location = new System.Drawing.Point(1420, 258);
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Name = "pnl_Izbornik_RadnikNaViselisnomRezacu";
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.TabIndex = 29;
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.Visible = false;
            // 
            // btn_IZB_prikaziRadniNalog_Viselisni
            // 
            this.btn_IZB_prikaziRadniNalog_Viselisni.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_prikaziRadniNalog_Viselisni.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_prikaziRadniNalog_Viselisni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_prikaziRadniNalog_Viselisni.Location = new System.Drawing.Point(15, 136);
            this.btn_IZB_prikaziRadniNalog_Viselisni.Name = "btn_IZB_prikaziRadniNalog_Viselisni";
            this.btn_IZB_prikaziRadniNalog_Viselisni.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_prikaziRadniNalog_Viselisni.TabIndex = 3;
            this.btn_IZB_prikaziRadniNalog_Viselisni.Text = "Prikaži radni nalog";
            this.btn_IZB_prikaziRadniNalog_Viselisni.UseVisualStyleBackColor = false;
            this.btn_IZB_prikaziRadniNalog_Viselisni.Click += new System.EventHandler(this.btn_IZB_prikaziRadniNalog_Viselisni_Click);
            // 
            // btn_IZB_upisiPaket_Elementara
            // 
            this.btn_IZB_upisiPaket_Elementara.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_upisiPaket_Elementara.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_upisiPaket_Elementara.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_upisiPaket_Elementara.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_upisiPaket_Elementara.Name = "btn_IZB_upisiPaket_Elementara";
            this.btn_IZB_upisiPaket_Elementara.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_upisiPaket_Elementara.TabIndex = 2;
            this.btn_IZB_upisiPaket_Elementara.Text = "Upiši paket";
            this.btn_IZB_upisiPaket_Elementara.UseVisualStyleBackColor = false;
            this.btn_IZB_upisiPaket_Elementara.Click += new System.EventHandler(this.btn_IZB_upisiPaket_Elementara_Click);
            // 
            // pnl_RP_upisiPaket_RadnikNaViselisnom
            // 
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.BackColor = System.Drawing.Color.White;
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Controls.Add(this.panel2);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Controls.Add(this.groupBox3);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Controls.Add(this.panel14);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Location = new System.Drawing.Point(1450, 232);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Name = "pnl_RP_upisiPaket_RadnikNaViselisnom";
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.TabIndex = 30;
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.panel16);
            this.panel2.Location = new System.Drawing.Point(8, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(953, 617);
            this.panel2.TabIndex = 31;
            this.panel2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label66);
            this.groupBox2.Location = new System.Drawing.Point(21, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(383, 189);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Unesite podatke o paketu";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(25, 82);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(158, 20);
            this.label58.TabIndex = 39;
            this.label58.Text = "Odaberite skladište:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(195, 82);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(156, 24);
            this.comboBox1.TabIndex = 40;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(29, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(322, 30);
            this.button1.TabIndex = 12;
            this.button1.Text = "Upiši";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(140, 34);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(211, 22);
            this.textBox1.TabIndex = 24;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(25, 34);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(99, 20);
            this.label66.TabIndex = 23;
            this.label66.Text = "Broj paketa:";
            // 
            // panel16
            // 
            this.panel16.Location = new System.Drawing.Point(3, 620);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(960, 42);
            this.panel16.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.cmbox_odaberiteSkladiste_viselisni);
            this.groupBox3.Controls.Add(this.btn_upisiPaket_viselisni);
            this.groupBox3.Controls.Add(this.tb_brojPaketa_viselisni);
            this.groupBox3.Controls.Add(this.label64);
            this.groupBox3.Location = new System.Drawing.Point(21, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(383, 189);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Unesite podatke o paketu";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(25, 82);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(158, 20);
            this.label54.TabIndex = 39;
            this.label54.Text = "Odaberite skladište:";
            // 
            // cmbox_odaberiteSkladiste_viselisni
            // 
            this.cmbox_odaberiteSkladiste_viselisni.FormattingEnabled = true;
            this.cmbox_odaberiteSkladiste_viselisni.Location = new System.Drawing.Point(195, 82);
            this.cmbox_odaberiteSkladiste_viselisni.Name = "cmbox_odaberiteSkladiste_viselisni";
            this.cmbox_odaberiteSkladiste_viselisni.Size = new System.Drawing.Size(156, 24);
            this.cmbox_odaberiteSkladiste_viselisni.TabIndex = 40;
            // 
            // btn_upisiPaket_viselisni
            // 
            this.btn_upisiPaket_viselisni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_upisiPaket_viselisni.Location = new System.Drawing.Point(29, 138);
            this.btn_upisiPaket_viselisni.Name = "btn_upisiPaket_viselisni";
            this.btn_upisiPaket_viselisni.Size = new System.Drawing.Size(322, 30);
            this.btn_upisiPaket_viselisni.TabIndex = 12;
            this.btn_upisiPaket_viselisni.Text = "Upiši";
            this.btn_upisiPaket_viselisni.UseVisualStyleBackColor = true;
            this.btn_upisiPaket_viselisni.Click += new System.EventHandler(this.btn_upisiPaket_viselisni_Click);
            // 
            // tb_brojPaketa_viselisni
            // 
            this.tb_brojPaketa_viselisni.Location = new System.Drawing.Point(140, 34);
            this.tb_brojPaketa_viselisni.Name = "tb_brojPaketa_viselisni";
            this.tb_brojPaketa_viselisni.Size = new System.Drawing.Size(211, 22);
            this.tb_brojPaketa_viselisni.TabIndex = 24;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(25, 34);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(99, 20);
            this.label64.TabIndex = 23;
            this.label64.Text = "Broj paketa:";
            // 
            // panel14
            // 
            this.panel14.Location = new System.Drawing.Point(3, 620);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(960, 42);
            this.panel14.TabIndex = 3;
            // 
            // pnl_Izbornik_RadnikSGotovomRobom
            // 
            this.pnl_Izbornik_RadnikSGotovomRobom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.pnl_Izbornik_RadnikSGotovomRobom.Controls.Add(this.btn_IZB_PrikaziRadniNalog_GotovaRoba);
            this.pnl_Izbornik_RadnikSGotovomRobom.Controls.Add(this.btn_IZB_upisiPaket_GotovaRoba);
            this.pnl_Izbornik_RadnikSGotovomRobom.Location = new System.Drawing.Point(1403, 276);
            this.pnl_Izbornik_RadnikSGotovomRobom.Name = "pnl_Izbornik_RadnikSGotovomRobom";
            this.pnl_Izbornik_RadnikSGotovomRobom.Size = new System.Drawing.Size(315, 660);
            this.pnl_Izbornik_RadnikSGotovomRobom.TabIndex = 31;
            this.pnl_Izbornik_RadnikSGotovomRobom.Visible = false;
            // 
            // btn_IZB_PrikaziRadniNalog_GotovaRoba
            // 
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.Location = new System.Drawing.Point(15, 135);
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.Name = "btn_IZB_PrikaziRadniNalog_GotovaRoba";
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.TabIndex = 3;
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.Text = "Prikaži radni nalog";
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.UseVisualStyleBackColor = false;
            this.btn_IZB_PrikaziRadniNalog_GotovaRoba.Click += new System.EventHandler(this.btn_IZB_PrikaziRadniNalog_GotovaRoba_Click);
            // 
            // btn_IZB_upisiPaket_GotovaRoba
            // 
            this.btn_IZB_upisiPaket_GotovaRoba.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_IZB_upisiPaket_GotovaRoba.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_IZB_upisiPaket_GotovaRoba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_IZB_upisiPaket_GotovaRoba.Location = new System.Drawing.Point(15, 87);
            this.btn_IZB_upisiPaket_GotovaRoba.Name = "btn_IZB_upisiPaket_GotovaRoba";
            this.btn_IZB_upisiPaket_GotovaRoba.Size = new System.Drawing.Size(285, 42);
            this.btn_IZB_upisiPaket_GotovaRoba.TabIndex = 2;
            this.btn_IZB_upisiPaket_GotovaRoba.Text = "Upiši paket";
            this.btn_IZB_upisiPaket_GotovaRoba.UseVisualStyleBackColor = false;
            this.btn_IZB_upisiPaket_GotovaRoba.Click += new System.EventHandler(this.btn_IZB_upisiPaket_GotovaRoba_Click);
            // 
            // pnl_RP_napisiPopratnicu
            // 
            this.pnl_RP_napisiPopratnicu.BackColor = System.Drawing.Color.White;
            this.pnl_RP_napisiPopratnicu.Controls.Add(this.gb_upisitePakete_popratnica);
            this.pnl_RP_napisiPopratnicu.Controls.Add(this.gb_izgledPopratnice_popratnica);
            this.pnl_RP_napisiPopratnicu.Controls.Add(this.panel15);
            this.pnl_RP_napisiPopratnicu.Controls.Add(this.gb_podaciOPopratnici_popratnica);
            this.pnl_RP_napisiPopratnicu.Location = new System.Drawing.Point(1391, 286);
            this.pnl_RP_napisiPopratnicu.Name = "pnl_RP_napisiPopratnicu";
            this.pnl_RP_napisiPopratnicu.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_napisiPopratnicu.TabIndex = 32;
            this.pnl_RP_napisiPopratnicu.Visible = false;
            // 
            // gb_upisitePakete_popratnica
            // 
            this.gb_upisitePakete_popratnica.Controls.Add(this.dgv_brojPaketa_popratnica);
            this.gb_upisitePakete_popratnica.Controls.Add(this.btn_spremiNovuPopratnicu);
            this.gb_upisitePakete_popratnica.Location = new System.Drawing.Point(24, 272);
            this.gb_upisitePakete_popratnica.Name = "gb_upisitePakete_popratnica";
            this.gb_upisitePakete_popratnica.Size = new System.Drawing.Size(340, 272);
            this.gb_upisitePakete_popratnica.TabIndex = 14;
            this.gb_upisitePakete_popratnica.TabStop = false;
            this.gb_upisitePakete_popratnica.Text = "Upišite pakete";
            // 
            // dgv_brojPaketa_popratnica
            // 
            this.dgv_brojPaketa_popratnica.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_brojPaketa_popratnica.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_BrojPaketa});
            this.dgv_brojPaketa_popratnica.Location = new System.Drawing.Point(18, 32);
            this.dgv_brojPaketa_popratnica.Name = "dgv_brojPaketa_popratnica";
            this.dgv_brojPaketa_popratnica.RowTemplate.Height = 24;
            this.dgv_brojPaketa_popratnica.Size = new System.Drawing.Size(147, 183);
            this.dgv_brojPaketa_popratnica.TabIndex = 0;
            // 
            // col_BrojPaketa
            // 
            this.col_BrojPaketa.HeaderText = "Broj paketa";
            this.col_BrojPaketa.Name = "col_BrojPaketa";
            // 
            // btn_spremiNovuPopratnicu
            // 
            this.btn_spremiNovuPopratnicu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiNovuPopratnicu.Location = new System.Drawing.Point(18, 235);
            this.btn_spremiNovuPopratnicu.Name = "btn_spremiNovuPopratnicu";
            this.btn_spremiNovuPopratnicu.Size = new System.Drawing.Size(308, 30);
            this.btn_spremiNovuPopratnicu.TabIndex = 12;
            this.btn_spremiNovuPopratnicu.Text = "Spremi";
            this.btn_spremiNovuPopratnicu.UseVisualStyleBackColor = true;
            this.btn_spremiNovuPopratnicu.Click += new System.EventHandler(this.btn_spremiNovuPopratnicu_Click);
            // 
            // gb_izgledPopratnice_popratnica
            // 
            this.gb_izgledPopratnice_popratnica.Controls.Add(this.btn_ispisiPopratnicu);
            this.gb_izgledPopratnice_popratnica.Controls.Add(this.rtb_izgledPopratnice);
            this.gb_izgledPopratnice_popratnica.Location = new System.Drawing.Point(382, 17);
            this.gb_izgledPopratnice_popratnica.Name = "gb_izgledPopratnice_popratnica";
            this.gb_izgledPopratnice_popratnica.Size = new System.Drawing.Size(555, 527);
            this.gb_izgledPopratnice_popratnica.TabIndex = 13;
            this.gb_izgledPopratnice_popratnica.TabStop = false;
            this.gb_izgledPopratnice_popratnica.Text = "Izgled popratnice";
            // 
            // btn_ispisiPopratnicu
            // 
            this.btn_ispisiPopratnicu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ispisiPopratnicu.Location = new System.Drawing.Point(127, 490);
            this.btn_ispisiPopratnicu.Name = "btn_ispisiPopratnicu";
            this.btn_ispisiPopratnicu.Size = new System.Drawing.Size(308, 30);
            this.btn_ispisiPopratnicu.TabIndex = 13;
            this.btn_ispisiPopratnicu.Text = "Ispiši";
            this.btn_ispisiPopratnicu.UseVisualStyleBackColor = true;
            this.btn_ispisiPopratnicu.Click += new System.EventHandler(this.btn_ispisiPopratnicu_Click);
            // 
            // rtb_izgledPopratnice
            // 
            this.rtb_izgledPopratnice.AcceptsTab = true;
            this.rtb_izgledPopratnice.Location = new System.Drawing.Point(6, 24);
            this.rtb_izgledPopratnice.Name = "rtb_izgledPopratnice";
            this.rtb_izgledPopratnice.ReadOnly = true;
            this.rtb_izgledPopratnice.Size = new System.Drawing.Size(543, 455);
            this.rtb_izgledPopratnice.TabIndex = 0;
            this.rtb_izgledPopratnice.Text = "";
            // 
            // panel15
            // 
            this.panel15.Location = new System.Drawing.Point(3, 620);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(960, 42);
            this.panel15.TabIndex = 3;
            // 
            // gb_podaciOPopratnici_popratnica
            // 
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.tb_uplatnicaNalog_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label63);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.tb_mjestoIstovara_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label65);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.cmbox_odaberitePrikolicu_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label62);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.tb_kupac_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label59);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.tb_mjestoUtovara_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label60);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.cmbox_odaberiteKamion_popratnica);
            this.gb_podaciOPopratnici_popratnica.Controls.Add(this.label61);
            this.gb_podaciOPopratnici_popratnica.Location = new System.Drawing.Point(24, 17);
            this.gb_podaciOPopratnici_popratnica.Name = "gb_podaciOPopratnici_popratnica";
            this.gb_podaciOPopratnici_popratnica.Size = new System.Drawing.Size(340, 245);
            this.gb_podaciOPopratnici_popratnica.TabIndex = 8;
            this.gb_podaciOPopratnici_popratnica.TabStop = false;
            this.gb_podaciOPopratnici_popratnica.Text = "Podaci o popratnici";
            // 
            // tb_uplatnicaNalog_popratnica
            // 
            this.tb_uplatnicaNalog_popratnica.Location = new System.Drawing.Point(170, 200);
            this.tb_uplatnicaNalog_popratnica.Name = "tb_uplatnicaNalog_popratnica";
            this.tb_uplatnicaNalog_popratnica.Size = new System.Drawing.Size(156, 22);
            this.tb_uplatnicaNalog_popratnica.TabIndex = 23;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(13, 201);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(124, 20);
            this.label63.TabIndex = 22;
            this.label63.Text = "Uplatnica/nalog";
            // 
            // tb_mjestoIstovara_popratnica
            // 
            this.tb_mjestoIstovara_popratnica.Location = new System.Drawing.Point(170, 133);
            this.tb_mjestoIstovara_popratnica.Name = "tb_mjestoIstovara_popratnica";
            this.tb_mjestoIstovara_popratnica.Size = new System.Drawing.Size(156, 22);
            this.tb_mjestoIstovara_popratnica.TabIndex = 21;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(13, 134);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(128, 20);
            this.label65.TabIndex = 20;
            this.label65.Text = "Mjesto istovara:";
            // 
            // cmbox_odaberitePrikolicu_popratnica
            // 
            this.cmbox_odaberitePrikolicu_popratnica.FormattingEnabled = true;
            this.cmbox_odaberitePrikolicu_popratnica.Location = new System.Drawing.Point(170, 66);
            this.cmbox_odaberitePrikolicu_popratnica.Name = "cmbox_odaberitePrikolicu_popratnica";
            this.cmbox_odaberitePrikolicu_popratnica.Size = new System.Drawing.Size(131, 24);
            this.cmbox_odaberitePrikolicu_popratnica.TabIndex = 17;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(11, 67);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(154, 20);
            this.label62.TabIndex = 16;
            this.label62.Text = "Odaberite prikolicu:";
            // 
            // tb_kupac_popratnica
            // 
            this.tb_kupac_popratnica.Location = new System.Drawing.Point(170, 168);
            this.tb_kupac_popratnica.Name = "tb_kupac_popratnica";
            this.tb_kupac_popratnica.Size = new System.Drawing.Size(156, 22);
            this.tb_kupac_popratnica.TabIndex = 15;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(13, 169);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(61, 20);
            this.label59.TabIndex = 14;
            this.label59.Text = "Kupac:";
            // 
            // tb_mjestoUtovara_popratnica
            // 
            this.tb_mjestoUtovara_popratnica.Location = new System.Drawing.Point(170, 100);
            this.tb_mjestoUtovara_popratnica.Name = "tb_mjestoUtovara_popratnica";
            this.tb_mjestoUtovara_popratnica.Size = new System.Drawing.Size(156, 22);
            this.tb_mjestoUtovara_popratnica.TabIndex = 13;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(11, 102);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(124, 20);
            this.label60.TabIndex = 12;
            this.label60.Text = "Mjesto utovara:";
            // 
            // cmbox_odaberiteKamion_popratnica
            // 
            this.cmbox_odaberiteKamion_popratnica.FormattingEnabled = true;
            this.cmbox_odaberiteKamion_popratnica.Location = new System.Drawing.Point(170, 31);
            this.cmbox_odaberiteKamion_popratnica.Name = "cmbox_odaberiteKamion_popratnica";
            this.cmbox_odaberiteKamion_popratnica.Size = new System.Drawing.Size(131, 24);
            this.cmbox_odaberiteKamion_popratnica.TabIndex = 11;
            this.cmbox_odaberiteKamion_popratnica.SelectedIndexChanged += new System.EventHandler(this.cmbox_odaberiteKamion_popratnica_SelectedIndexChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(11, 32);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(145, 20);
            this.label61.TabIndex = 6;
            this.label61.Text = "Odaberite kamion:";
            // 
            // printDialog_Popratnica
            // 
            this.printDialog_Popratnica.Document = this.printDocument_popratnica;
            this.printDialog_Popratnica.UseEXDialog = true;
            // 
            // printDocument_popratnica
            // 
            this.printDocument_popratnica.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_popratnica_PrintPage);
            // 
            // pnl_RP_pretraziSkladiste
            // 
            this.pnl_RP_pretraziSkladiste.BackColor = System.Drawing.Color.White;
            this.pnl_RP_pretraziSkladiste.Controls.Add(this.gb_prog_rezultati);
            this.pnl_RP_pretraziSkladiste.Controls.Add(this.groupBox4);
            this.pnl_RP_pretraziSkladiste.Controls.Add(this.groupBox5);
            this.pnl_RP_pretraziSkladiste.Controls.Add(this.panel20);
            this.pnl_RP_pretraziSkladiste.Location = new System.Drawing.Point(1381, 297);
            this.pnl_RP_pretraziSkladiste.Name = "pnl_RP_pretraziSkladiste";
            this.pnl_RP_pretraziSkladiste.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_pretraziSkladiste.TabIndex = 33;
            this.pnl_RP_pretraziSkladiste.Visible = false;
            // 
            // gb_prog_rezultati
            // 
            this.gb_prog_rezultati.Controls.Add(this.dgv_prog_rezultati);
            this.gb_prog_rezultati.Location = new System.Drawing.Point(23, 138);
            this.gb_prog_rezultati.Name = "gb_prog_rezultati";
            this.gb_prog_rezultati.Size = new System.Drawing.Size(914, 455);
            this.gb_prog_rezultati.TabIndex = 27;
            this.gb_prog_rezultati.TabStop = false;
            this.gb_prog_rezultati.Text = "Rezultati pretrage";
            this.gb_prog_rezultati.Visible = false;
            // 
            // dgv_prog_rezultati
            // 
            this.dgv_prog_rezultati.AllowUserToAddRows = false;
            this.dgv_prog_rezultati.AllowUserToDeleteRows = false;
            this.dgv_prog_rezultati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prog_rezultati.Location = new System.Drawing.Point(6, 32);
            this.dgv_prog_rezultati.Name = "dgv_prog_rezultati";
            this.dgv_prog_rezultati.ReadOnly = true;
            this.dgv_prog_rezultati.RowTemplate.Height = 24;
            this.dgv_prog_rezultati.Size = new System.Drawing.Size(902, 407);
            this.dgv_prog_rezultati.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lb_prog_skl_spec);
            this.groupBox4.Controls.Add(this.cmbox_prog_skla_spec);
            this.groupBox4.Controls.Add(this.lb_prog_naziv);
            this.groupBox4.Controls.Add(this.btn_prog_Pretrazi);
            this.groupBox4.Controls.Add(this.tb_prog_nazivi);
            this.groupBox4.Location = new System.Drawing.Point(364, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(573, 111);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Odaberite detalje";
            // 
            // lb_prog_skl_spec
            // 
            this.lb_prog_skl_spec.AutoSize = true;
            this.lb_prog_skl_spec.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_prog_skl_spec.Location = new System.Drawing.Point(18, 34);
            this.lb_prog_skl_spec.Name = "lb_prog_skl_spec";
            this.lb_prog_skl_spec.Size = new System.Drawing.Size(82, 20);
            this.lb_prog_skl_spec.TabIndex = 43;
            this.lb_prog_skl_spec.Text = "Skladište:";
            // 
            // cmbox_prog_skla_spec
            // 
            this.cmbox_prog_skla_spec.FormattingEnabled = true;
            this.cmbox_prog_skla_spec.Location = new System.Drawing.Point(195, 34);
            this.cmbox_prog_skla_spec.Name = "cmbox_prog_skla_spec";
            this.cmbox_prog_skla_spec.Size = new System.Drawing.Size(156, 24);
            this.cmbox_prog_skla_spec.TabIndex = 44;
            // 
            // lb_prog_naziv
            // 
            this.lb_prog_naziv.AutoSize = true;
            this.lb_prog_naziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_prog_naziv.Location = new System.Drawing.Point(18, 64);
            this.lb_prog_naziv.Name = "lb_prog_naziv";
            this.lb_prog_naziv.Size = new System.Drawing.Size(89, 20);
            this.lb_prog_naziv.TabIndex = 23;
            this.lb_prog_naziv.Text = "Broj klade:";
            // 
            // btn_prog_Pretrazi
            // 
            this.btn_prog_Pretrazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_prog_Pretrazi.Location = new System.Drawing.Point(419, 58);
            this.btn_prog_Pretrazi.Name = "btn_prog_Pretrazi";
            this.btn_prog_Pretrazi.Size = new System.Drawing.Size(148, 30);
            this.btn_prog_Pretrazi.TabIndex = 12;
            this.btn_prog_Pretrazi.Text = "Pretraži";
            this.btn_prog_Pretrazi.UseVisualStyleBackColor = true;
            this.btn_prog_Pretrazi.Click += new System.EventHandler(this.btn_prog_Pretrazi_Click);
            // 
            // tb_prog_nazivi
            // 
            this.tb_prog_nazivi.Location = new System.Drawing.Point(181, 64);
            this.tb_prog_nazivi.Name = "tb_prog_nazivi";
            this.tb_prog_nazivi.Size = new System.Drawing.Size(170, 22);
            this.tb_prog_nazivi.TabIndex = 24;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.cmbox_PertraziPo_pp);
            this.groupBox5.Location = new System.Drawing.Point(23, 15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(320, 111);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(15, 34);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(96, 20);
            this.label67.TabIndex = 41;
            this.label67.Text = "Pretraži po:";
            // 
            // cmbox_PertraziPo_pp
            // 
            this.cmbox_PertraziPo_pp.FormattingEnabled = true;
            this.cmbox_PertraziPo_pp.Location = new System.Drawing.Point(143, 30);
            this.cmbox_PertraziPo_pp.Name = "cmbox_PertraziPo_pp";
            this.cmbox_PertraziPo_pp.Size = new System.Drawing.Size(156, 24);
            this.cmbox_PertraziPo_pp.TabIndex = 42;
            this.cmbox_PertraziPo_pp.SelectedIndexChanged += new System.EventHandler(this.cmbox_PertraziPo_pp_SelectedIndexChanged);
            // 
            // panel20
            // 
            this.panel20.Location = new System.Drawing.Point(3, 620);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(960, 42);
            this.panel20.TabIndex = 3;
            // 
            // pnl_RP_izmjeniPodatkeOZaposlenima
            // 
            this.pnl_RP_izmjeniPodatkeOZaposlenima.BackColor = System.Drawing.Color.White;
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.groupBox11);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.panel18);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.btn_iz_spremiIzmjeneKorRacuna);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.groupBox7);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.gbox_iz_spol);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.gb_iz_dodatneInfo);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Controls.Add(this.gb_iz_osobniPodaci);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Location = new System.Drawing.Point(1341, 334);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Name = "pnl_RP_izmjeniPodatkeOZaposlenima";
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_izmjeniPodatkeOZaposlenima.TabIndex = 34;
            this.pnl_RP_izmjeniPodatkeOZaposlenima.Visible = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label78);
            this.groupBox11.Controls.Add(this.cmbox_iz_odaberiteZaposlenika);
            this.groupBox11.Location = new System.Drawing.Point(23, 32);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(452, 102);
            this.groupBox11.TabIndex = 13;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Zaposlenik";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(27, 43);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(180, 20);
            this.label78.TabIndex = 6;
            this.label78.Text = "Odaberite zaposlenika:";
            // 
            // cmbox_iz_odaberiteZaposlenika
            // 
            this.cmbox_iz_odaberiteZaposlenika.FormattingEnabled = true;
            this.cmbox_iz_odaberiteZaposlenika.Location = new System.Drawing.Point(213, 43);
            this.cmbox_iz_odaberiteZaposlenika.Name = "cmbox_iz_odaberiteZaposlenika";
            this.cmbox_iz_odaberiteZaposlenika.Size = new System.Drawing.Size(212, 24);
            this.cmbox_iz_odaberiteZaposlenika.TabIndex = 0;
            this.cmbox_iz_odaberiteZaposlenika.SelectedIndexChanged += new System.EventHandler(this.cmbox_iz_odaberiteZaposlenika_SelectedIndexChanged);
            // 
            // panel18
            // 
            this.panel18.Location = new System.Drawing.Point(3, 620);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(960, 42);
            this.panel18.TabIndex = 3;
            // 
            // btn_iz_spremiIzmjeneKorRacuna
            // 
            this.btn_iz_spremiIzmjeneKorRacuna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_iz_spremiIzmjeneKorRacuna.Location = new System.Drawing.Point(778, 575);
            this.btn_iz_spremiIzmjeneKorRacuna.Name = "btn_iz_spremiIzmjeneKorRacuna";
            this.btn_iz_spremiIzmjeneKorRacuna.Size = new System.Drawing.Size(158, 30);
            this.btn_iz_spremiIzmjeneKorRacuna.TabIndex = 12;
            this.btn_iz_spremiIzmjeneKorRacuna.Text = "Spremi";
            this.btn_iz_spremiIzmjeneKorRacuna.UseVisualStyleBackColor = true;
            this.btn_iz_spremiIzmjeneKorRacuna.Click += new System.EventHandler(this.btn_iz_spremiIzmjeneKorRacuna_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label70);
            this.groupBox7.Controls.Add(this.cmbox_iz_pozRada);
            this.groupBox7.Location = new System.Drawing.Point(498, 285);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(441, 102);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Poslovni detalji";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(8, 42);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(111, 20);
            this.label70.TabIndex = 6;
            this.label70.Text = "Pozicija rada:";
            // 
            // cmbox_iz_pozRada
            // 
            this.cmbox_iz_pozRada.FormattingEnabled = true;
            this.cmbox_iz_pozRada.Location = new System.Drawing.Point(197, 42);
            this.cmbox_iz_pozRada.Name = "cmbox_iz_pozRada";
            this.cmbox_iz_pozRada.Size = new System.Drawing.Size(212, 24);
            this.cmbox_iz_pozRada.TabIndex = 0;
            // 
            // gbox_iz_spol
            // 
            this.gbox_iz_spol.Controls.Add(this.rb_iz_zensko);
            this.gbox_iz_spol.Controls.Add(this.rb_iz_musko);
            this.gbox_iz_spol.Location = new System.Drawing.Point(498, 152);
            this.gbox_iz_spol.Name = "gbox_iz_spol";
            this.gbox_iz_spol.Size = new System.Drawing.Size(174, 119);
            this.gbox_iz_spol.TabIndex = 11;
            this.gbox_iz_spol.TabStop = false;
            this.gbox_iz_spol.Text = "Spol";
            // 
            // rb_iz_zensko
            // 
            this.rb_iz_zensko.AutoSize = true;
            this.rb_iz_zensko.Location = new System.Drawing.Point(50, 62);
            this.rb_iz_zensko.Name = "rb_iz_zensko";
            this.rb_iz_zensko.Size = new System.Drawing.Size(76, 21);
            this.rb_iz_zensko.TabIndex = 6;
            this.rb_iz_zensko.TabStop = true;
            this.rb_iz_zensko.Text = "Žensko";
            this.rb_iz_zensko.UseVisualStyleBackColor = true;
            // 
            // rb_iz_musko
            // 
            this.rb_iz_musko.AutoSize = true;
            this.rb_iz_musko.Checked = true;
            this.rb_iz_musko.Location = new System.Drawing.Point(50, 35);
            this.rb_iz_musko.Name = "rb_iz_musko";
            this.rb_iz_musko.Size = new System.Drawing.Size(70, 21);
            this.rb_iz_musko.TabIndex = 5;
            this.rb_iz_musko.TabStop = true;
            this.rb_iz_musko.Text = "Muško";
            this.rb_iz_musko.UseVisualStyleBackColor = true;
            // 
            // gb_iz_dodatneInfo
            // 
            this.gb_iz_dodatneInfo.Controls.Add(this.tb_iz_email);
            this.gb_iz_dodatneInfo.Controls.Add(this.label71);
            this.gb_iz_dodatneInfo.Controls.Add(this.tb_iz_adresa);
            this.gb_iz_dodatneInfo.Controls.Add(this.label72);
            this.gb_iz_dodatneInfo.Controls.Add(this.label73);
            this.gb_iz_dodatneInfo.Controls.Add(this.tb_iz_mob);
            this.gb_iz_dodatneInfo.Controls.Add(this.tb_iz_tel);
            this.gb_iz_dodatneInfo.Controls.Add(this.label74);
            this.gb_iz_dodatneInfo.Location = new System.Drawing.Point(23, 350);
            this.gb_iz_dodatneInfo.Name = "gb_iz_dodatneInfo";
            this.gb_iz_dodatneInfo.Size = new System.Drawing.Size(452, 202);
            this.gb_iz_dodatneInfo.TabIndex = 9;
            this.gb_iz_dodatneInfo.TabStop = false;
            this.gb_iz_dodatneInfo.Text = "Dodatne informacije";
            // 
            // tb_iz_email
            // 
            this.tb_iz_email.Location = new System.Drawing.Point(93, 155);
            this.tb_iz_email.Name = "tb_iz_email";
            this.tb_iz_email.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_email.TabIndex = 7;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(29, 155);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(60, 20);
            this.label71.TabIndex = 6;
            this.label71.Text = "e-mail:";
            // 
            // tb_iz_adresa
            // 
            this.tb_iz_adresa.Location = new System.Drawing.Point(93, 41);
            this.tb_iz_adresa.Multiline = true;
            this.tb_iz_adresa.Name = "tb_iz_adresa";
            this.tb_iz_adresa.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_adresa.TabIndex = 1;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(22, 41);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(67, 20);
            this.label72.TabIndex = 0;
            this.label72.Text = "Adresa:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(50, 82);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(37, 20);
            this.label73.TabIndex = 2;
            this.label73.Text = "Tel:";
            // 
            // tb_iz_mob
            // 
            this.tb_iz_mob.Location = new System.Drawing.Point(93, 117);
            this.tb_iz_mob.Name = "tb_iz_mob";
            this.tb_iz_mob.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_mob.TabIndex = 5;
            // 
            // tb_iz_tel
            // 
            this.tb_iz_tel.Location = new System.Drawing.Point(93, 80);
            this.tb_iz_tel.Name = "tb_iz_tel";
            this.tb_iz_tel.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_tel.TabIndex = 3;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(41, 117);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(46, 20);
            this.label74.TabIndex = 4;
            this.label74.Text = "Mob:";
            // 
            // gb_iz_osobniPodaci
            // 
            this.gb_iz_osobniPodaci.Controls.Add(this.tb_iz_ime);
            this.gb_iz_osobniPodaci.Controls.Add(this.label75);
            this.gb_iz_osobniPodaci.Controls.Add(this.label76);
            this.gb_iz_osobniPodaci.Controls.Add(this.tb_iz_oib);
            this.gb_iz_osobniPodaci.Controls.Add(this.tb_iz_prezime);
            this.gb_iz_osobniPodaci.Controls.Add(this.label77);
            this.gb_iz_osobniPodaci.Location = new System.Drawing.Point(23, 151);
            this.gb_iz_osobniPodaci.Name = "gb_iz_osobniPodaci";
            this.gb_iz_osobniPodaci.Size = new System.Drawing.Size(452, 175);
            this.gb_iz_osobniPodaci.TabIndex = 8;
            this.gb_iz_osobniPodaci.TabStop = false;
            this.gb_iz_osobniPodaci.Text = "Osobni podatci";
            // 
            // tb_iz_ime
            // 
            this.tb_iz_ime.Location = new System.Drawing.Point(93, 41);
            this.tb_iz_ime.Name = "tb_iz_ime";
            this.tb_iz_ime.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_ime.TabIndex = 1;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(46, 41);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(41, 20);
            this.label75.TabIndex = 0;
            this.label75.Text = "Ime:";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(11, 80);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(76, 20);
            this.label76.TabIndex = 2;
            this.label76.Text = "Prezime:";
            // 
            // tb_iz_oib
            // 
            this.tb_iz_oib.Enabled = false;
            this.tb_iz_oib.Location = new System.Drawing.Point(93, 117);
            this.tb_iz_oib.Name = "tb_iz_oib";
            this.tb_iz_oib.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_oib.TabIndex = 5;
            // 
            // tb_iz_prezime
            // 
            this.tb_iz_prezime.Location = new System.Drawing.Point(93, 80);
            this.tb_iz_prezime.Name = "tb_iz_prezime";
            this.tb_iz_prezime.Size = new System.Drawing.Size(208, 22);
            this.tb_iz_prezime.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(44, 119);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(43, 20);
            this.label77.TabIndex = 4;
            this.label77.Text = "OIB:";
            // 
            // pnl_RP_pretraziPopratnice
            // 
            this.pnl_RP_pretraziPopratnice.BackColor = System.Drawing.Color.White;
            this.pnl_RP_pretraziPopratnice.Controls.Add(this.rtb_prikaziPopratnice);
            this.pnl_RP_pretraziPopratnice.Controls.Add(this.groupBox6);
            this.pnl_RP_pretraziPopratnice.Controls.Add(this.btn_prikaziPopratnicu);
            this.pnl_RP_pretraziPopratnice.Controls.Add(this.panel19);
            this.pnl_RP_pretraziPopratnice.Location = new System.Drawing.Point(1375, 301);
            this.pnl_RP_pretraziPopratnice.Name = "pnl_RP_pretraziPopratnice";
            this.pnl_RP_pretraziPopratnice.Size = new System.Drawing.Size(953, 617);
            this.pnl_RP_pretraziPopratnice.TabIndex = 35;
            this.pnl_RP_pretraziPopratnice.Visible = false;
            // 
            // rtb_prikaziPopratnice
            // 
            this.rtb_prikaziPopratnice.Location = new System.Drawing.Point(21, 132);
            this.rtb_prikaziPopratnice.Name = "rtb_prikaziPopratnice";
            this.rtb_prikaziPopratnice.ReadOnly = true;
            this.rtb_prikaziPopratnice.Size = new System.Drawing.Size(914, 477);
            this.rtb_prikaziPopratnice.TabIndex = 14;
            this.rtb_prikaziPopratnice.Text = "";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label69);
            this.groupBox6.Controls.Add(this.cmbox_prikaziPopratnicu_datumUpisa);
            this.groupBox6.Controls.Add(this.datePicker_prikaziPopratnicu);
            this.groupBox6.Controls.Add(this.label68);
            this.groupBox6.Location = new System.Drawing.Point(21, 9);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(387, 102);
            this.groupBox6.TabIndex = 13;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Zaposlenik";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(31, 68);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(62, 20);
            this.label69.TabIndex = 15;
            this.label69.Text = "Detalji:";
            // 
            // cmbox_prikaziPopratnicu_datumUpisa
            // 
            this.cmbox_prikaziPopratnicu_datumUpisa.FormattingEnabled = true;
            this.cmbox_prikaziPopratnicu_datumUpisa.Location = new System.Drawing.Point(99, 66);
            this.cmbox_prikaziPopratnicu_datumUpisa.Name = "cmbox_prikaziPopratnicu_datumUpisa";
            this.cmbox_prikaziPopratnicu_datumUpisa.Size = new System.Drawing.Size(274, 24);
            this.cmbox_prikaziPopratnicu_datumUpisa.TabIndex = 14;
            // 
            // datePicker_prikaziPopratnicu
            // 
            this.datePicker_prikaziPopratnicu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_prikaziPopratnicu.Location = new System.Drawing.Point(240, 34);
            this.datePicker_prikaziPopratnicu.Name = "datePicker_prikaziPopratnicu";
            this.datePicker_prikaziPopratnicu.Size = new System.Drawing.Size(133, 22);
            this.datePicker_prikaziPopratnicu.TabIndex = 13;
            this.datePicker_prikaziPopratnicu.ValueChanged += new System.EventHandler(this.datePicker_prikaziPopratnicu_ValueChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(31, 34);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(192, 20);
            this.label68.TabIndex = 2;
            this.label68.Text = "Datum upisa popratnice:";
            // 
            // btn_prikaziPopratnicu
            // 
            this.btn_prikaziPopratnicu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_prikaziPopratnicu.Location = new System.Drawing.Point(429, 78);
            this.btn_prikaziPopratnicu.Name = "btn_prikaziPopratnicu";
            this.btn_prikaziPopratnicu.Size = new System.Drawing.Size(338, 30);
            this.btn_prikaziPopratnicu.TabIndex = 12;
            this.btn_prikaziPopratnicu.Text = "Prikaži";
            this.btn_prikaziPopratnicu.UseVisualStyleBackColor = true;
            this.btn_prikaziPopratnicu.Click += new System.EventHandler(this.btn_prikaziPopratnicu_Click);
            // 
            // panel19
            // 
            this.panel19.Location = new System.Drawing.Point(3, 620);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(960, 42);
            this.panel19.TabIndex = 3;
            // 
            // Form_Administrator
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1271, 720);
            this.Controls.Add(this.pnl_RP_pretraziPopratnice);
            this.Controls.Add(this.pnl_RP_izmjeniPodatkeOZaposlenima);
            this.Controls.Add(this.pnl_RP_pretraziSkladiste);
            this.Controls.Add(this.pnl_RP_poslovodaPrikaziRadniNalog);
            this.Controls.Add(this.pnl_Izbornik_Direktor);
            this.Controls.Add(this.pnl_Izbornik_PomocniRadnikPrijePilane);
            this.Controls.Add(this.pnl_RP_napisiRadniNalog);
            this.Controls.Add(this.pnl_Izbornik_PoslovodaElementare);
            this.Controls.Add(this.pnl_RP_napisiPopratnicu);
            this.Controls.Add(this.pnl_Izbornik_RadnikSGotovomRobom);
            this.Controls.Add(this.pnl_RP_prikaziOstaleKamione);
            this.Controls.Add(this.pnl_RP_upisiPaket_RadnikNaViselisnom);
            this.Controls.Add(this.pnl_Izbornik_RadnikNaViselisnomRezacu);
            this.Controls.Add(this.pnl_RP_upisiPaketUSkladiste);
            this.Controls.Add(this.pnl_Izbornik_RadnikNaViličaru);
            this.Controls.Add(this.pnl_RP_PrikaziStatistiku_MajstorNaTPT);
            this.Controls.Add(this.pnl_Izbornik_MajstorNaTPT);
            this.Controls.Add(this.pnl_RP_upisDugackihKladaZaPiljenje);
            this.Controls.Add(this.pnl_RP_UpisiKladeKojeCeSeIzrezati);
            this.Controls.Add(this.pnl_RP_upisiKlade);
            this.Controls.Add(this.pnl_RP_PrikaziRadniNalogSAMO_RN);
            this.Controls.Add(this.pnl_Izbornik_ZaOneSaSamoRN);
            this.Controls.Add(this.pnl_Izbornik_PoslovodaPilane);
            this.Controls.Add(this.pnl_RP_prikazTrenutnihKamiona);
            this.Controls.Add(this.pnl_Izbornik_Portir);
            this.Controls.Add(this.pnl_RP_upisiNoviKamion);
            this.Controls.Add(this.pnl_RP_kreirajNovogZaposlenika);
            this.Controls.Add(this.pnl_info);
            this.Controls.Add(this.pnl_Head);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Administrator";
            this.Text = "AUPDI";
            this.Load += new System.EventHandler(this.Form_Administrator_Load);
            this.pnl_Head.ResumeLayout(false);
            this.pnl_Head.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_iconMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_logOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).EndInit();
            this.pnl_Izbornik_Direktor.ResumeLayout(false);
            this.pnl_RP_kreirajNovogZaposlenika.ResumeLayout(false);
            this.gb_KorRacun.ResumeLayout(false);
            this.gb_KorRacun.PerformLayout();
            this.gb_PoslovniDetalji.ResumeLayout(false);
            this.gb_PoslovniDetalji.PerformLayout();
            this.gb_spol.ResumeLayout(false);
            this.gb_spol.PerformLayout();
            this.gb_dodatneInfo.ResumeLayout(false);
            this.gb_dodatneInfo.PerformLayout();
            this.gb_osobniPodaci.ResumeLayout(false);
            this.gb_osobniPodaci.PerformLayout();
            this.pnl_RP_upisiNoviKamion.ResumeLayout(false);
            this.gb_podaciOVozilu.ResumeLayout(false);
            this.gb_podaciOVozilu.PerformLayout();
            this.pnl_RP_prikazTrenutnihKamiona.ResumeLayout(false);
            this.gb_trenutniKamioni.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_trenutniKamioni)).EndInit();
            this.pnl_RP_prikaziOstaleKamione.ResumeLayout(false);
            this.gb_kamioniPretraziPo.ResumeLayout(false);
            this.gb_kamioniPretraziPo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ostaliKamioni)).EndInit();
            this.gb_opcijePrikaziOstaleKamione.ResumeLayout(false);
            this.gb_opcijePrikaziOstaleKamione.PerformLayout();
            this.pnl_Izbornik_Portir.ResumeLayout(false);
            this.pnl_info.ResumeLayout(false);
            this.pnl_info.PerformLayout();
            this.pnl_Izbornik_PoslovodaPilane.ResumeLayout(false);
            this.pnl_RP_upisiKlade.ResumeLayout(false);
            this.gb_podaciOSkaldistu.ResumeLayout(false);
            this.gb_podaciOSkaldistu.PerformLayout();
            this.gb_podaciOKladama.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_podaciOKladama)).EndInit();
            this.gb_podaciOPopratnici.ResumeLayout(false);
            this.gb_podaciOPopratnici.PerformLayout();
            this.pnl_RP_napisiRadniNalog.ResumeLayout(false);
            this.gb_radniNalogHead.ResumeLayout(false);
            this.gb_radniNalogHead.PerformLayout();
            this.gb_tekstRadnogNaloga.ResumeLayout(false);
            this.gb_tekstRadnogNaloga.PerformLayout();
            this.gb_PodaciORadnomNalogu.ResumeLayout(false);
            this.gb_PodaciORadnomNalogu.PerformLayout();
            this.pnl_RP_poslovodaPrikaziRadniNalog.ResumeLayout(false);
            this.gb_RN_dodatneOpcije.ResumeLayout(false);
            this.gb_PozicijaRadaTxt.ResumeLayout(false);
            this.gb_pozicijaRadaPretraziPo.ResumeLayout(false);
            this.gb_pozicijaRadaPretraziPo.PerformLayout();
            this.pnl_Izbornik_ZaOneSaSamoRN.ResumeLayout(false);
            this.pnl_RP_PrikaziRadniNalogSAMO_RN.ResumeLayout(false);
            this.gb_txtRadnogNaloga_SAMO_RN.ResumeLayout(false);
            this.gb_PretraziPo_RN_SAMO_RN.ResumeLayout(false);
            this.gb_PretraziPo_RN_SAMO_RN.PerformLayout();
            this.pnl_Izbornik_PomocniRadnikPrijePilane.ResumeLayout(false);
            this.pnl_RP_UpisiKladeKojeCeSeIzrezati.ResumeLayout(false);
            this.gb_brojplocica_KladeKojeCeSeRezati.ResumeLayout(false);
            this.gb_brojplocica_KladeKojeCeSeRezati.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_brojPlocicaZaRezanje)).EndInit();
            this.pnl_RP_upisDugackihKladaZaPiljenje.ResumeLayout(false);
            this.pnl_RP_upisDugackihKladaZaPiljenje.PerformLayout();
            this.gb_podaciODrugojPoloviciKlade.ResumeLayout(false);
            this.gb_podaciODrugojPoloviciKlade.PerformLayout();
            this.gb_podaciOPrvojPoloviciKlade.ResumeLayout(false);
            this.gb_podaciOPrvojPoloviciKlade.PerformLayout();
            this.pnl_Izbornik_MajstorNaTPT.ResumeLayout(false);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.ResumeLayout(false);
            this.pnl_RP_PrikaziStatistiku_MajstorNaTPT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_Statistika_MajstorNaTPT)).EndInit();
            this.gb_StatistikaKubikaIKlada.ResumeLayout(false);
            this.gb_StatistikaKubikaIKlada.PerformLayout();
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.ResumeLayout(false);
            this.gb_PojedinostiZaStatistiku_MajstorNaTPT.PerformLayout();
            this.pnl_Izbornik_RadnikNaViličaru.ResumeLayout(false);
            this.pnl_RP_upisiPaketUSkladiste.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.gb_podaciOPaketu_susara.ResumeLayout(false);
            this.gb_podaciOPaketu_susara.PerformLayout();
            this.pnl_Izbornik_PoslovodaElementare.ResumeLayout(false);
            this.pnl_Izbornik_RadnikNaViselisnomRezacu.ResumeLayout(false);
            this.pnl_RP_upisiPaket_RadnikNaViselisnom.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.pnl_Izbornik_RadnikSGotovomRobom.ResumeLayout(false);
            this.pnl_RP_napisiPopratnicu.ResumeLayout(false);
            this.gb_upisitePakete_popratnica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_brojPaketa_popratnica)).EndInit();
            this.gb_izgledPopratnice_popratnica.ResumeLayout(false);
            this.gb_podaciOPopratnici_popratnica.ResumeLayout(false);
            this.gb_podaciOPopratnici_popratnica.PerformLayout();
            this.pnl_RP_pretraziSkladiste.ResumeLayout(false);
            this.gb_prog_rezultati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prog_rezultati)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.pnl_RP_izmjeniPodatkeOZaposlenima.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.gbox_iz_spol.ResumeLayout(false);
            this.gbox_iz_spol.PerformLayout();
            this.gb_iz_dodatneInfo.ResumeLayout(false);
            this.gb_iz_dodatneInfo.PerformLayout();
            this.gb_iz_osobniPodaci.ResumeLayout(false);
            this.gb_iz_osobniPodaci.PerformLayout();
            this.pnl_RP_pretraziPopratnice.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_Head;
        private System.Windows.Forms.Panel pnl_Izbornik_Direktor;
        private System.Windows.Forms.Button btn_kreirajNovogZaposlenika;
        private System.Windows.Forms.Panel pnl_RP_kreirajNovogZaposlenika;
        private System.Windows.Forms.GroupBox gb_PoslovniDetalji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbox_pozicijaRada;
        private System.Windows.Forms.GroupBox gb_spol;
        private System.Windows.Forms.GroupBox gb_dodatneInfo;
        private System.Windows.Forms.TextBox tb_adresa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_mob;
        private System.Windows.Forms.TextBox tb_tel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gb_osobniPodaci;
        private System.Windows.Forms.TextBox tb_ime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_OIB;
        private System.Windows.Forms.TextBox tb_prezime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_spremiNovogZaposlenika;
        private System.Windows.Forms.GroupBox gb_KorRacun;
        private System.Windows.Forms.TextBox tb_korIme;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_lozinka;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_generirajLozinku;
        private System.Windows.Forms.RadioButton rb_zensko;
        private System.Windows.Forms.RadioButton rb_musko;
        private System.Windows.Forms.Label lb_info;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnl_info;
        private System.Windows.Forms.Label lb_naslov;
        private System.Windows.Forms.Panel pnl_Izbornik_Portir;
        private System.Windows.Forms.Button btn_IZB_UpisiNoviKamion;
        private System.Windows.Forms.Panel pnl_RP_upisiNoviKamion;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_spremiNoviKamion;
        private System.Windows.Forms.GroupBox gb_podaciOVozilu;
        private System.Windows.Forms.TextBox tb_oibVozača;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_prezimeVozača;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_regOznakaKamiona;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb_imeVozača;
        private System.Windows.Forms.TextBox tb_regOznakaPrikolice;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btn_IZB_PrikaziTrenutneKamione;
        private System.Windows.Forms.Panel pnl_RP_prikazTrenutnihKamiona;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox gb_trenutniKamioni;
        private System.Windows.Forms.DataGridView dgw_trenutniKamioni;
        private System.Windows.Forms.Button btn_odlazakKamiona;
        private System.Windows.Forms.Timer timerPrikaziBrojUpita;
        private System.Windows.Forms.Button btn_IZB_prikaziOstaleKamione;
        private System.Windows.Forms.Panel pnl_RP_prikaziOstaleKamione;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox gb_opcijePrikaziOstaleKamione;
        private System.Windows.Forms.DataGridView dgv_ostaliKamioni;
        private System.Windows.Forms.TextBox tb_kamioniPretraziPo;
        private System.Windows.Forms.Button btn_pretraziOstaleKamione;
        private System.Windows.Forms.DateTimePicker datePicker_up;
        private System.Windows.Forms.DateTimePicker datePicker_down;
        private System.Windows.Forms.GroupBox gb_kamioniPretraziPo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbox_pretraziPo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel pnl_Izbornik_PoslovodaPilane;
        private System.Windows.Forms.Button btn_IZB_poslovodaPrikaziRadniNalog;
        private System.Windows.Forms.Button btn_IZB_NapisiRadniNalog;
        private System.Windows.Forms.Button btn_IZB_UpisiKlade;
        private System.Windows.Forms.Panel pnl_RP_upisiKlade;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_spremiUpisaneKlade;
        private System.Windows.Forms.GroupBox gb_podaciOPopratnici;
        private System.Windows.Forms.ComboBox cmbox_odaberiteKamion;
        private System.Windows.Forms.DateTimePicker datePicker_datumOtpremanja;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_imePrezimeOtpremnika;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tb_vrijemeOtpremanja;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tb_mjestoUtovara;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tb_sumarija;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox gb_podaciOKladama;
        private System.Windows.Forms.DataGridView dgv_podaciOKladama;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_brPlocice;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sifraDrveta;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_duljinaKlade;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_debljinaKlade;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_obujamKlade;
        private System.Windows.Forms.GroupBox gb_podaciOSkaldistu;
        private System.Windows.Forms.ComboBox cmbox_odaberiteSkladiste;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel pnl_RP_napisiRadniNalog;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_spremiRadniNalog;
        private System.Windows.Forms.GroupBox gb_PodaciORadnomNalogu;
        private System.Windows.Forms.DateTimePicker datePicker_nalogZaDatum;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox gb_tekstRadnogNaloga;
        private System.Windows.Forms.TextBox tb_radniNalogTekst;
        private System.Windows.Forms.ComboBox cmbox_nalogRadnik;
        private System.Windows.Forms.ComboBox cmbox_nalogPozicijaRada;
        private System.Windows.Forms.GroupBox gb_radniNalogHead;
        private System.Windows.Forms.TextBox tb_RadniNalogHead;
        private System.Windows.Forms.Panel pnl_RP_poslovodaPrikaziRadniNalog;
        private System.Windows.Forms.GroupBox gb_PozicijaRadaTxt;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox gb_pozicijaRadaPretraziPo;
        private System.Windows.Forms.ComboBox cmbox_poslovodaRNRadnik;
        private System.Windows.Forms.ComboBox cmbox_poslovodaRNPozicijaRada;
        private System.Windows.Forms.DateTimePicker datePicker_RadniNalogDatum;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox gb_RN_dodatneOpcije;
        private System.Windows.Forms.RichTextBox rtb_RadniNalogTxt;
        private System.Windows.Forms.Button btn_poslovodaIsprintajRN;
        private System.Windows.Forms.ComboBox cmbox_vrijemePisanjaRN;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btn_poslovodaPrikaziRadniNalog;
        private System.Drawing.Printing.PrintDocument printDocument_RadniNalog;
        private System.Windows.Forms.PrintDialog printDialog_RadniNalog;
        private System.Windows.Forms.Panel pnl_Izbornik_ZaOneSaSamoRN;
        private System.Windows.Forms.Button btn_IZB_PrikaziRadniNalog;
        private System.Windows.Forms.Panel pnl_RP_PrikaziRadniNalogSAMO_RN;
        private System.Windows.Forms.GroupBox gb_txtRadnogNaloga_SAMO_RN;
        private System.Windows.Forms.RichTextBox rtb_txtRN_SAMO_RN;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox gb_PretraziPo_RN_SAMO_RN;
        private System.Windows.Forms.ComboBox cmbox_vrijemePisanjaRN_SAMO_RN;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btn_prikaziRN_SAMO_RN;
        private System.Windows.Forms.DateTimePicker datePicker_datumIzvrsavanjaRN_SAMO_RN;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btn_isprintajRN_SAMO_RN;
        private System.Drawing.Printing.PrintDocument printDocument_SAMO_RN;
        private System.Windows.Forms.PrintDialog printDialog_SAMO_RN;
        private System.Windows.Forms.Panel pnl_Izbornik_PomocniRadnikPrijePilane;
        private System.Windows.Forms.Button btn_IZB_UpisDugackihKladaZaPiljenje;
        private System.Windows.Forms.Button btn_IZB_upisiKladeKojeCeSeIzrezati;
        private System.Windows.Forms.Panel pnl_RP_UpisiKladeKojeCeSeIzrezati;
        private System.Windows.Forms.GroupBox gb_brojplocica_KladeKojeCeSeRezati;
        private System.Windows.Forms.DataGridView dgv_brojPlocicaZaRezanje;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button btn_upisiIzrezanePlocice;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbox_OdaberiteMajstoraZaRezanjeKlada;
        private System.Windows.Forms.Panel pnl_RP_upisDugackihKladaZaPiljenje;
        private System.Windows.Forms.GroupBox gb_podaciOPrvojPoloviciKlade;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btn_upisiKladuZaPiljenje;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox tb_brojPlociceZaPiljenje;
        private System.Windows.Forms.TextBox tb_duljina1klade;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox gb_podaciODrugojPoloviciKlade;
        private System.Windows.Forms.TextBox tb_obujam2klade;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tb_debljina2klade;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tb_duljina2klade;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tb_obujam1klade;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_debljina1klade;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tb_sifra2Klade;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tb_sifra1Klade;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbox_odaberiteSkaldisteZaPiljenjeKlada;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btn_IZB_PrikaziRadniNalogRadnikPrijePilane;
        private System.Windows.Forms.Panel pnl_Izbornik_MajstorNaTPT;
        private System.Windows.Forms.Button btn_IZB_PrikaziStatistiku_MajstorNaTPT;
        private System.Windows.Forms.Button btn_IZB_PrikaziRadniNalog_MajstorNaTPT;
        private System.Windows.Forms.Panel pnl_RP_PrikaziStatistiku_MajstorNaTPT;
        private System.Windows.Forms.GroupBox gb_StatistikaKubikaIKlada;
        private System.Windows.Forms.Label lb_IzrezanihKubika_MajstorNaTPT;
        private System.Windows.Forms.Label lb_IzrezanihKlada_MajstorNaTPT;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox gb_PojedinostiZaStatistiku_MajstorNaTPT;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cmbox_RazdobljeStatistike_MajstorNaTPT;
        private System.Windows.Forms.Button btn_prikaziStatistiku_MajstorNaTPT;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_Statistika_MajstorNaTPT;
        private System.Windows.Forms.Label lb_granice_MajstorTPT;
        private System.Windows.Forms.Panel pnl_Izbornik_RadnikNaViličaru;
        private System.Windows.Forms.Button btn_IZB_upisiPaketUSusaru;
        private System.Windows.Forms.Button btn_IZB_PrikaziRadniNalog_RadnikNaViličaru;
        private System.Windows.Forms.Panel pnl_RP_upisiPaketUSkladiste;
        private System.Windows.Forms.GroupBox gb_podaciOPaketu_susara;
        private System.Windows.Forms.TextBox tb_duljinaDaske;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btn_spremiPaketUSkladiste;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox cmbox_odaberiteSkladiste_paket;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox tb_težinaPaketa;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox tb_količinaDasaka;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox tb_debljinaDaske;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox tb_sirinaDaske;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox cmbox_klasaDrveta_paket;
        private System.Windows.Forms.ComboBox cmbox_vrstaDrveta_paket;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_IsprintajMarkicuPaketa;
        private System.Windows.Forms.RichTextBox rtb_markicaPaketa_susara;
        private System.Windows.Forms.PrintDialog printDialog_MarkicaPaketa_Susara;
        private System.Drawing.Printing.PrintDocument printDocument_MarkicaPaketa_Susara;
        private System.Windows.Forms.Panel pnl_Izbornik_PoslovodaElementare;
        private System.Windows.Forms.Button btn_IZB_prikaziRadniNalogPoslovodaElementare;
        private System.Windows.Forms.Button btn_IZB_NapisiRadniNalog_Elementara;
        private System.Windows.Forms.Panel pnl_Izbornik_RadnikNaViselisnomRezacu;
        private System.Windows.Forms.Button btn_IZB_prikaziRadniNalog_Viselisni;
        private System.Windows.Forms.Button btn_IZB_upisiPaket_Elementara;
        private System.Windows.Forms.Panel pnl_RP_upisiPaket_RadnikNaViselisnom;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_upisiPaket_viselisni;
        private System.Windows.Forms.TextBox tb_brojPaketa_viselisni;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox cmbox_odaberiteSkladiste_viselisni;
        private System.Windows.Forms.Panel pnl_Izbornik_RadnikSGotovomRobom;
        private System.Windows.Forms.Button btn_IZB_PrikaziRadniNalog_GotovaRoba;
        private System.Windows.Forms.Button btn_IZB_upisiPaket_GotovaRoba;
        private System.Windows.Forms.Button btn_IZB_NapisiPopratnicu;
        private System.Windows.Forms.Panel pnl_RP_napisiPopratnicu;
        private System.Windows.Forms.GroupBox gb_upisitePakete_popratnica;
        private System.Windows.Forms.DataGridView dgv_brojPaketa_popratnica;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_BrojPaketa;
        private System.Windows.Forms.Button btn_spremiNovuPopratnicu;
        private System.Windows.Forms.GroupBox gb_izgledPopratnice_popratnica;
        private System.Windows.Forms.Button btn_ispisiPopratnicu;
        private System.Windows.Forms.RichTextBox rtb_izgledPopratnice;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.GroupBox gb_podaciOPopratnici_popratnica;
        private System.Windows.Forms.TextBox tb_uplatnicaNalog_popratnica;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox tb_mjestoIstovara_popratnica;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox cmbox_odaberitePrikolicu_popratnica;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox tb_kupac_popratnica;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tb_mjestoUtovara_popratnica;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox cmbox_odaberiteKamion_popratnica;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.PrintDialog printDialog_Popratnica;
        private System.Drawing.Printing.PrintDocument printDocument_popratnica;
        private System.Windows.Forms.Button btn_IZB_polovodaPilane_PrikaziStatistikuMajstoraNaTPT;
        private System.Windows.Forms.Label lb_odaberiteRadnika_TPT;
        private System.Windows.Forms.ComboBox cmbox_odaberiteRadnika_statistikaTPT;
        private System.Windows.Forms.Button btn_IZB_pretragaSkladista;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel pnl_RP_pretraziSkladiste;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn_prog_Pretrazi;
        private System.Windows.Forms.TextBox tb_prog_nazivi;
        private System.Windows.Forms.Label lb_prog_naziv;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox cmbox_PertraziPo_pp;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lb_prog_skl_spec;
        private System.Windows.Forms.ComboBox cmbox_prog_skla_spec;
        private System.Windows.Forms.GroupBox gb_prog_rezultati;
        private System.Windows.Forms.DataGridView dgv_prog_rezultati;
        private System.Windows.Forms.Button btn_IZB_pretraziSkladiste_elem;
        private System.Windows.Forms.Button btn_IZB_izmjeniPodatkeOZaposlenima;
        private System.Windows.Forms.Panel pnl_RP_izmjeniPodatkeOZaposlenima;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btn_iz_spremiIzmjeneKorRacuna;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.ComboBox cmbox_iz_pozRada;
        private System.Windows.Forms.GroupBox gbox_iz_spol;
        private System.Windows.Forms.RadioButton rb_iz_zensko;
        private System.Windows.Forms.RadioButton rb_iz_musko;
        private System.Windows.Forms.GroupBox gb_iz_dodatneInfo;
        private System.Windows.Forms.TextBox tb_iz_email;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox tb_iz_adresa;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox tb_iz_mob;
        private System.Windows.Forms.TextBox tb_iz_tel;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.GroupBox gb_iz_osobniPodaci;
        private System.Windows.Forms.TextBox tb_iz_ime;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox tb_iz_oib;
        private System.Windows.Forms.TextBox tb_iz_prezime;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox cmbox_iz_odaberiteZaposlenika;
        private System.Windows.Forms.Button btn_IZB_prikaziStatistikuTPT_direktor;
        private System.Windows.Forms.Button btn_IZB_pretraziSkladiste_direktor;
        private System.Windows.Forms.Button btn_IZB_pretraziPopratnice_direktor;
        private System.Windows.Forms.Panel pnl_RP_pretraziPopratnice;
        private System.Windows.Forms.RichTextBox rtb_prikaziPopratnice;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button btn_prikaziPopratnicu;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btn_IZB_prikaziPopratnice;
        private System.Windows.Forms.Button btn_IZB_napisiRadniNalog_direktor;
        private System.Windows.Forms.Button btn_IZB_prikaziRadniNalog_direktor;
        private System.Windows.Forms.PictureBox pb_logOut;
        private System.Windows.Forms.PictureBox pb_minimize;
        private System.Windows.Forms.PictureBox pb_exit;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.ComboBox cmbox_prikaziPopratnicu_datumUpisa;
        private System.Windows.Forms.DateTimePicker datePicker_prikaziPopratnicu;
        private System.Windows.Forms.Label lb_errorTekst;
        private System.Windows.Forms.PictureBox pb_iconMain;
    }
}

