﻿namespace Zavrsin_rad_app
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pb_icon = new System.Windows.Forms.PictureBox();
            this.pb_settings = new System.Windows.Forms.PictureBox();
            this.pb_minimizeLogin = new System.Windows.Forms.PictureBox();
            this.lb_naslov = new System.Windows.Forms.Label();
            this.pb_login_exit = new System.Windows.Forms.PictureBox();
            this.tb_lozinkaPrijava = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_korImePrijava = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_prijaviSe = new System.Windows.Forms.Button();
            this.pnl_login = new System.Windows.Forms.Panel();
            this.pnl_settings = new System.Windows.Forms.Panel();
            this.btn_odustaniLogin = new System.Windows.Forms.Button();
            this.gb_postavkeServera = new System.Windows.Forms.GroupBox();
            this.btn_vratiPočetnePostavke = new System.Windows.Forms.Button();
            this.tb_internet_port = new System.Windows.Forms.TextBox();
            this.lb_txt_port_int = new System.Windows.Forms.Label();
            this.tb_internet_bazaPod = new System.Windows.Forms.TextBox();
            this.lb_txt_baza_int = new System.Windows.Forms.Label();
            this.tb_internet_server = new System.Windows.Forms.TextBox();
            this.lb_txt_server_int = new System.Windows.Forms.Label();
            this.tb_lokal_bazaPod = new System.Windows.Forms.TextBox();
            this.lb_txt_baza_local = new System.Windows.Forms.Label();
            this.tb_lokal_server = new System.Windows.Forms.TextBox();
            this.lb_txt_server_local = new System.Windows.Forms.Label();
            this.rb_internet = new System.Windows.Forms.RadioButton();
            this.rb_lokal = new System.Windows.Forms.RadioButton();
            this.btn_spremiSettings = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_settings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimizeLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_exit)).BeginInit();
            this.pnl_login.SuspendLayout();
            this.pnl_settings.SuspendLayout();
            this.gb_postavkeServera.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(191)))), ((int)(((byte)(60)))));
            this.panel1.Controls.Add(this.pb_icon);
            this.panel1.Controls.Add(this.pb_settings);
            this.panel1.Controls.Add(this.pb_minimizeLogin);
            this.panel1.Controls.Add(this.lb_naslov);
            this.panel1.Controls.Add(this.pb_login_exit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1314, 54);
            this.panel1.TabIndex = 0;
            // 
            // pb_icon
            // 
            this.pb_icon.Location = new System.Drawing.Point(12, 10);
            this.pb_icon.Name = "pb_icon";
            this.pb_icon.Size = new System.Drawing.Size(34, 34);
            this.pb_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_icon.TabIndex = 31;
            this.pb_icon.TabStop = false;
            // 
            // pb_settings
            // 
            this.pb_settings.Location = new System.Drawing.Point(434, 10);
            this.pb_settings.Name = "pb_settings";
            this.pb_settings.Size = new System.Drawing.Size(34, 34);
            this.pb_settings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_settings.TabIndex = 30;
            this.pb_settings.TabStop = false;
            this.pb_settings.Click += new System.EventHandler(this.pb_settings_Click);
            this.pb_settings.MouseEnter += new System.EventHandler(this.pb_settings_MouseEnter);
            this.pb_settings.MouseLeave += new System.EventHandler(this.pb_settings_MouseLeave);
            // 
            // pb_minimizeLogin
            // 
            this.pb_minimizeLogin.Location = new System.Drawing.Point(474, 10);
            this.pb_minimizeLogin.Name = "pb_minimizeLogin";
            this.pb_minimizeLogin.Size = new System.Drawing.Size(34, 34);
            this.pb_minimizeLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_minimizeLogin.TabIndex = 19;
            this.pb_minimizeLogin.TabStop = false;
            this.pb_minimizeLogin.Click += new System.EventHandler(this.pb_minimizeLogin_Click);
            this.pb_minimizeLogin.MouseEnter += new System.EventHandler(this.pb_minimizeLogin_MouseEnter);
            this.pb_minimizeLogin.MouseLeave += new System.EventHandler(this.pb_minimizeLogin_MouseLeave);
            // 
            // lb_naslov
            // 
            this.lb_naslov.AutoSize = true;
            this.lb_naslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_naslov.ForeColor = System.Drawing.SystemColors.Control;
            this.lb_naslov.Location = new System.Drawing.Point(48, 19);
            this.lb_naslov.Name = "lb_naslov";
            this.lb_naslov.Size = new System.Drawing.Size(226, 25);
            this.lb_naslov.TabIndex = 18;
            this.lb_naslov.Text = "AUPDI - Prijava u sustav";
            // 
            // pb_login_exit
            // 
            this.pb_login_exit.Location = new System.Drawing.Point(514, 10);
            this.pb_login_exit.Name = "pb_login_exit";
            this.pb_login_exit.Size = new System.Drawing.Size(34, 34);
            this.pb_login_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_login_exit.TabIndex = 0;
            this.pb_login_exit.TabStop = false;
            this.pb_login_exit.Click += new System.EventHandler(this.pb_login_exit_Click);
            this.pb_login_exit.MouseEnter += new System.EventHandler(this.pb_login_exit_MouseEnter);
            this.pb_login_exit.MouseLeave += new System.EventHandler(this.pb_login_exit_MouseLeave);
            // 
            // tb_lozinkaPrijava
            // 
            this.tb_lozinkaPrijava.Location = new System.Drawing.Point(219, 119);
            this.tb_lozinkaPrijava.Name = "tb_lozinkaPrijava";
            this.tb_lozinkaPrijava.PasswordChar = '*';
            this.tb_lozinkaPrijava.Size = new System.Drawing.Size(208, 22);
            this.tb_lozinkaPrijava.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(141, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "Lozinka:";
            // 
            // tb_korImePrijava
            // 
            this.tb_korImePrijava.Location = new System.Drawing.Point(219, 78);
            this.tb_korImePrijava.Name = "tb_korImePrijava";
            this.tb_korImePrijava.Size = new System.Drawing.Size(208, 22);
            this.tb_korImePrijava.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(89, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Korisničko ime:";
            // 
            // btn_prijaviSe
            // 
            this.btn_prijaviSe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_prijaviSe.Location = new System.Drawing.Point(390, 202);
            this.btn_prijaviSe.Name = "btn_prijaviSe";
            this.btn_prijaviSe.Size = new System.Drawing.Size(158, 30);
            this.btn_prijaviSe.TabIndex = 3;
            this.btn_prijaviSe.Text = "Prijavi se";
            this.btn_prijaviSe.UseVisualStyleBackColor = true;
            this.btn_prijaviSe.Click += new System.EventHandler(this.btn_prijaviSe_Click);
            // 
            // pnl_login
            // 
            this.pnl_login.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnl_login.Controls.Add(this.label8);
            this.pnl_login.Controls.Add(this.btn_prijaviSe);
            this.pnl_login.Controls.Add(this.tb_korImePrijava);
            this.pnl_login.Controls.Add(this.tb_lozinkaPrijava);
            this.pnl_login.Controls.Add(this.label9);
            this.pnl_login.Location = new System.Drawing.Point(0, 54);
            this.pnl_login.Name = "pnl_login";
            this.pnl_login.Size = new System.Drawing.Size(560, 250);
            this.pnl_login.TabIndex = 19;
            // 
            // pnl_settings
            // 
            this.pnl_settings.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnl_settings.Controls.Add(this.btn_odustaniLogin);
            this.pnl_settings.Controls.Add(this.gb_postavkeServera);
            this.pnl_settings.Controls.Add(this.btn_spremiSettings);
            this.pnl_settings.Location = new System.Drawing.Point(693, 54);
            this.pnl_settings.Name = "pnl_settings";
            this.pnl_settings.Size = new System.Drawing.Size(560, 337);
            this.pnl_settings.TabIndex = 20;
            // 
            // btn_odustaniLogin
            // 
            this.btn_odustaniLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_odustaniLogin.Location = new System.Drawing.Point(213, 295);
            this.btn_odustaniLogin.Name = "btn_odustaniLogin";
            this.btn_odustaniLogin.Size = new System.Drawing.Size(158, 30);
            this.btn_odustaniLogin.TabIndex = 5;
            this.btn_odustaniLogin.Text = "Odustani";
            this.btn_odustaniLogin.UseVisualStyleBackColor = true;
            this.btn_odustaniLogin.Click += new System.EventHandler(this.btn_odustaniLogin_Click);
            // 
            // gb_postavkeServera
            // 
            this.gb_postavkeServera.BackColor = System.Drawing.SystemColors.Control;
            this.gb_postavkeServera.Controls.Add(this.btn_vratiPočetnePostavke);
            this.gb_postavkeServera.Controls.Add(this.tb_internet_port);
            this.gb_postavkeServera.Controls.Add(this.lb_txt_port_int);
            this.gb_postavkeServera.Controls.Add(this.tb_internet_bazaPod);
            this.gb_postavkeServera.Controls.Add(this.lb_txt_baza_int);
            this.gb_postavkeServera.Controls.Add(this.tb_internet_server);
            this.gb_postavkeServera.Controls.Add(this.lb_txt_server_int);
            this.gb_postavkeServera.Controls.Add(this.tb_lokal_bazaPod);
            this.gb_postavkeServera.Controls.Add(this.lb_txt_baza_local);
            this.gb_postavkeServera.Controls.Add(this.tb_lokal_server);
            this.gb_postavkeServera.Controls.Add(this.lb_txt_server_local);
            this.gb_postavkeServera.Controls.Add(this.rb_internet);
            this.gb_postavkeServera.Controls.Add(this.rb_lokal);
            this.gb_postavkeServera.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_postavkeServera.Location = new System.Drawing.Point(22, 18);
            this.gb_postavkeServera.Name = "gb_postavkeServera";
            this.gb_postavkeServera.Size = new System.Drawing.Size(520, 266);
            this.gb_postavkeServera.TabIndex = 4;
            this.gb_postavkeServera.TabStop = false;
            this.gb_postavkeServera.Text = "Postavke servera";
            // 
            // btn_vratiPočetnePostavke
            // 
            this.btn_vratiPočetnePostavke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_vratiPočetnePostavke.Location = new System.Drawing.Point(356, 230);
            this.btn_vratiPočetnePostavke.Name = "btn_vratiPočetnePostavke";
            this.btn_vratiPočetnePostavke.Size = new System.Drawing.Size(158, 30);
            this.btn_vratiPočetnePostavke.TabIndex = 5;
            this.btn_vratiPočetnePostavke.Text = "Početne postavke";
            this.btn_vratiPočetnePostavke.UseVisualStyleBackColor = true;
            this.btn_vratiPočetnePostavke.Click += new System.EventHandler(this.btn_vratiPočetnePostavke_Click);
            // 
            // tb_internet_port
            // 
            this.tb_internet_port.Location = new System.Drawing.Point(183, 71);
            this.tb_internet_port.Name = "tb_internet_port";
            this.tb_internet_port.Size = new System.Drawing.Size(208, 22);
            this.tb_internet_port.TabIndex = 27;
            // 
            // lb_txt_port_int
            // 
            this.lb_txt_port_int.AutoSize = true;
            this.lb_txt_port_int.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_txt_port_int.Location = new System.Drawing.Point(65, 73);
            this.lb_txt_port_int.Name = "lb_txt_port_int";
            this.lb_txt_port_int.Size = new System.Drawing.Size(40, 18);
            this.lb_txt_port_int.TabIndex = 28;
            this.lb_txt_port_int.Text = "Port:";
            // 
            // tb_internet_bazaPod
            // 
            this.tb_internet_bazaPod.Location = new System.Drawing.Point(183, 99);
            this.tb_internet_bazaPod.Name = "tb_internet_bazaPod";
            this.tb_internet_bazaPod.Size = new System.Drawing.Size(208, 22);
            this.tb_internet_bazaPod.TabIndex = 25;
            // 
            // lb_txt_baza_int
            // 
            this.lb_txt_baza_int.AutoSize = true;
            this.lb_txt_baza_int.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_txt_baza_int.Location = new System.Drawing.Point(65, 101);
            this.lb_txt_baza_int.Name = "lb_txt_baza_int";
            this.lb_txt_baza_int.Size = new System.Drawing.Size(111, 18);
            this.lb_txt_baza_int.TabIndex = 26;
            this.lb_txt_baza_int.Text = "Baza podataka:";
            // 
            // tb_internet_server
            // 
            this.tb_internet_server.Location = new System.Drawing.Point(183, 43);
            this.tb_internet_server.Name = "tb_internet_server";
            this.tb_internet_server.Size = new System.Drawing.Size(208, 22);
            this.tb_internet_server.TabIndex = 23;
            // 
            // lb_txt_server_int
            // 
            this.lb_txt_server_int.AutoSize = true;
            this.lb_txt_server_int.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_txt_server_int.Location = new System.Drawing.Point(65, 45);
            this.lb_txt_server_int.Name = "lb_txt_server_int";
            this.lb_txt_server_int.Size = new System.Drawing.Size(55, 18);
            this.lb_txt_server_int.TabIndex = 24;
            this.lb_txt_server_int.Text = "Server:";
            // 
            // tb_lokal_bazaPod
            // 
            this.tb_lokal_bazaPod.Location = new System.Drawing.Point(183, 186);
            this.tb_lokal_bazaPod.Name = "tb_lokal_bazaPod";
            this.tb_lokal_bazaPod.Size = new System.Drawing.Size(208, 22);
            this.tb_lokal_bazaPod.TabIndex = 21;
            // 
            // lb_txt_baza_local
            // 
            this.lb_txt_baza_local.AutoSize = true;
            this.lb_txt_baza_local.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_txt_baza_local.Location = new System.Drawing.Point(65, 188);
            this.lb_txt_baza_local.Name = "lb_txt_baza_local";
            this.lb_txt_baza_local.Size = new System.Drawing.Size(111, 18);
            this.lb_txt_baza_local.TabIndex = 22;
            this.lb_txt_baza_local.Text = "Baza podataka:";
            // 
            // tb_lokal_server
            // 
            this.tb_lokal_server.Location = new System.Drawing.Point(183, 159);
            this.tb_lokal_server.Name = "tb_lokal_server";
            this.tb_lokal_server.Size = new System.Drawing.Size(208, 22);
            this.tb_lokal_server.TabIndex = 19;
            // 
            // lb_txt_server_local
            // 
            this.lb_txt_server_local.AutoSize = true;
            this.lb_txt_server_local.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_txt_server_local.Location = new System.Drawing.Point(65, 161);
            this.lb_txt_server_local.Name = "lb_txt_server_local";
            this.lb_txt_server_local.Size = new System.Drawing.Size(55, 18);
            this.lb_txt_server_local.TabIndex = 20;
            this.lb_txt_server_local.Text = "Server:";
            // 
            // rb_internet
            // 
            this.rb_internet.AutoSize = true;
            this.rb_internet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_internet.Location = new System.Drawing.Point(34, 21);
            this.rb_internet.Name = "rb_internet";
            this.rb_internet.Size = new System.Drawing.Size(187, 22);
            this.rb_internet.TabIndex = 1;
            this.rb_internet.TabStop = true;
            this.rb_internet.Text = "Spoji se putem interneta";
            this.rb_internet.UseVisualStyleBackColor = true;
            this.rb_internet.Click += new System.EventHandler(this.rb_internet_Click);
            // 
            // rb_lokal
            // 
            this.rb_lokal.AutoSize = true;
            this.rb_lokal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_lokal.Location = new System.Drawing.Point(34, 137);
            this.rb_lokal.Name = "rb_lokal";
            this.rb_lokal.Size = new System.Drawing.Size(224, 22);
            this.rb_lokal.TabIndex = 0;
            this.rb_lokal.TabStop = true;
            this.rb_lokal.Text = "Spoji se kao lokalno računalo";
            this.rb_lokal.UseVisualStyleBackColor = true;
            this.rb_lokal.Click += new System.EventHandler(this.rb_lokal_Click);
            // 
            // btn_spremiSettings
            // 
            this.btn_spremiSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_spremiSettings.Location = new System.Drawing.Point(377, 295);
            this.btn_spremiSettings.Name = "btn_spremiSettings";
            this.btn_spremiSettings.Size = new System.Drawing.Size(158, 30);
            this.btn_spremiSettings.TabIndex = 3;
            this.btn_spremiSettings.Text = "Spremi";
            this.btn_spremiSettings.UseVisualStyleBackColor = true;
            this.btn_spremiSettings.Click += new System.EventHandler(this.btn_spremiSettings_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1314, 475);
            this.Controls.Add(this.pnl_settings);
            this.Controls.Add(this.pnl_login);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AUPDI - Prijava u sustav";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_settings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimizeLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_exit)).EndInit();
            this.pnl_login.ResumeLayout(false);
            this.pnl_login.PerformLayout();
            this.pnl_settings.ResumeLayout(false);
            this.gb_postavkeServera.ResumeLayout(false);
            this.gb_postavkeServera.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb_lozinkaPrijava;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_korImePrijava;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_prijaviSe;
        private System.Windows.Forms.PictureBox pb_login_exit;
        private System.Windows.Forms.Label lb_naslov;
        private System.Windows.Forms.PictureBox pb_minimizeLogin;
        private System.Windows.Forms.Panel pnl_login;
        private System.Windows.Forms.Panel pnl_settings;
        private System.Windows.Forms.Button btn_vratiPočetnePostavke;
        private System.Windows.Forms.GroupBox gb_postavkeServera;
        private System.Windows.Forms.TextBox tb_internet_port;
        private System.Windows.Forms.Label lb_txt_port_int;
        private System.Windows.Forms.TextBox tb_internet_bazaPod;
        private System.Windows.Forms.Label lb_txt_baza_int;
        private System.Windows.Forms.TextBox tb_internet_server;
        private System.Windows.Forms.Label lb_txt_server_int;
        private System.Windows.Forms.TextBox tb_lokal_bazaPod;
        private System.Windows.Forms.Label lb_txt_baza_local;
        private System.Windows.Forms.TextBox tb_lokal_server;
        private System.Windows.Forms.Label lb_txt_server_local;
        private System.Windows.Forms.RadioButton rb_internet;
        private System.Windows.Forms.RadioButton rb_lokal;
        private System.Windows.Forms.Button btn_spremiSettings;
        private System.Windows.Forms.PictureBox pb_settings;
        private System.Windows.Forms.Button btn_odustaniLogin;
        private System.Windows.Forms.PictureBox pb_icon;
    }
}