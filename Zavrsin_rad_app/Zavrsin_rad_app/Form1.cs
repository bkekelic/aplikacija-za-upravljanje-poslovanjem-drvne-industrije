﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

//174, 124, 91
//39, 174, 96


namespace Zavrsin_rad_app
{

    public partial class Form_Administrator : Form
    {
        private bool dragging = false;
        private Point startPoint = new Point(0, 0);

        private Point izbornikPoint = new Point(0, 60);
        private Point radnaPlohaPoint = new Point(317, 60);
        private Point listaIzbornika = new Point(1275, 60);

        Random rnd = new Random();
        public string loginConString;

        List<string> listaPozRada = new List<string>();
        List<string> listaPozRadaPoslovodaPilane = new List<string>();
        List<string> listaPozRadaPoslovodaElementare = new List<string>();
        List<string> listaPretraziPo = new List<string>();
        List<string> listaRazdobljeZaStat = new List<string>();
        List<string> listaVrstaDrveta = new List<string>();
        List<string> listaKlasaDrveta = new List<string>();
        List<string> listaPretraziSkladiste = new List<string>();
        List<string> listaPretraziSkladisteElem = new List<string>();
        List<string> listaGrešaka = new List<string>();


        Zaposlenik TrenutnoPrijavljenKorisnik = new Zaposlenik();
        MojServer AktivanServer = new MojServer();
        Upiti mojiUpiti = new Upiti(0, 0, 0, 0);


        public Form_Administrator()
        {
            InitializeComponent();
            pb_iconMain.Image = Zavrsin_rad_app.Properties.Resources.wood_icon;
            pb_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button2;
            pb_logOut.Image = Zavrsin_rad_app.Properties.Resources.logOut_button3;
            pb_minimize.Image = Zavrsin_rad_app.Properties.Resources.minimize_button;


            dodajListe();
        }
        private void Form_Administrator_Load(object sender, EventArgs e)
        {
            this.Width = 1270;
            this.Height = 720;

            AktivanServer.SetConString(loginConString);
            TrenutnoPrijavljenKorisnik.SetKorIme("");
            TrenutnoPrijavljenKorisnik.SetLozinka("");

            for(int i = 0; i < loginConString.Length; i++)
            {
                if(loginConString[i] == 'I' && loginConString[i+1] == 'D' && loginConString[i+2] == '=')
                {
                    i += 3;
                    while(loginConString[i]!= ';')
                    {
                        TrenutnoPrijavljenKorisnik.SetKorIme(TrenutnoPrijavljenKorisnik.GetKorIme() + loginConString[i]);
                        i++;
                    }
                    i += 10;
                    while (i<loginConString.Length)
                    {
                        TrenutnoPrijavljenKorisnik.SetLozinka(TrenutnoPrijavljenKorisnik.GetLozinka() + loginConString[i]);
                        i++;
                    }
                }
            }

            //connectionServer.SetConString("Data Source=" + connectionServer.GetServer() + ";Initial Catalog=" + connectionServer.GetBazaPod() + ";User ID=" + connectionServer.GetUser() + ";Password=" + connectionServer.GetPassword()+"");


            provjeriPrijavljenogKorisnika();
            prikaziIzbornik();

        }

        //timeri
        private void timerPrikaziBrojUpita_Tick(object sender, EventArgs e)
        {
            timerPrikaziBrojUpita.Stop();

            mojiUpiti.resetirajUpite();

            lb_errorTekst.Visible = false;

            for(int i = 0; i < listaGrešaka.Count; i++)
            {
                listaGrešaka.RemoveAt(i);
            }
        }

        //pomicanje sučelja
        private void pnl_Head_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            startPoint = new Point(e.X, e.Y);
        }
        private void pnl_Head_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        private void pnl_Head_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging == true)
            {
                Point tempPoint = PointToScreen(e.Location);
                Location = new Point(tempPoint.X - startPoint.X, tempPoint.Y - startPoint.Y);
            }
        }

        //buttoni na sučelju      
        private void pb_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void pb_minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void pb_logOut_Click(object sender, EventArgs e)
        {
            sakrijIzbornike();
            sakrijRadnePlohe();

            this.Hide();
            LoginForm newForm = new LoginForm();
            newForm.Show();
        }

        private void pb_exit_MouseEnter(object sender, EventArgs e)
        {
            pb_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button5;

        }
        private void pb_exit_MouseLeave(object sender, EventArgs e)
        {
            pb_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button2;

        }
        private void pb_logOut_MouseEnter(object sender, EventArgs e)
        {
            pb_logOut.Image = Zavrsin_rad_app.Properties.Resources.logOut_button4;

        }
        private void pb_logOut_MouseLeave(object sender, EventArgs e)
        {
            pb_logOut.Image = Zavrsin_rad_app.Properties.Resources.logOut_button3;

        }
        private void pb_minimize_MouseEnter(object sender, EventArgs e)
        {
            pb_minimize.Image = Zavrsin_rad_app.Properties.Resources.minimize_button2;

        }
        private void pb_minimize_MouseLeave(object sender, EventArgs e)
        {
            pb_minimize.Image = Zavrsin_rad_app.Properties.Resources.minimize_button;

        }
   

        //funkcije za upite
        private void izvršiUpitBezPovrata(string query, int code)
        {

            try
            {
                SqlConnection con = new SqlConnection(AktivanServer.GetConString());
                con.Open();

                SqlCommand command = new SqlCommand(query, con);
                command.ExecuteNonQuery();

                con.Close();

            }
            catch (Exception)
            {
                ispišiGreškuNaEkran();
                listaGrešaka.Add(query);
            }


        }
        private DataTable izvršiUpitSPovratomDT(string query, int code)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlConnection con = new SqlConnection(AktivanServer.GetConString());
                con.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, con);
                dataAdapter.Fill(dt);
                con.Close();

                return dt;
            }
            catch (Exception)
            {
                ispišiGreškuNaEkran();
                listaGrešaka.Add(query);
                return dt;
            }


        }
        private void ispišiGreškuNaEkran()
        {
            //resetiraj timer
            if (timerPrikaziBrojUpita.Enabled)
            {
                timerPrikaziBrojUpita.Stop();
                timerPrikaziBrojUpita.Start();
            }
            else
            {
                timerPrikaziBrojUpita.Start();
            }
            lb_errorTekst.Visible = true;
            
           
        }
        private void lb_errorTekst_Click(object sender, EventArgs e)
        {
            string errors = "Dogodile su se slijedeće pogreške:\n";
            for(int i = 0;i < listaGrešaka.Count; i++)
            {
                errors += i+1 + ". " + listaGrešaka[i] + "\n\n";
            }

            MessageBox.Show(errors,"Prikaz pogrešaka");
        }


        //opće funkcije
        private int getRandomNum(int dg, int gg)
        {
            return rnd.Next(dg, gg + 1);
        }
        private void dodajListe()
        {
            listaPozRada.Add("Portir");
            listaPozRada.Add("Poslovođa pilane");
            listaPozRada.Add("Radnik na bageru");
            listaPozRada.Add("Pomoćni radnik prije pilane");
            listaPozRada.Add("Majstor na TPT-u");
            listaPozRada.Add("Forajzer");
            listaPozRada.Add("Radnik na iznosu");
            listaPozRada.Add("Radnik na viličaru");
            listaPozRada.Add("Radnik na višelisnom rezaču");
            listaPozRada.Add("Radnik u elementari");
            listaPozRada.Add("Radnik s gotovom robom");
            listaPozRada.Add("Poslovođa elementare");
            listaPozRada.Add("Majstor na održavanju");
            listaPozRada.Add("Čistač");
            listaPozRada.Add("Radnik u kuhinji");


            listaPozRadaPoslovodaPilane.Add("Radnik na bageru");
            listaPozRadaPoslovodaPilane.Add("Pomoćni radnik prije pilane");
            listaPozRadaPoslovodaPilane.Add("Majstor na TPT-u");
            listaPozRadaPoslovodaPilane.Add("Forajzer");
            listaPozRadaPoslovodaPilane.Add("Radnik na iznosu");
            listaPozRadaPoslovodaPilane.Add("Radnik na viličaru");

            listaPozRadaPoslovodaElementare.Add("Radnik na viličaru");
            listaPozRadaPoslovodaElementare.Add("Radnik na višelisnom rezaču");
            listaPozRadaPoslovodaElementare.Add("Radnik u elementari");
            listaPozRadaPoslovodaElementare.Add("Radnik s gotovom robom");

            listaPretraziPo.Add("Registracija kamiona");
            listaPretraziPo.Add("Registracija prikolice");
            listaPretraziPo.Add("Ime vozača");
            listaPretraziPo.Add("Prezime vozača");
            listaPretraziPo.Add("OIB vozača");
            listaPretraziPo.Add("OIB portira");

            listaRazdobljeZaStat.Add("Danas");
            listaRazdobljeZaStat.Add("Jučer");
            listaRazdobljeZaStat.Add("Ovaj tjedan");
            listaRazdobljeZaStat.Add("Prošli tjedan");
            listaRazdobljeZaStat.Add("Ovaj mjesec");
            listaRazdobljeZaStat.Add("Prošli mjesec");
            listaRazdobljeZaStat.Add("Ova godina");
            listaRazdobljeZaStat.Add("Prošla godina");
            listaRazdobljeZaStat.Add("Ukupno");

            listaVrstaDrveta.Add("Hrast");
            listaVrstaDrveta.Add("Bukva");
            listaVrstaDrveta.Add("Lipa");
            listaVrstaDrveta.Add("Trešnja");
            listaVrstaDrveta.Add("Grab");
            listaVrstaDrveta.Add("Bagrem");

            listaKlasaDrveta.Add("Prime");
            listaKlasaDrveta.Add("Rustic");
            listaKlasaDrveta.Add("Lamel");
            listaKlasaDrveta.Add("Dorada");
            listaKlasaDrveta.Add("Komercijalna");

            listaPretraziSkladiste.Add("Broju klade");
            listaPretraziSkladiste.Add("Broju paketa");
            listaPretraziSkladiste.Add("Skladištu");
            listaPretraziSkladiste.Add("Specifikacijama klade");
            listaPretraziSkladiste.Add("Specifikacijama paketa");

            listaPretraziSkladisteElem.Add("Broju paketa");
            listaPretraziSkladisteElem.Add("Skladištu");
            listaPretraziSkladisteElem.Add("Specifikacijama paketa");

        }
        private void provjeriPrijavljenogKorisnika()
        {
           
            string query = "select ime,prezime, OIB, pozicijaRada from dbo.zaposlenici where korIme like '" + TrenutnoPrijavljenKorisnik.GetKorIme() + "'";
            DataTable dt = new DataTable();
            dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {

                TrenutnoPrijavljenKorisnik.SetIme(row.ItemArray[0].ToString());
                TrenutnoPrijavljenKorisnik.SetPrezime(row.ItemArray[1].ToString());
                TrenutnoPrijavljenKorisnik.SetOib(row.ItemArray[2].ToString());
                TrenutnoPrijavljenKorisnik.SetPozicijaRada(row.ItemArray[3].ToString());
            }
            
            
      
            lb_info.Text = TrenutnoPrijavljenKorisnik.GetIme() + " "+TrenutnoPrijavljenKorisnik.GetPrezime() + " - "+TrenutnoPrijavljenKorisnik.GetPozicijaRada();

 
        }
        private string getDanašnjiDatum()
        {
            return DateTime.Now.ToShortDateString();
        }
        private string getSadašnjeVrijeme()
        {
            string value = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();
            return value;
        }
        private void isprazniCmbox(ComboBox cmbox)
        {
            int lenght = cmbox.Items.Count;

            for (int i = 0; i < lenght; i++)
            {
                cmbox.Items.RemoveAt(0);
            }
            cmbox.Text = "";
        }
        private string getLoznika()
        {
            string letters = "abcdefghijklmnoprstuvzABCDEFGHIJKLMNOPRSTUVZ0123456789";
            string password = "";
            int rndNum;

            for (int i = 0; i < 8; i++)
            {
                rndNum = getRandomNum(0, letters.Length - 1);
                password += letters[rndNum];
            }

            return password;
        }

        //prikazivanje 
        private void prikaziIzbornik()
        {
            //root korisnik
            if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Direktor" || TrenutnoPrijavljenKorisnik.GetKorIme() == "sa")
            {
                pnl_Izbornik_Direktor.Visible = true;
                pnl_Izbornik_Direktor.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Portir")
            {
                pnl_Izbornik_Portir.Visible = true;
                pnl_Izbornik_Portir.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane")
            {
                pnl_Izbornik_PoslovodaPilane.Visible = true;
                pnl_Izbornik_PoslovodaPilane.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik na bageru" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Forajzer" ||TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik na iznosu" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik u elementari" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na održavanju" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Cistac" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik u kuhinji")
            {
                pnl_Izbornik_ZaOneSaSamoRN.Visible = true;
                pnl_Izbornik_ZaOneSaSamoRN.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Pomocni radnik prije pilane")
            {
                pnl_Izbornik_PomocniRadnikPrijePilane.Visible = true;
                pnl_Izbornik_PomocniRadnikPrijePilane.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na TPT-u")
            {
                pnl_Izbornik_MajstorNaTPT.Visible = true;
                pnl_Izbornik_MajstorNaTPT.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik na vilicaru")
            {
                pnl_Izbornik_RadnikNaViličaru.Visible = true;
                pnl_Izbornik_RadnikNaViličaru.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda elementare")
            {
                pnl_Izbornik_PoslovodaElementare.Visible = true;
                pnl_Izbornik_PoslovodaElementare.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik na višelisnom rezacu")
            {
                pnl_Izbornik_RadnikNaViselisnomRezacu.Visible = true;
                pnl_Izbornik_RadnikNaViselisnomRezacu.Location = izbornikPoint;
            }
            else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik s gotovom robom")
            {
                pnl_Izbornik_RadnikSGotovomRobom.Visible = true;
                pnl_Izbornik_RadnikSGotovomRobom.Location = izbornikPoint;
            }

        }
        private void sakrijIzbornike()
        {
            pnl_Izbornik_Direktor.Visible = false;
            pnl_Izbornik_Portir.Visible = false;
            pnl_Izbornik_PoslovodaPilane.Visible = false;
            pnl_Izbornik_ZaOneSaSamoRN.Visible = false;
            pnl_Izbornik_PomocniRadnikPrijePilane.Visible = false;
            pnl_Izbornik_RadnikNaViličaru.Visible = false;
            pnl_Izbornik_PoslovodaElementare.Visible = false;
            pnl_Izbornik_RadnikNaViselisnomRezacu.Visible = false;
            pnl_Izbornik_RadnikSGotovomRobom.Visible = false;

        }
        private void sakrijRadnePlohe()
        {

            pnl_RP_kreirajNovogZaposlenika.Visible = false;
            pnl_RP_upisiNoviKamion.Visible = false;
            pnl_RP_prikazTrenutnihKamiona.Visible = false;
            pnl_RP_prikaziOstaleKamione.Visible = false;
            pnl_RP_upisiKlade.Visible = false;
            pnl_RP_napisiRadniNalog.Visible = false;
            pnl_RP_poslovodaPrikaziRadniNalog.Visible = false;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = false;
            pnl_RP_UpisiKladeKojeCeSeIzrezati.Visible = false;
            pnl_RP_upisDugackihKladaZaPiljenje.Visible = false;
            pnl_RP_upisiPaketUSkladiste.Visible = false;
            pnl_RP_upisiPaket_RadnikNaViselisnom.Visible = false;
            pnl_RP_pretraziSkladiste.Visible = false;
            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Visible = false;
            pnl_RP_izmjeniPodatkeOZaposlenima.Visible = false;
            pnl_RP_pretraziPopratnice.Visible = false;
            pnl_RP_napisiPopratnicu.Visible = false;
        }

        //printanje
        private void printDocument_RadniNalog_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString(rtb_RadniNalogTxt.Text, new Font("Arial", 9), Brushes.Black, new Point(100, 100));
        }
        private void printDocument_SAMO_RN_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString(rtb_txtRN_SAMO_RN.Text, new Font("Arial", 9), Brushes.Black, new Point(100, 100));
        }
        private void printDocument_MarkicaPaketa_Susara_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString(rtb_markicaPaketa_susara.Text, new Font("Arial", 12), Brushes.Black, new Point(100, 100));
        }
        private void printDocument_popratnica_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString(rtb_izgledPopratnice.Text, new Font("Arial", 9), Brushes.Black, new Point(100, 100));
        }



        //**********    DIREKTOR / ROOT

        //Kriraj novog zaposlenika
        private void btn_IZB_kreirajNovogZaposlenika_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_kreirajNovogZaposlenika.Visible = true;
            pnl_RP_kreirajNovogZaposlenika.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_pozicijaRada);

            foreach (Control c in gb_osobniPodaci.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_dodatneInfo.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_KorRacun.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_PoslovniDetalji.Controls) if (c is ComboBox) c.Text = "";
            rb_musko.Checked = true;


            foreach (string value in listaPozRada)
            {
                cmbox_pozicijaRada.Items.Add(value);
            }


        }
        private void btn_generirajLozinku_Click(object sender, EventArgs e)
        {        
            tb_lozinka.Text = getLoznika();
        }
        private void btn_spremiNovogZaposlenika_Click(object sender, EventArgs e)
        {

            Zaposlenik Novi = new Zaposlenik();

            Novi.SetIme(tb_ime.Text);
            Novi.SetPrezime(tb_prezime.Text);
            Novi.SetOib(tb_OIB.Text);
            Novi.SetAdresa(tb_adresa.Text);
            Novi.SetTel(tb_tel.Text);
            Novi.SetMob(tb_tel.Text);
            Novi.SetEmail(tb_email.Text);

            if (rb_musko.Checked) Novi.SetSpol("M");
            else Novi.SetSpol("Z");

            Novi.SetPozicijaRada(cmbox_pozicijaRada.SelectedItem.ToString());
            Novi.SetKorIme(tb_korIme.Text);
            Novi.SetLozinka(tb_lozinka.Text);


            String query = "insert into zaposlenici(ime,prezime,oib,adresa,tel,mob,email,spol,pozicijaRada,korIme,lozinka) values ('" + Novi.GetIme() + "','" + Novi.GetPrezime() + "','" + Novi.GetOib() + "','" + Novi.GetAdresa() + "','" + Novi.GetTel() + "','" + Novi.GetMob() + "','" + Novi.GetEmail() + "','" + Novi.GetSpol() + "','" + Novi.GetPozicijaRada() + "','" + Novi.GetKorIme() + "','" + Novi.GetLozinka() + "')";
            query += "\n create login " + Novi.GetKorIme() + " with password='" + Novi.GetLozinka() + "'";
            query += "\n create user " + Novi.GetKorIme() + " for login " + Novi.GetKorIme();
            query += "\n grant select to " + Novi.GetKorIme();
            query += "\n grant insert to " + Novi.GetKorIme();
            query += "\n grant update to " + Novi.GetKorIme();
            query += "\n grant delete to " + Novi.GetKorIme();
            izvršiUpitBezPovrata(query, 1);

            foreach (Control c in gb_osobniPodaci.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_dodatneInfo.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_KorRacun.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_PoslovniDetalji.Controls) if (c is ComboBox) c.ResetText();          
            rb_musko.Checked = true;

        }
        private void tb_prezime_Leave(object sender, EventArgs e)
        {
            tb_korIme.Text = tb_ime.Text[0].ToString().ToLower() + tb_prezime.Text.ToLower();
        }

        //Izmjeni podatke o zaposlenima
        private void btn_IZB_izmjeniPodatkeOZaposlenima_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_izmjeniPodatkeOZaposlenima.Visible = true;
            pnl_RP_izmjeniPodatkeOZaposlenima.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_iz_odaberiteZaposlenika);
            isprazniCmbox(cmbox_iz_pozRada);

            string query = "select ime, prezime, oib from zaposlenici";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach(DataRow row in dt.Rows)
            {
                cmbox_iz_odaberiteZaposlenika.Items.Add(row.ItemArray[0].ToString() + " "+ row.ItemArray[1].ToString() + " ("+row.ItemArray[2].ToString()+")");
            }

        }
        private void cmbox_iz_odaberiteZaposlenika_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbox_iz_odaberiteZaposlenika.SelectedIndex != -1)
            {
                Zaposlenik zap = new Zaposlenik();

                string word = cmbox_iz_odaberiteZaposlenika.SelectedItem.ToString();
                string oib = "";
                for(int i = 0; i < word.Length; i++)
                {
                    if(word[i] == '(')
                    {
                        i++;
                        while(word[i]!= ')')
                        {
                            oib += word[i];
                            i++;
                        }
                    }                  
                }


                string query = "SELECT ime, prezime, OIB, adresa, tel, mob, email, spol, pozicijaRada, korIme FROM zaposlenici WHERE oib = '" + oib + "'";
                DataTable dt = izvršiUpitSPovratomDT(query, 0);
                foreach(DataRow row in dt.Rows)
                {
                    zap.SetIme(row.ItemArray[0].ToString());
                    zap.SetPrezime(row.ItemArray[1].ToString());
                    zap.SetOib(row.ItemArray[2].ToString());
                    zap.SetAdresa(row.ItemArray[3].ToString());
                    zap.SetTel(row.ItemArray[4].ToString());
                    zap.SetMob(row.ItemArray[5].ToString());
                    zap.SetEmail(row.ItemArray[6].ToString());
                    zap.SetSpol(row.ItemArray[7].ToString());
                    zap.SetPozicijaRada(row.ItemArray[8].ToString());
                    zap.SetKorIme(row.ItemArray[9].ToString());
                }
                tb_iz_ime.Text = zap.GetIme();
                tb_iz_prezime.Text = zap.GetPrezime();
                tb_iz_oib.Text = zap.GetOib();
                tb_iz_adresa.Text = zap.GetAdresa();
                tb_iz_tel.Text = zap.GetTel();
                tb_iz_mob.Text = zap.GetMob();
                tb_iz_email.Text = zap.GetEmail();

                if (zap.GetSpol() == "M") rb_iz_musko.Checked = true;
                else if (zap.GetSpol() == "Z") rb_iz_zensko.Checked = true;

                isprazniCmbox(cmbox_iz_pozRada);
                cmbox_iz_pozRada.Text = zap.GetPozicijaRada();
                foreach (string val in listaPozRada) cmbox_iz_pozRada.Items.Add(val);

            }

        }
        private void btn_iz_spremiIzmjeneKorRacuna_Click(object sender, EventArgs e)
        {
            Zaposlenik zap = new Zaposlenik();
            zap.SetIme(tb_iz_ime.Text);
            zap.SetPrezime(tb_iz_prezime.Text);
            zap.SetOib(tb_iz_oib.Text);
            zap.SetAdresa(tb_iz_adresa.Text);
            zap.SetTel(tb_iz_tel.Text);
            zap.SetMob(tb_iz_mob.Text);
            zap.SetEmail(tb_iz_email.Text);
            zap.SetPozicijaRada(cmbox_iz_pozRada.Text);
            if (rb_iz_musko.Checked) zap.SetSpol("M");
            else if (rb_iz_zensko.Checked) zap.SetSpol("Z");
      



            string query = "UPDATE zaposlenici SET ime = '" + zap.GetIme() + "',prezime = '" + zap.GetPrezime() + "',adresa = '" + zap.GetAdresa() + "',tel = '" + zap.GetTel() + "',mob = '" + zap.GetMob() + "',email = '" + zap.GetEmail() + "',spol = '" + zap.GetSpol() + "',pozicijaRada = '" + zap.GetPozicijaRada() + "' where oib = '"+tb_iz_oib.Text+"'";          
            izvršiUpitBezPovrata(query, 1);

            foreach (Control c in gb_iz_osobniPodaci.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_iz_dodatneInfo.Controls) if (c is TextBox) c.Text = "";
            cmbox_iz_odaberiteZaposlenika.Text = "";
            cmbox_iz_pozRada.Text = "";
            rb_iz_musko.Checked = true;

        }

        //Prikaži statistiku majstora na TPT-u
        private void btn_IZB_prikaziStatistikuTPT_direktor_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Visible = true;
            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Location = radnaPlohaPoint;

            lb_odaberiteRadnika_TPT.Visible = true;
            cmbox_odaberiteRadnika_statistikaTPT.Visible = true;

            isprazniCmbox(cmbox_odaberiteRadnika_statistikaTPT);
            isprazniCmbox(cmbox_RazdobljeStatistike_MajstorNaTPT);

            lb_IzrezanihKubika_MajstorNaTPT.Text = "0";
            lb_IzrezanihKlada_MajstorNaTPT.Text = "0";
            lb_granice_MajstorTPT.Text = "";

            string query = "SELECT ime, prezime FROM zaposlenici WHERE pozicijaRada = 'Majstor na TPT-u'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberiteRadnika_statistikaTPT.Items.Add(row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString());
            }
            cmbox_odaberiteRadnika_statistikaTPT.Items.Add("Ukupno");

            foreach (string value in listaRazdobljeZaStat)
            {
                cmbox_RazdobljeStatistike_MajstorNaTPT.Items.Add(value);
            }

            chart_Statistika_MajstorNaTPT.Visible = false;

        }

        //Pretraži skladište
        private void btn_IZB_pretraziSkladiste_direktor_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_pretraziSkladiste.Visible = true;
            pnl_RP_pretraziSkladiste.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_PertraziPo_pp);
            isprazniCmbox(cmbox_prog_skla_spec);
            tb_prog_nazivi.Text = "";
            cmbox_pretraziPo.Text = "";
            cmbox_prog_skla_spec.Text = "";

            dgv_prog_rezultati.DataSource = null;

            foreach (string val in listaPretraziSkladiste) cmbox_PertraziPo_pp.Items.Add(val);
        }

        //Pretraži popratnice
        private void btn_IZB_pretraziPopratnice_direktor_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_pretraziPopratnice.Visible = true;
            pnl_RP_pretraziPopratnice.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_prikaziPopratnicu_datumUpisa);
            rtb_prikaziPopratnice.Text = "";

            string query = "select broj, RegOznakaKamiona, RegOznakaPrikolice, Kupac from popratnice where datumOtpremanja = '" + datePicker_prikaziPopratnicu.Value.ToShortDateString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach(DataRow row in dt.Rows)
            {
                cmbox_prikaziPopratnicu_datumUpisa.Items.Add("Br " + row.ItemArray[0].ToString() + ", " + row.ItemArray[1].ToString() + " / " + row.ItemArray[2].ToString() + " - " + row.ItemArray[3].ToString());
            }

        }
        private void btn_prikaziPopratnicu_Click(object sender, EventArgs e)
        {
            string val = cmbox_prikaziPopratnicu_datumUpisa.SelectedItem.ToString();
            string num = "";
            for(int i = 3; val[i] != ','; i++)
            {
                num += val[i];
            }

            string query = "select tekst from popratnice where broj = '" + num + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 1);
            foreach (DataRow row in dt.Rows) rtb_prikaziPopratnice.Text = row.ItemArray[0].ToString();
        }
        private void datePicker_prikaziPopratnicu_ValueChanged(object sender, EventArgs e)
        {
            isprazniCmbox(cmbox_prikaziPopratnicu_datumUpisa);

            string query = "select broj, RegOznakaKamiona, RegOznakaPrikolice, Kupac from popratnice where datumOtpremanja = '" + datePicker_prikaziPopratnicu.Value.ToShortDateString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                cmbox_prikaziPopratnicu_datumUpisa.Items.Add("Br " + row.ItemArray[0].ToString() + ", " + row.ItemArray[1].ToString() + " / " + row.ItemArray[2].ToString() + " - " + row.ItemArray[3].ToString());
            }
        }

        //Napiši radni nalog
        private void btn_IZB_napisiRadniNalog_direktor_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_napisiRadniNalog.Visible = true;
            pnl_RP_napisiRadniNalog.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_nalogPozicijaRada);
            isprazniCmbox(cmbox_nalogRadnik);
            tb_RadniNalogHead.Text = "";
            tb_radniNalogTekst.Text = "";

            cmbox_nalogPozicijaRada.Items.Add("Poslovoda pilane");
            cmbox_nalogPozicijaRada.Items.Add("Poslovoda elementare");
            cmbox_nalogPozicijaRada.Items.Add("Majstor na održavanju");
            cmbox_nalogPozicijaRada.Items.Add("Čistač");
            cmbox_nalogPozicijaRada.Items.Add("Radnik u kuhinji");
            datePicker_nalogZaDatum.MinDate = DateTime.Now;
        }

        //Prikaži radni nalog
        private void btn_IZB_prikaziRadniNalog_direktor_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_poslovodaPrikaziRadniNalog.Visible = true;
            pnl_RP_poslovodaPrikaziRadniNalog.Location = radnaPlohaPoint;

            btn_poslovodaIsprintajRN.Enabled = false;

            isprazniCmbox(cmbox_poslovodaRNPozicijaRada);
            isprazniCmbox(cmbox_poslovodaRNRadnik);
            isprazniCmbox(cmbox_vrijemePisanjaRN);

            rtb_RadniNalogTxt.Text = "";

            foreach (string val in listaPozRada)
            {
                cmbox_poslovodaRNPozicijaRada.Items.Add(val);
            }
        }



        //**********    PORTIR

        //Upiši novi kamion
        private void btn_spremiNoviKamion_Click(object sender, EventArgs e)
        {
            Kamion noviKamion = new Kamion();

            noviKamion.SetregOznakaKamiona(tb_regOznakaKamiona.Text);
            noviKamion.SetregOznakaPrikolice(tb_regOznakaPrikolice.Text);
            noviKamion.SetimeVozača(tb_imeVozača.Text);
            noviKamion.SetPrezimeVozača(tb_prezimeVozača.Text);
            noviKamion.SetOIBVozača(tb_oibVozača.Text);
            noviKamion.SetDatumUlaska(DateTime.Now.ToShortDateString());
            noviKamion.SetVrijemeUlaska(getSadašnjeVrijeme());
            noviKamion.SetOIBPortira(TrenutnoPrijavljenKorisnik.GetOib());

            string query = "insert into kamioni_u_tvrtki(regOznakaKamiona,regOznakaPrikolice,imeVozaca, prezimeVozaca, OIBVozaca, datumUlaska, vrijemeUlaska, OIBportira) values ('" + noviKamion.GetregOznakaKamiona() + "','" + noviKamion.GetregOznakaPrikolice() + "','" + noviKamion.GetimeVozača() + "','" + noviKamion.GetPrezimeVozača() + "','" + noviKamion.GetOIBVozača() + "','" + noviKamion.GetDatumUlaska() + "','" + noviKamion.GetVrijemeUlaska() + "','" + noviKamion.GetOIBPortira() + "') ";
            izvršiUpitBezPovrata(query, 1);

            foreach (Control c in gb_podaciOVozilu.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }

        }
        private void btn_IZB_UpisiNoviKamion_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisiNoviKamion.Visible = true;
            pnl_RP_upisiNoviKamion.Location = radnaPlohaPoint;
        }

        //Prikaži trenutne kamione
        private void btn_IZB_PrikaziTrenutneKamione_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_prikazTrenutnihKamiona.Visible = true;
            pnl_RP_prikazTrenutnihKamiona.Location = radnaPlohaPoint;

            string query = "select regOznakaKamiona as 'Kamion', regOznakaPrikolice as 'Prikolica', imeVozaca as 'Ime', prezimeVozaca as 'Prezime',OIBVozaca as 'OIB Vozaca', datumUlaska as 'Datum ulaska', vrijemeUlaska as 'Vrijeme ulaska' from kamioni_u_tvrtki where datumIzlaska IS NULL";
            dgw_trenutniKamioni.DataSource = izvršiUpitSPovratomDT(query, 1);


        }
        private void btn_odlazakKamiona_Click(object sender, EventArgs e)
        {
            Kamion odlazeciKamion = new Kamion();

            foreach (DataGridViewRow row in dgw_trenutniKamioni.SelectedRows)
            {
                odlazeciKamion.SetOIBVozača(row.Cells[4].Value.ToString());
                odlazeciKamion.SetDatumUlaska(row.Cells[5].Value.ToString());
                odlazeciKamion.SetVrijemeUlaska(row.Cells[6].Value.ToString());
            }
            string query = "UPDATE dbo.kamioni_u_tvrtki SET datumIzlaska = '" + DateTime.Now.ToShortDateString() + "', vrijemeIzlaska = '" + getSadašnjeVrijeme() + "' WHERE OIBVozaca ='" + odlazeciKamion.GetOIBVozača() + "' AND datumIzlaska IS NULL";

            izvršiUpitBezPovrata(query, 0);

            btn_IZB_PrikaziTrenutneKamione.PerformClick();

        }

        //Prikaži ostale kamione
        private void btn_IZB_prikaziOstaleKamione_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_prikaziOstaleKamione.Visible = true;
            pnl_RP_prikaziOstaleKamione.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_pretraziPo);
            foreach (string value in listaPretraziPo)
            {
                cmbox_pretraziPo.Items.Add(value);
            }

            dgv_ostaliKamioni.DataSource = null;



        }
        private void btn_pretraziOstaleKamione_Click(object sender, EventArgs e)
        {

            string query = "SELECT regOznakaKamiona as 'Registracija kamiona', regOznakaPrikolice as 'Registracija prikolice', imeVozaca as 'Ime vozača', prezimeVozaca as 'Prezime vozača',OIBVozaca as 'OIB vozača', datumUlaska as 'Datum ulaska', vrijemeUlaska as 'Vrijeme ulaska', datumIzlaska as 'Datum izlaska', vrijemeIzlaska as 'Vrijeme izlaska' FROM kamioni_u_tvrtki ";


            if (datePicker_down.Checked && datePicker_up.Checked)
            {
                query += "WHERE datumUlaska BETWEEN '" + datePicker_down.Value + "' AND '" + datePicker_up.Value + "' ";
            }
            else if (datePicker_down.Checked)
            {
                query += "WHERE datumUlaska = '" + datePicker_down.Value + "' ";
            }

            if (cmbox_pretraziPo.SelectedIndex != -1)
            {
                string value = "";
                if (cmbox_pretraziPo.SelectedItem.ToString() == "Registracija kamiona") value = "RegOznakaKamiona";
                else if (cmbox_pretraziPo.SelectedItem.ToString() == "Registracija prikolice") value = "RegOznakaPrikolice";
                else if (cmbox_pretraziPo.SelectedItem.ToString() == "Ime vozača") value = "imeVozaca";
                else if (cmbox_pretraziPo.SelectedItem.ToString() == "Prezime vozača") value = "prezimeVozaca";
                else if (cmbox_pretraziPo.SelectedItem.ToString() == "OIB vozača") value = "OIBVozaca";
                else if (cmbox_pretraziPo.SelectedItem.ToString() == "OIB portira") value = "OIBportira";


                if (datePicker_down.Checked)
                {
                    query += " AND " + value + " = '" + tb_kamioniPretraziPo.Text + "' ";
                }
                else
                {
                    query += "WHERE " + value + " = '" + tb_kamioniPretraziPo.Text + "' ";
                }

            }

            dgv_ostaliKamioni.DataSource = izvršiUpitSPovratomDT(query, 1);

        }
        private void cmbox_pretraziPo_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_kamioniPretraziPo.Text = "";
        }





        //**********    POSLOVOĐA PILANE

        //Upiši klade
        private void btn_IZB_UpisiKlade_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisiKlade.Visible = true;
            pnl_RP_upisiKlade.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_odaberiteKamion);
            isprazniCmbox(cmbox_odaberiteSkladiste);


            string query = "SELECT regOznakaKamiona FROM kamioni_u_tvrtki WHERE datumIzlaska IS NULL";

            DataTable dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberiteKamion.Items.Add(row.ItemArray[0].ToString());
            }


            query = "SELECT naziv FROM skladista where broj like '2%' ";

            dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberiteSkladiste.Items.Add(row.ItemArray[0].ToString());
            }

        }
        private void btn_spremiUpisaneKlade_Click(object sender, EventArgs e)
        {
            string query = "select broj from skladista where naziv = '" + cmbox_odaberiteSkladiste.SelectedItem.ToString() + "'";
            string brojSkladista = "";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                brojSkladista = row.ItemArray[0].ToString();
            }


            int num = 0;
            bool error = false;
            for (int i = 0; i < dgv_podaciOKladama.Rows.Count - 1; i++)
            {
                query = "select count(broj) from plocice where broj = '" + dgv_podaciOKladama.Rows[i].Cells[0].Value.ToString() + "'";
                dt = izvršiUpitSPovratomDT(query, 0);
                foreach (DataRow row in dt.Rows) Int32.TryParse(row.ItemArray[0].ToString(), out num);

                if(num == 0)
                {
                    query = "insert into plocice (broj, sumarija, mjestoUtovara, duljina, debljina, obujam, sifraDrveta) values ('" + dgv_podaciOKladama.Rows[i].Cells[0].Value.ToString() + "','" + tb_sumarija.Text + "','" + tb_mjestoUtovara.Text + "','" + dgv_podaciOKladama.Rows[i].Cells[2].Value + "','" + dgv_podaciOKladama.Rows[i].Cells[3].Value + "','" + dgv_podaciOKladama.Rows[i].Cells[4].Value + "','" + dgv_podaciOKladama.Rows[i].Cells[1].Value.ToString() + "')";     
                    query += "\n insert into klade (brojPlocice,regKamiona,ImePrezOtpremnika,datumOtpremanja,vrijemeOtpremanja,brojSkladista,OIBPoslovode,datumUpisa,vrijemeUpisa) values ('" + dgv_podaciOKladama.Rows[i].Cells[0].Value.ToString() + "','" + cmbox_odaberiteKamion.SelectedItem.ToString() + "','" + tb_imePrezimeOtpremnika.Text + "','" + datePicker_datumOtpremanja.Value + "','" + tb_vrijemeOtpremanja.Text + "','" + brojSkladista + "','" + TrenutnoPrijavljenKorisnik.GetOib() + "','" + getDanašnjiDatum() + "','" + getSadašnjeVrijeme() + "')";
                    izvršiUpitBezPovrata(query, 1);
                }
                else
                {
                    error = true;
                    MessageBox.Show("Plocica " + dgv_podaciOKladama.Rows[i].Cells[0].Value.ToString() + " je vec unesena!");
                }
                
            }

            if(error == false)
            {
                foreach (Control c in gb_podaciOPopratnici.Controls) if (c is TextBox) c.Text = "";
                foreach (Control c in gb_podaciOPopratnici.Controls) if (c is ComboBox) c.ResetText();
                foreach (Control c in gb_podaciOSkaldistu.Controls) if (c is ComboBox) c.ResetText();
            }
           
            dgv_podaciOKladama.Rows.Clear();
            dgv_podaciOKladama.Refresh();

        }

        //Napiši radni nalog
        private void btn_IZB_NapisiRadniNalog_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_napisiRadniNalog.Visible = true;
            pnl_RP_napisiRadniNalog.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_nalogPozicijaRada);
            isprazniCmbox(cmbox_nalogRadnik);
            tb_RadniNalogHead.Text = "";
            tb_radniNalogTekst.Text = "";

            foreach (string val in listaPozRadaPoslovodaPilane)
            {
                cmbox_nalogPozicijaRada.Items.Add(val);
            }

            datePicker_nalogZaDatum.MinDate = DateTime.Now;


        }
        private void cmbox_nalogPozicijaRada_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "select ime, prezime from zaposlenici where pozicijaRada = '" + cmbox_nalogPozicijaRada.SelectedItem.ToString() + "'";

            DataTable dt = izvršiUpitSPovratomDT(query, 0);

            isprazniCmbox(cmbox_nalogRadnik);
            string ime, prezime;


            foreach (DataRow row in dt.Rows)
            {
                ime = row.ItemArray[0].ToString();
                prezime = row.ItemArray[1].ToString();
                cmbox_nalogRadnik.Items.Add(ime + " " + prezime);
            }
        }
        private void tb_radniNalogTekst_MouseClick(object sender, MouseEventArgs e)
        {
            int rb = 0;

            string query = "select max(rb) from nalozi";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                Int32.TryParse(row.ItemArray[0].ToString(), out rb);
                rb++;
            }
            tb_RadniNalogHead.Enabled = true;
            tb_RadniNalogHead.Text = "\tRADNI NALOG" + Environment.NewLine + Environment.NewLine;
            tb_RadniNalogHead.Text += "Izradio: _ _ _ _ _ _ _ " + TrenutnoPrijavljenKorisnik.GetIme() + " " + TrenutnoPrijavljenKorisnik.GetPrezime() + Environment.NewLine;
            tb_RadniNalogHead.Text += "Datum izrade: _ _ _ _" + getDanašnjiDatum() + " " + getSadašnjeVrijeme() + Environment.NewLine;
            tb_RadniNalogHead.Text += "Radnik: _ _ _ _ _ _ _ " + cmbox_nalogRadnik.SelectedItem.ToString() + Environment.NewLine;
            tb_RadniNalogHead.Text += "Za datum: _ _ _ _ _ _" + datePicker_nalogZaDatum.Value.Date.ToShortDateString() + Environment.NewLine;
            tb_RadniNalogHead.Text += "Broj naloga: _ _ _ _ _ " + rb.ToString() + Environment.NewLine;
            tb_RadniNalogHead.Text += "____________________________________" + Environment.NewLine + Environment.NewLine;
            tb_RadniNalogHead.Enabled = false;
        }
        private void btn_spremiRadniNalog_Click(object sender, EventArgs e)
        {
            string oib = "";

            string ime = "", prezime = "";
            string word = cmbox_nalogRadnik.SelectedItem.ToString();
            bool space = false;

            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == ' ') space = true;
                else
                {
                    if (space == false) ime += word[i];
                    else prezime += word[i];
                }
            }

            string query = "select OIB from zaposlenici where ime = '" + ime + "' AND prezime = '" + prezime + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                oib = row.ItemArray[0].ToString();
            }


            int rb = 0; ;
            query = "select max(rb) from nalozi";
            dt = izvršiUpitSPovratomDT(query, 1);
            foreach (DataRow row in dt.Rows)
            {
                if (row.ItemArray[0] is null) row.ItemArray[0] = "0";
                Int32.TryParse(row.ItemArray[0].ToString(), out rb);
            }
            rb++;

            string zaglavlje = "";
            zaglavlje = "\tRADNI NALOG" + Environment.NewLine + Environment.NewLine;
            zaglavlje += "Izradio: _ _ _ _ _ _ _ " + TrenutnoPrijavljenKorisnik.GetIme() + " " + TrenutnoPrijavljenKorisnik.GetPrezime() + Environment.NewLine;
            zaglavlje += "Datum izrade: _ _ _ _" + getDanašnjiDatum() + " " + getSadašnjeVrijeme() + Environment.NewLine;
            zaglavlje += "Radnik: _ _ _ _ _ _ _ " + cmbox_nalogRadnik.SelectedItem.ToString() + Environment.NewLine;
            zaglavlje += "Za datum: _ _ _ _ _ _" + datePicker_nalogZaDatum.Value.Date.ToShortDateString() + Environment.NewLine;
            zaglavlje += "Broj naloga: _ _ _ _ _ " + rb.ToString() + Environment.NewLine;
            zaglavlje += "____________________________________" + Environment.NewLine + Environment.NewLine;


            query = "insert into nalozi (tekst, OIBIzradivaca, OIBRadnika, datumIzdavanja, vrijemeIzdavanja, datumRada) values ('" + zaglavlje + tb_radniNalogTekst.Text + "','" + TrenutnoPrijavljenKorisnik.GetOib() + "','" + oib + "','" + getDanašnjiDatum() + "','" + getSadašnjeVrijeme() + "','" + datePicker_nalogZaDatum.Value.ToShortDateString() + "')";
            izvršiUpitBezPovrata(query, 1);

            foreach (Control c in gb_tekstRadnogNaloga.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_radniNalogHead.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_PodaciORadnomNalogu.Controls) if (c is ComboBox) c.ResetText();

        }

        //Prikaži radni nalog
        private void btn_IZB_poslovodaPrikaziRadniNalog_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_poslovodaPrikaziRadniNalog.Visible = true;
            pnl_RP_poslovodaPrikaziRadniNalog.Location = radnaPlohaPoint;

            btn_poslovodaIsprintajRN.Enabled = false;

            isprazniCmbox(cmbox_poslovodaRNPozicijaRada);
            isprazniCmbox(cmbox_poslovodaRNRadnik);
            isprazniCmbox(cmbox_vrijemePisanjaRN);

            rtb_RadniNalogTxt.Text = "";

            foreach (string val in listaPozRadaPoslovodaPilane)
            {
                cmbox_poslovodaRNPozicijaRada.Items.Add(val);
            }
            cmbox_poslovodaRNPozicijaRada.Items.Add("Poslovoda pilane");


        }
        private void cmbox_poslovodaRNPozicijaRada_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda pilane" || cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda elementare")
            {
                cmbox_poslovodaRNRadnik.Items.Add(TrenutnoPrijavljenKorisnik.GetIme() + " " + TrenutnoPrijavljenKorisnik.GetPrezime());
            }
            else
            {
                string query = "select ime, prezime from zaposlenici where pozicijaRada = '" + cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() + "'";

                DataTable dt = izvršiUpitSPovratomDT(query, 0);

                isprazniCmbox(cmbox_poslovodaRNRadnik);
                cmbox_poslovodaRNRadnik.Text = "";

                string ime, prezime;

                foreach (DataRow row in dt.Rows)
                {
                    ime = row.ItemArray[0].ToString();
                    prezime = row.ItemArray[1].ToString();
                    cmbox_poslovodaRNRadnik.Items.Add(ime + " " + prezime);
                }
            }
            cmbox_vrijemePisanjaRN.Text = "";



        }
        private void btn_poslovodaPrikaziRadniNalog_Click_1(object sender, EventArgs e)
        {

            string ime = "", prezime = "";
            string word = cmbox_poslovodaRNRadnik.SelectedItem.ToString();
            bool space = false;
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == ' ') space = true;
                else
                {
                    if (space == false) ime += word[i];
                    else prezime += word[i];
                }
            }

            string datum = "", vrijeme = "";
            word = cmbox_vrijemePisanjaRN.SelectedItem.ToString();
            space = false;
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == ' ') space = true;
                else
                {
                    if (space == false) datum += word[i];
                    else vrijeme += word[i];
                }
            }

            string oib = "";
            string query = "select OIB from zaposlenici where ime = '" + ime + "' AND prezime = '" + prezime + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                oib = row.ItemArray[0].ToString();
            }

                      
            string oibIzradivaca = "";

            if (TrenutnoPrijavljenKorisnik.GetOib() == "0000000000")
            {
                query = "select tekst from nalozi where OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "' AND datumIzdavanja = '" + datum + "'  AND  vrijemeIzdavanja = '" + vrijeme + "'";
            }
            else if ((cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda pilane" || cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda elementare") && (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda elementare"))
            {
                oibIzradivaca = "0000000000";
                query = "select tekst from nalozi where OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "' AND datumIzdavanja = '" + datum + "'  AND  vrijemeIzdavanja = '" + vrijeme + "' AND OIBIzradivaca = '" + oibIzradivaca + "'";

            }
            else
            {
                oibIzradivaca = TrenutnoPrijavljenKorisnik.GetOib();
                query = "select tekst from nalozi where OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "' AND datumIzdavanja = '" + datum + "'  AND  vrijemeIzdavanja = '" + vrijeme + "' AND OIBIzradivaca = '" + oibIzradivaca + "'";
            }

            string mainText = "";
            dt = izvršiUpitSPovratomDT(query, 1);
            foreach (DataRow row in dt.Rows)
            {
                mainText = row.ItemArray[0].ToString();
            }

            //prikazivanje teksta

            rtb_RadniNalogTxt.Text = mainText;

            btn_poslovodaIsprintajRN.Enabled = true;
        }
        private void cmbox_vrijemePisanjaRN_Click(object sender, EventArgs e)
        {
            if (cmbox_poslovodaRNRadnik.SelectedIndex != -1)
            {

                isprazniCmbox(cmbox_vrijemePisanjaRN);

                string oib = "";
                string ime = "", prezime = "";
                string word = cmbox_poslovodaRNRadnik.SelectedItem.ToString();
                bool space = false;

                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == ' ') space = true;
                    else
                    {
                        if (space == false) ime += word[i];
                        else prezime += word[i];
                    }
                }

                string query = "select OIB from zaposlenici where ime = '" + ime + "' AND prezime = '" + prezime + "'";
                DataTable dt = izvršiUpitSPovratomDT(query, 0);
                foreach (DataRow row in dt.Rows)
                {
                    oib = row.ItemArray[0].ToString();
                }



                string oibIzradivaca = "";

                if(TrenutnoPrijavljenKorisnik.GetOib() == "0000000000")
                {
                    query = "select datumIzdavanja, vrijemeIzdavanja from nalozi where  OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "'";
                }
                else if ((cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda pilane" || cmbox_poslovodaRNPozicijaRada.SelectedItem.ToString() == "Poslovoda elementare") && (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" || TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda elementare"))
                {
                    oibIzradivaca = "0000000000";
                    query = "select datumIzdavanja, vrijemeIzdavanja from nalozi where  OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "' AND OIBIzradivaca = '" + oibIzradivaca + "'";

                }
                else
                {
                    oibIzradivaca = TrenutnoPrijavljenKorisnik.GetOib();
                    query = "select datumIzdavanja, vrijemeIzdavanja from nalozi where  OIBRadnika = '" + oib + "' AND datumRada = '" + datePicker_RadniNalogDatum.Value.ToShortDateString() + "' AND OIBIzradivaca = '" + oibIzradivaca + "'";
                }

                dt = izvršiUpitSPovratomDT(query, 0);
                foreach (DataRow row in dt.Rows)
                {
                    word = row.ItemArray[0].ToString();
                    string datum = "";

                    for (int i = 0; word[i] != ' '; i++)
                    {
                        datum += word[i];
                    }

                    cmbox_vrijemePisanjaRN.Items.Add(datum + " " + row.ItemArray[1].ToString());

                }
            }
        }
        private void btn_poslovodaIsprintajRN_Click(object sender, EventArgs e)
        {
            if (printDialog_RadniNalog.ShowDialog() == DialogResult.OK)
            {
                printDocument_RadniNalog.Print();
            }
        }

        //Prikaži statistiku majstora na TPT-u
        private void btn_polovodaPilane_PrikaziStatistikuMajstoraNaTPT_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Visible = true;
            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Location = radnaPlohaPoint;      

            lb_odaberiteRadnika_TPT.Visible = true;
            cmbox_odaberiteRadnika_statistikaTPT.Visible = true;

            isprazniCmbox(cmbox_odaberiteRadnika_statistikaTPT);
            isprazniCmbox(cmbox_RazdobljeStatistike_MajstorNaTPT);

            lb_IzrezanihKubika_MajstorNaTPT.Text = "0";
            lb_IzrezanihKlada_MajstorNaTPT.Text = "0";
            lb_granice_MajstorTPT.Text = "";

            string query = "SELECT ime, prezime FROM zaposlenici WHERE pozicijaRada = 'Majstor na TPT-u'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach(DataRow row in dt.Rows)
            {
                cmbox_odaberiteRadnika_statistikaTPT.Items.Add(row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString());
            }
            cmbox_odaberiteRadnika_statistikaTPT.Items.Add("Ukupno");

            foreach (string value in listaRazdobljeZaStat)
            {
                cmbox_RazdobljeStatistike_MajstorNaTPT.Items.Add(value);
            }

            chart_Statistika_MajstorNaTPT.Visible = false;
        }

        //Pretraži skladište
        private void btn_IZB_pretragaSkladista_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_pretraziSkladiste.Visible = true;
            pnl_RP_pretraziSkladiste.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_PertraziPo_pp);
            isprazniCmbox(cmbox_prog_skla_spec);
            tb_prog_nazivi.Text = "";
            cmbox_pretraziPo.Text = "";
            cmbox_prog_skla_spec.Text = "";

            dgv_prog_rezultati.DataSource = null;
            

            foreach (string val in listaPretraziSkladiste) cmbox_PertraziPo_pp.Items.Add(val);

        }
        private void cmbox_PertraziPo_pp_SelectedIndexChanged(object sender, EventArgs e)
        {
            isprazniCmbox(cmbox_prog_skla_spec);

            tb_prog_nazivi.Text = "";
            cmbox_prog_skla_spec.Text = "";

            if(cmbox_PertraziPo_pp.SelectedIndex != -1)
            {
              
                if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Broju klade")
                {
                    lb_prog_naziv.Enabled = true;
                    tb_prog_nazivi.Enabled = true;
                    lb_prog_skl_spec.Enabled = false;
                    cmbox_prog_skla_spec.Enabled = false;
                    lb_prog_naziv.Text = "Broj klade";
                }
                else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Broju paketa")
                {
                    lb_prog_skl_spec.Text = "Odaberite skladište";
                    lb_prog_naziv.Enabled = true;
                    tb_prog_nazivi.Enabled = true;
                    lb_prog_skl_spec.Enabled = false;
                    cmbox_prog_skla_spec.Enabled = false;
                    lb_prog_naziv.Text = "Broj paketa";
                }
                else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Skladištu")
                {
                    lb_prog_skl_spec.Enabled = true;
                    cmbox_prog_skla_spec.Enabled = true;
                    lb_prog_skl_spec.Text = "Odaberite skladište";
                    lb_prog_naziv.Enabled = false;
                    tb_prog_nazivi.Enabled = false;

                    string query = "select naziv from skladista";
                    DataTable dt = izvršiUpitSPovratomDT(query, 0);
                    foreach (DataRow row in dt.Rows) cmbox_prog_skla_spec.Items.Add(row.ItemArray[0].ToString());
                }
                else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Specifikacijama klade")
                {
                    lb_prog_skl_spec.Text = "Pretraži po:";
                    
                    lb_prog_naziv.Enabled = true;
                    tb_prog_nazivi.Enabled = true;
                    lb_prog_skl_spec.Enabled = true;
                    cmbox_prog_skla_spec.Enabled = true;

                    cmbox_prog_skla_spec.Items.Add("Šifri drveta");
                    cmbox_prog_skla_spec.Items.Add("Vrsti drveta");
                    cmbox_prog_skla_spec.Items.Add("Klasi drveta");


                    lb_prog_naziv.Text = "Upišite vrijednost:";
                }
                else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Specifikacijama paketa")
                {
                    lb_prog_skl_spec.Text = "Pretraži po:";

                    lb_prog_naziv.Enabled = true;
                    tb_prog_nazivi.Enabled = true;
                    lb_prog_skl_spec.Enabled = true;
                    cmbox_prog_skla_spec.Enabled = true;

                    cmbox_prog_skla_spec.Items.Add("Vrsti drveta");
                    cmbox_prog_skla_spec.Items.Add("Klasi drveta");


                    lb_prog_naziv.Text = "Upišite vrijednost:";
                }

            }


        }
        private void btn_prog_Pretrazi_Click(object sender, EventArgs e)
        { 
            if(cmbox_PertraziPo_pp.SelectedItem.ToString() == "Broju klade")
            {
                gb_prog_rezultati.Visible = true;

                string query = "SELECT skl.naziv as 'Skladiste', p.obujam as 'Obujam klade [m3]', s.vrsta as 'Vrsta drveta', s.klasa as 'Klasa drveta', p.duljina as 'Duljina klade [cm]', p.debljina as 'Debljina klade [cm]', k.datumUpisa as' Datum upisa',p.sumarija as Šumarija, k.izrezana as Izrezana, k.datumRezanja as 'Datum rezanja'FROM plocice as p, sifra_drveta as s, klade as k, skladista skl WHERE p.broj = k.brojPlocice AND p.sifraDrveta = s.sifra AND k.brojSkladista = skl.broj AND p.broj = '" + tb_prog_nazivi.Text+"'";
                DataTable dt = izvršiUpitSPovratomDT(query, 1);
                dgv_prog_rezultati.DataSource = dt;

            }
            else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Broju paketa")
            {
                gb_prog_rezultati.Visible = true;

                string query = "SELECT s.naziv as 'Skladište', p.obujam as 'Obujam paketa [m3]',p.kolicinaProizvoda as 'Količina proizvoda [kom]', p.vrstaDrveta as 'Vrsta drveta', p.klasaDrveta as 'Klasa drveta', p.duljinaProizvoda as 'Duljina proizvoda [cm]', p.sirinaProizvoda as 'Širina proizvoda [cm]', p.visinaProizvoda as 'Visina proizvoda [cm]',p.stanjeDrveta as 'Stanje drveta',p.datumUpisaSusara as 'Datum upisa u sušaru', p.datumUpisaElementara as 'Datum upisa u elementaru', p.datumUpisaSklGotoveRobe as 'Datum upisa u skladište gotove robe' FROM paketi p, skladista s WHERE p.brojSkladista = s.broj and p.rb = '" + tb_prog_nazivi.Text + "' ";

                if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane")
                {
                    query += "AND p.stanjeDrveta = 'SIROVO'";
                }
                else if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda elementare")
                {
                    query += "AND p.stanjeDrveta != 'SIROVO'";
                }
                DataTable dt = izvršiUpitSPovratomDT(query, 1);
                dgv_prog_rezultati.DataSource = dt;

            }
            else if (cmbox_PertraziPo_pp.SelectedItem.ToString() == "Skladištu")
            {
                gb_prog_rezultati.Visible = true;
                string broj = "";
                string query = "select broj from skladista where naziv = '"+cmbox_prog_skla_spec.SelectedItem.ToString()+"'";
                DataTable dt = izvršiUpitSPovratomDT(query, 0);
                foreach (DataRow row in dt.Rows) broj = row.ItemArray[0].ToString();

                if(broj[0] == '2')
                {
                    query = "select count(k.brojPlocice) as 'Broj klada [kom]', sum(p.obujam) as 'Ukupan obujam klada [kom]' from klade k, plocice p where brojSkladista = '" + broj + "' and k.brojPlocice = p.broj and k.izrezana is null";
                }
                else if(broj[0] == '5')
                {
                    query = "select stanjeDrveta as 'Stanje drveta u paketu', count(rb) as 'Broj paketa [kom]', sum(obujam) as 'Ukupan obujam paketa [m3]' from paketi where paketi.brojPopratnice is null and paketi.brojSkladista = '"+broj+"' group by stanjeDrveta";
                }
                else
                {
                    query = "select stanjeDrveta as 'Stanje drveta u paketu', count(rb) as 'Broj paketa [kom]', sum(obujam) as 'Ukupan obujam paketa [m3]' from paketi where paketi.brojSkladista = '" + broj + "' group by stanjeDrveta";
                }

                dt = izvršiUpitSPovratomDT(query, 1);
                dgv_prog_rezultati.DataSource = dt;

            }
            else if(cmbox_PertraziPo_pp.SelectedItem.ToString() =="Specifikacijama klade")
            {
                gb_prog_rezultati.Visible = true;
                string query = "";
                if(cmbox_prog_skla_spec.SelectedItem.ToString() == "Šifri drveta")
                {
                    query = "select  skladista.naziv as 'Naziv skladišta', count(plocice.broj) as 'Broj klada', sum(plocice.obujam) as 'Ukupan obujam' from plocice,skladista,klade where plocice.sifraDrveta = '"+tb_prog_nazivi.Text+"' and skladista.broj = klade.brojSkladista and klade.brojPlocice = plocice.broj and klade.izrezana is null group by skladista.naziv";
                }
                else if (cmbox_prog_skla_spec.SelectedItem.ToString() == "Vrsti drveta")
                {
                    query = "select  skladista.naziv as 'Naziv skladišta', count(plocice.broj) as 'Broj klada', sum(plocice.obujam) as 'Ukupan obujam' from plocice,skladista,klade,sifra_drveta where sifra_drveta.sifra = plocice.sifraDrveta AND sifra_drveta.vrsta = '"+tb_prog_nazivi.Text+"'  and plocice.broj = klade.brojPlocice and skladista.broj = klade.brojSkladista and klade.izrezana is null group by skladista.naziv";
                }
                else if (cmbox_prog_skla_spec.SelectedItem.ToString() == "Klasi drveta")
                {
                    query = "select  skladista.naziv as 'Naziv skladišta', count(plocice.broj) as 'Broj klada', sum(plocice.obujam) as 'Ukupan obujam' from plocice,skladista,klade,sifra_drveta where sifra_drveta.sifra = plocice.sifraDrveta AND sifra_drveta.klasa = '" + tb_prog_nazivi.Text + "'  and plocice.broj = klade.brojPlocice and skladista.broj = klade.brojSkladista and klade.izrezana is null group by skladista.naziv";
                }
                DataTable dt = izvršiUpitSPovratomDT(query, 1);
                dgv_prog_rezultati.DataSource = dt;

            }
            else if(cmbox_PertraziPo_pp.SelectedItem.ToString() == "Specifikacijama paketa")
            {
                gb_prog_rezultati.Visible = true;
                string query = "";

                if(cmbox_prog_skla_spec.SelectedItem.ToString() == "Vrsti drveta")
                {
                    query = "select paketi.stanjeDrveta as 'Stanje drveta u paketu', count(paketi.rb) as 'Broj paketa [kom]', sum(paketi.obujam) as 'Obujam paketa [m3]' from paketi where paketi.brojPopratnice is null and  paketi.vrstaDrveta = '" + tb_prog_nazivi.Text + "' GROUP BY paketi.stanjeDrveta";
                }
                else if (cmbox_prog_skla_spec.SelectedItem.ToString() == "Klasi drveta")
                {
                    query = "select paketi.stanjeDrveta as 'Stanje drveta u paketu', count(paketi.rb) as 'Broj paketa [kom]', sum(paketi.obujam) as 'Obujam paketa [m3]' from paketi where paketi.brojPopratnice is null and  paketi.klasaDrveta = '" + tb_prog_nazivi.Text + "' GROUP BY paketi.stanjeDrveta";
                }
                DataTable dt = izvršiUpitSPovratomDT(query, 1);
                dgv_prog_rezultati.DataSource = dt;

            }
        }


        //**********    RADNIK NA BAGERU, FORAJZER, RADNIK NA IZNOSU, RADNIK U ELEMENTARI

        //Prikaži radni nalog za zaposlenike
        private void btn_IZB_PrikaziRadniNalog_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;

            btn_isprintajRN_SAMO_RN.Enabled = false;
        }
        private void cmbox_vrijemePisanjaRN_SAMO_RN_Click(object sender, EventArgs e)
        {
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";

            string query = "select datumIzdavanja, vrijemeIzdavanja from nalozi where OIBRadnika = '" + TrenutnoPrijavljenKorisnik.GetOib() + "' AND datumRada = '" + datePicker_datumIzvrsavanjaRN_SAMO_RN.Value.ToShortDateString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            string datum = "", word = "";

            foreach (DataRow row in dt.Rows)
            {
                word = row.ItemArray[0].ToString();
                datum = "";
                for (int i = 0; word[i] != ' '; i++) datum += word[i];            
                cmbox_vrijemePisanjaRN_SAMO_RN.Items.Add(datum + " " + row.ItemArray[1].ToString());
            }
        }
        private void btn_isprintajRN_SAMO_RN_Click(object sender, EventArgs e)
        {
            if (printDialog_RadniNalog.ShowDialog() == DialogResult.OK)
            {
                printDocument_SAMO_RN.Print();
            }
        }
        private void btn_prikaziRN_SAMO_RN_Click(object sender, EventArgs e)
        {

            string query = "select tekst from nalozi where OIBRadnika = '" + TrenutnoPrijavljenKorisnik.GetOib() + "' AND datumRada = '" + datePicker_datumIzvrsavanjaRN_SAMO_RN.Value.ToShortDateString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 1);

            string mainText = "";
            foreach (DataRow row in dt.Rows) mainText = row.ItemArray[0].ToString();
            
            rtb_txtRN_SAMO_RN.Text = mainText;
            btn_isprintajRN_SAMO_RN.Enabled = true;
        }



        //********** POMOCNI RADNIK PRIJE PILANE

        //Upis klada koje će se izrezati
        private void btn_IZB_upisiKladeKojeCeSeIzrezati_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_UpisiKladeKojeCeSeIzrezati.Visible = true;
            pnl_RP_UpisiKladeKojeCeSeIzrezati.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_OdaberiteMajstoraZaRezanjeKlada);

            string query = "select ime, prezime from zaposlenici where pozicijaRada = 'Majstor na TPT-u'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                cmbox_OdaberiteMajstoraZaRezanjeKlada.Items.Add(row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString());
            }

        }
        private void btn_upisiIzrezanePlocice_Click(object sender, EventArgs e)
        {


            string ime = "", prezime = "";
            string word = cmbox_OdaberiteMajstoraZaRezanjeKlada.SelectedItem.ToString();
            bool space = false;
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == ' ') space = true;
                else
                {
                    if (space == false) ime += word[i];
                    else prezime += word[i];
                }
            }

            string oib = "";
            string query = "select OIB from zaposlenici where ime = '" + ime + "' AND prezime = '" + prezime + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                oib = row.ItemArray[0].ToString();
            }

            for (int i = 0; i < dgv_brojPlocicaZaRezanje.Rows.Count - 1; i++)
            {
                query = "select count(brojPlocice) from klade where izrezana is null and brojPlocice = '" + dgv_brojPlocicaZaRezanje.Rows[i].Cells[0].Value.ToString() + "'";
                dt = izvršiUpitSPovratomDT(query, 0);
                foreach (DataRow row in dt.Rows)
                {
                    if (row.ItemArray[0].ToString() == "0")
                    {
                        MessageBox.Show("Pločica s brojem " + dgv_brojPlocicaZaRezanje.Rows[i].Cells[0].Value.ToString() + "ne postoji na skladištu ili je već izrezana", "Greška");
                    }
                    else
                    {
                        query = "update klade set izrezana = '1', datumRezanja = '" + getDanašnjiDatum() + "', vrijemeRezanja = '" + getSadašnjeVrijeme() + "', OIBmajstoraTPT = '" + oib + "' where brojPlocice = '" + dgv_brojPlocicaZaRezanje.Rows[i].Cells[0].Value.ToString() + "' ";
                        izvršiUpitBezPovrata(query, 1);
                    }
                }
               
            }


            dgv_brojPlocicaZaRezanje.Rows.Clear();
            dgv_brojPlocicaZaRezanje.Refresh();


        }

        //Upis dugačkih klada
        private void btn_IZB_UpisDugackihKladaZaPiljenje_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisDugackihKladaZaPiljenje.Visible = true;
            pnl_RP_upisDugackihKladaZaPiljenje.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_odaberiteSkaldisteZaPiljenjeKlada);
            tb_brojPlociceZaPiljenje.Text = "";

            string query = "SELECT naziv FROM skladista where broj like '2%'";

            DataTable dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberiteSkaldisteZaPiljenjeKlada.Items.Add(row.ItemArray[0].ToString());
            }
        }
        private void tb_brojPlociceZaPiljenje_Leave(object sender, EventArgs e)
        {
            string query = "select count(brojPlocice) from klade where izrezana is null and  brojPlocice ='" + tb_brojPlociceZaPiljenje.Text + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach(DataRow row in dt.Rows)
            {
                if (row.ItemArray[0].ToString() == "0")
                {
                    MessageBox.Show("Pločica s brojem " + tb_brojPlociceZaPiljenje.Text + "ne postoji na skladištu ili je već izrezana", "Greška");
                    tb_brojPlociceZaPiljenje.Text = "";
                }

            }
          

        } 
        private void btn_upisiKladuZaPiljenje_Click(object sender, EventArgs e)
        {
            string query = "SELECT p.broj, p.sumarija, p.mjestoUtovara, k.regKamiona, k.ImePrezOtpremnika, k.datumOtpremanja, k.vrijemeOtpremanja, k.OIBposlovode, k.datumUpisa, k.vrijemeUpisa FROM plocice as p, klade as k WHERE p.broj = '" + tb_brojPlociceZaPiljenje.Text + "' AND p.broj = k.brojPlocice ";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            Klade klada = new Klade();
            foreach (DataRow row in dt.Rows)
            {
                klada.SetBrojPlocice(row.ItemArray[0].ToString());
                klada.SetSumarija(row.ItemArray[1].ToString());
                klada.SetMjestoUtovara(row.ItemArray[2].ToString());
                klada.SetRegKamiona(row.ItemArray[3].ToString());
                klada.SetImePrezOtpremnika(row.ItemArray[4].ToString());
                klada.SetDatumOtpremanja(row.ItemArray[5].ToString());
                klada.SetVrijemeOtpremanja(row.ItemArray[6].ToString());
                klada.SetOIBposlovode(row.ItemArray[7].ToString());
                klada.SetDatumUpisa(row.ItemArray[8].ToString());
                klada.SetVrijemeUpisa(row.ItemArray[9].ToString());
            }

            query = "select broj from skladista where naziv = '" + cmbox_odaberiteSkaldisteZaPiljenjeKlada.SelectedItem.ToString() + "'";
            string brojSkladista = "";

            dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                brojSkladista = row.ItemArray[0].ToString();
            }

            query = "delete from klade where brojPlocice = '"+tb_brojPlociceZaPiljenje.Text+"'";
            query += "\n delete from plocice where broj = '" + tb_brojPlociceZaPiljenje.Text + "'";
            query += "\n insert into plocice (broj, sumarija, mjestoUtovara, duljina, debljina, obujam, sifraDrveta)  VALUES ('" + klada.GetBrojPlocice()+"/1" + "','" + klada.GetSumarija() + "','" + klada.GetMjestoUtovara() + "','" + tb_duljina1klade.Text + "','" + tb_debljina1klade.Text + "','" + tb_obujam1klade.Text + "','" + tb_sifra1Klade.Text + "')   ";
            query += "\n insert into klade (brojPlocice, regKamiona, imePrezOtpremnika, datumOtpremanja, vrijemeOtpremanja, brojSkladista, OIBposlovode, datumUpisa, vrijemeUpisa) values ('" + klada.GetBrojPlocice() + "/1" + "','" + klada.GetRegKamiona() + "','" + klada.GetImePrezOtpremnika() + "','" + klada.GetDatumOtpremanja() + "','" + klada.GetVrijemeOtpremanja() + "','" + brojSkladista + "','" + klada.GetOIBposlovode() + "','" + klada.GetDatumUpisa() + "','" + klada.GetVrijemeUpisa() + "' )";
            query += "\n insert into plocice (broj, sumarija, mjestoUtovara, duljina, debljina, obujam, sifraDrveta)  VALUES ('" + klada.GetBrojPlocice() + "/2" + "','" + klada.GetSumarija() + "','" + klada.GetMjestoUtovara() + "','" + tb_duljina2klade.Text + "','" + tb_debljina2klade.Text + "','" + tb_obujam2klade.Text + "','" + tb_sifra2Klade.Text + "')   ";
            query += "\n insert into klade (brojPlocice, regKamiona, imePrezOtpremnika, datumOtpremanja, vrijemeOtpremanja, brojSkladista, OIBposlovode, datumUpisa, vrijemeUpisa) values ('" + klada.GetBrojPlocice() + "/2" + "','" + klada.GetRegKamiona() + "','" + klada.GetImePrezOtpremnika() + "','" + klada.GetDatumOtpremanja() + "','" + klada.GetVrijemeOtpremanja() + "','" + brojSkladista + "','" + klada.GetOIBposlovode() + "','" + klada.GetDatumUpisa() + "','" + klada.GetVrijemeUpisa() + "' )";
            izvršiUpitBezPovrata(query, 1);


            foreach (Control c in gb_podaciOPrvojPoloviciKlade.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_podaciODrugojPoloviciKlade.Controls) if (c is TextBox) c.Text = "";
            tb_brojPlociceZaPiljenje.Text = "";

        }
      
        //Prikaži radni nalog
        private void btn_IZB_PrikaziRadniNalogRadnikPrijePilane_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;
            btn_isprintajRN_SAMO_RN.Enabled = false;
        }

        


        //********** MAJSTOR NA TPT-u

        //Prikaži radni nalog
        private void btn_IZB_PrikaziRadniNalog_MajstorNaTPT_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;
            btn_isprintajRN_SAMO_RN.Enabled = false;
        }

        //Prikaži statistiku
        private void btn_IZB_PrikaziStatistiku_MajstorNaTPT_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Visible = true;
            pnl_RP_PrikaziStatistiku_MajstorNaTPT.Location = radnaPlohaPoint;

            lb_IzrezanihKubika_MajstorNaTPT.Text = "0";
            lb_IzrezanihKlada_MajstorNaTPT.Text = "0";
            lb_granice_MajstorTPT.Text = "";

            isprazniCmbox(cmbox_RazdobljeStatistike_MajstorNaTPT);
            foreach(string value in listaRazdobljeZaStat)
            {
                cmbox_RazdobljeStatistike_MajstorNaTPT.Items.Add(value);
            }

            chart_Statistika_MajstorNaTPT.Visible = false;
            lb_odaberiteRadnika_TPT.Visible = false;
            cmbox_odaberiteRadnika_statistikaTPT.Visible = false;


        }
        private List<DateTime> getDonjuIGornjuGranicuDatuma(string raspon)
        {

            DateTime donjaGranica = DateTime.Today;
            DateTime gornjaGranica = DateTime.Today;

            if (raspon == "Danas") { }
            else if(raspon == "Jučer")
            {
                donjaGranica = donjaGranica.AddDays(-1);
                gornjaGranica = gornjaGranica.AddDays(-1);
            }
            else if (raspon == "Ovaj tjedan")
            {
                string danTjedna = DateTime.Today.DayOfWeek.ToString();
                while (danTjedna != "Monday")
                {
                    donjaGranica = donjaGranica.AddDays(-1);
                    danTjedna = donjaGranica.DayOfWeek.ToString();
                }

                danTjedna = DateTime.Today.DayOfWeek.ToString();
                while (danTjedna != "Saturday")
                {

                    if (danTjedna == "Sunday")
                    {
                        gornjaGranica = gornjaGranica.AddDays(-1);
                        danTjedna = gornjaGranica.DayOfWeek.ToString();
                    }
                    else
                    {
                        gornjaGranica = gornjaGranica.AddDays(1);
                        danTjedna = gornjaGranica.DayOfWeek.ToString();
                    }


                }
            }
            else if (raspon == "Prošli tjedan")
            {
                string danTjedna = DateTime.Today.DayOfWeek.ToString();

                bool found = false;

                while (true)
                {
                    if (danTjedna == "Monday" && found == false) found = true;
                    else if (danTjedna == "Monday" && found == true) break;
                    donjaGranica = donjaGranica.AddDays(-1);
                    danTjedna = donjaGranica.DayOfWeek.ToString();
                }



                danTjedna = DateTime.Today.DayOfWeek.ToString();

                if (gornjaGranica.DayOfWeek.ToString() != "Saturday" && gornjaGranica.DayOfWeek.ToString() != "Sunday")
                {

                    while (danTjedna != "Saturday")
                    {
                        gornjaGranica = gornjaGranica.AddDays(-1);
                        danTjedna = gornjaGranica.DayOfWeek.ToString();
                    }

                }
                else
                {
                    found = false;

                    while (true)
                    {
                        if (danTjedna == "Saturday" && found == false) found = true;
                        else if (danTjedna == "Saturday" && found == true) break;
                        gornjaGranica = gornjaGranica.AddDays(-1);
                        danTjedna = gornjaGranica.DayOfWeek.ToString();

                    }

                }
            }
            else if (raspon == "Ovaj mjesec")
            {
                while (donjaGranica.Day.ToString() != "1")
                {
                    donjaGranica = donjaGranica.AddDays(-1);
                }
                int daniMjeseca = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);

                while (gornjaGranica.Day != daniMjeseca)
                {
                    gornjaGranica = gornjaGranica.AddDays(1);
                }

            }
            else if (raspon == "Prošli mjesec")
            {
                bool found = false;

                while (true)
                {
                    if (donjaGranica.Day.ToString() == "1" && found == false) found = true;
                    else if (donjaGranica.Day.ToString() == "1" && found == true) break;
                    donjaGranica = donjaGranica.AddDays(-1);
                }

                int startingDays = DateTime.Today.Day;

                do
                {
                    gornjaGranica = gornjaGranica.AddDays(-1);

                } while (startingDays > gornjaGranica.Day);

            }
            else if (raspon == "Ova godina" || raspon == "Prošla godina" || raspon == "Ukupno")
            {
                while (donjaGranica.Day.ToString() != "1" || donjaGranica.Month.ToString() != "1")
                {
                    donjaGranica = donjaGranica.AddDays(-1);
                }

                while (gornjaGranica.Day.ToString() != "31" || gornjaGranica.Month.ToString() != "12")
                {
                    gornjaGranica = gornjaGranica.AddDays(1);
                }

                if (raspon == "Prošla godina")
                {
                    donjaGranica = donjaGranica.AddYears(-1);
                    gornjaGranica = gornjaGranica.AddYears(-1);
                }
                else if(raspon == "Ukupno")
                {
                    donjaGranica = donjaGranica.AddYears(-10);
                    gornjaGranica = DateTime.Today;
                }

            }


            List<DateTime> datumi = new List<DateTime>();
            datumi.Add(donjaGranica);
            datumi.Add(gornjaGranica);
            return datumi;
            
        }     
        private void btn_prikaziStatistiku_MajstorNaTPT_Click(object sender, EventArgs e)
        {
            string raspon = cmbox_RazdobljeStatistike_MajstorNaTPT.SelectedItem.ToString();
            List<DateTime> listaDatuma = getDonjuIGornjuGranicuDatuma(raspon);

            DateTime donjaGranica = listaDatuma[0];
            DateTime gornjaGranica = listaDatuma[1];

            string oib = "";
            string query = "";

            if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" || TrenutnoPrijavljenKorisnik.GetKorIme() == "sa")
            {
               
                string ime = "", prezime = "";
                string word = cmbox_odaberiteRadnika_statistikaTPT.SelectedItem.ToString();
                bool space = false;
                if (word != "Ukupno")
                {
                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word[i] == ' ') space = true;
                        else
                        {
                            if (space == false) ime += word[i];
                            else prezime += word[i];
                        }
                    }

                    string query2 = "select OIB from zaposlenici where ime = '" + ime + "' AND prezime = '" + prezime + "' AND pozicijaRada = 'Majstor na TPT-u'";
                    DataTable dt2 = izvršiUpitSPovratomDT(query2, 0);
                    foreach (DataRow row in dt2.Rows)
                    {
                        oib = row.ItemArray[0].ToString();
                    }
                    query = "select count(k.izrezana), sum(p.obujam) from klade as k, plocice as p where k.izrezana = '1' AND  k.brojPlocice = p.broj AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "'  AND OIBmajstoraTPT = '" + oib + "' ";

                }
                else
                {
                    query = "select count(k.izrezana), sum(p.obujam) from klade as k, plocice as p where k.izrezana = '1' AND  k.brojPlocice = p.broj AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "' ";
                }


            }
            else
            {
                oib = TrenutnoPrijavljenKorisnik.GetOib();
                query = "select count(k.izrezana), sum(p.obujam) from klade as k, plocice as p where k.izrezana = '1' AND  k.brojPlocice = p.broj AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "'  AND OIBmajstoraTPT = '" + oib + "' ";

            }

            DataTable dt = izvršiUpitSPovratomDT(query, 1);
            int izrezanih = 0;
            float kubika = 0;
            foreach (DataRow row in dt.Rows)
            {
                Int32.TryParse(row.ItemArray[0].ToString(), out izrezanih);
                float.TryParse(row.ItemArray[1].ToString(), out kubika);
            }

            lb_IzrezanihKlada_MajstorNaTPT.Text = izrezanih.ToString();
            lb_IzrezanihKubika_MajstorNaTPT.Text = kubika.ToString();
            lb_granice_MajstorTPT.Text = donjaGranica.ToShortDateString() + " - "+ gornjaGranica.ToShortDateString();

            //GRAFIČKI PRIKAZ
            chart_Statistika_MajstorNaTPT.Visible = true;
            chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisY.Title = "kubika [m3]";
            chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.Gray;
            chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.Gray;
            chart_Statistika_MajstorNaTPT.Series["Kubika"].BorderWidth = 4;

            foreach (var series in chart_Statistika_MajstorNaTPT.Series)
            {
                series.Points.Clear();
            }


            if(raspon == "Danas" || raspon == "Jučer")
            {
                chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Title = "sati [h]";
                chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Interval = 2;
                float[] kubikaPoSatu = new float[24];

                for (int i = 0; i < 24; i++)
                {
                    query = "select sum(plocice.obujam) from plocice,klade where plocice.broj = klade.brojPlocice AND klade.izrezana = '1' and vrijemeRezanja like '" + i + ":%' AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "'  ";
                    if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na TPT-u" || (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" && cmbox_odaberiteRadnika_statistikaTPT.SelectedItem.ToString() != "Ukupno"))
                    {
                        query += "AND OIBmajstoraTPT = '" + oib + "'";
                    }
                    dt = izvršiUpitSPovratomDT(query, 0);
                    foreach (DataRow row in dt.Rows)
                    {
                        float.TryParse(row.ItemArray[0].ToString(), out kubikaPoSatu[i]);
                        chart_Statistika_MajstorNaTPT.Series["Kubika"].Points.AddXY(i.ToString(), kubikaPoSatu[i]);
                    }

                }

            }
            else if(raspon == "Ovaj tjedan" || raspon == "Prošli tjedan" || raspon == "Ovaj mjesec" || raspon == "Prošli mjesec")
            {
                if(raspon == "Ovaj tjedan" || raspon == "Prošli tjedan")
                {
                    chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Title = "Datum u tjednu";
                    chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Interval = 1;
                }
                else
                {
                    chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Title = "Datum u mjesecu";
                }

                float[] kubikaURazdoblju = new float[32];

                int i = 0;
                while(donjaGranica.ToShortDateString() != gornjaGranica.AddDays(1).ToShortDateString())
                {
                    query = "select sum(plocice.obujam) from plocice,klade where plocice.broj = klade.brojPlocice AND klade.izrezana = '1'  AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "' AND datumRezanja = '"+donjaGranica+"' ";
                    if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na TPT-u" || (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" && cmbox_odaberiteRadnika_statistikaTPT.SelectedItem.ToString() != "Ukupno"))
                    {
                        query += "AND OIBmajstoraTPT = '" + oib + "'";
                    }
                    dt = izvršiUpitSPovratomDT(query, 0);
                    foreach(DataRow row in dt.Rows)
                    {
                        float.TryParse(row.ItemArray[0].ToString(), out kubikaURazdoblju[i]);
                        chart_Statistika_MajstorNaTPT.Series["Kubika"].Points.AddXY(donjaGranica.Day+"."+donjaGranica.Month+".", kubikaURazdoblju[i]);
                    }
                    donjaGranica = donjaGranica.AddDays(1);
                    i++;
                }


            }
            else if(raspon == "Ova godina" || raspon == "Prošla godina")
            {
                chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Title = "Mjesec";
                string mjesec = "";
                float[] kubikaURazdoblju = new float[13];
                for (int  i = 1; i < 13; i++)
                {
                    if (i < 10) mjesec = "0" + i.ToString();
                    else mjesec = i.ToString();
                    query = "select sum(plocice.obujam) from plocice,klade where plocice.broj = klade.brojPlocice AND klade.izrezana = '1' AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "' AND datumRezanja like  '%-"+mjesec+"-%'  ";
                    if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na TPT-u" || (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" && cmbox_odaberiteRadnika_statistikaTPT.SelectedItem.ToString() != "Ukupno"))
                    {
                        query += "AND OIBmajstoraTPT = '" + oib + "'";
                    }
                    dt = izvršiUpitSPovratomDT(query, 0);
                    foreach (DataRow row in dt.Rows)
                    {
                        float.TryParse(row.ItemArray[0].ToString(), out kubikaURazdoblju[i]);
                        chart_Statistika_MajstorNaTPT.Series["Kubika"].Points.AddXY(mjesec + ".", kubikaURazdoblju[i]);
                    }

                }


            }
            else if(raspon == "Ukupno")
            {
                chart_Statistika_MajstorNaTPT.ChartAreas[0].AxisX.Title = "Godine";
                float[] kubikaURazdoblju = new float[13];
                for (int i = 0; i<12; i++)
                {
                    
                    query = "select sum(plocice.obujam) from plocice,klade where plocice.broj = klade.brojPlocice AND klade.izrezana = '1' AND datumRezanja BETWEEN  '" + donjaGranica + "' AND '" + gornjaGranica + "' AND datumRezanja like  '"+donjaGranica.Year+"-%-%'  ";
                    if (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Majstor na TPT-u" || (TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane" && cmbox_odaberiteRadnika_statistikaTPT.SelectedItem.ToString() != "Ukupno"))
                    {
                        query += "AND OIBmajstoraTPT = '" + oib + "'";
                    }
                    dt = izvršiUpitSPovratomDT(query, 0);
                    foreach (DataRow row in dt.Rows)
                    {
                        float.TryParse(row.ItemArray[0].ToString(), out kubikaURazdoblju[i]);
                        chart_Statistika_MajstorNaTPT.Series["Kubika"].Points.AddXY(donjaGranica.Year.ToString()+".", kubikaURazdoblju[i]);
                    }
                    donjaGranica = donjaGranica.AddYears(1);

                }

            }

        }



        //********* RADNIK NA VILIČARU

        //Prikaži radni nalog
        private void btn_IZB_PrikaziRadniNalog_RadnikNaViličaru_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;
            btn_isprintajRN_SAMO_RN.Enabled = false;
        }

        //Upiši paket u sušaru
        private void btn_IZB_upisiPaketUSusaru_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisiPaketUSkladiste.Visible = true;
            pnl_RP_upisiPaketUSkladiste.Location = radnaPlohaPoint;


            isprazniCmbox(cmbox_klasaDrveta_paket);
            isprazniCmbox(cmbox_vrstaDrveta_paket);
            isprazniCmbox(cmbox_odaberiteSkladiste_paket);

            foreach (string value in listaVrstaDrveta) cmbox_vrstaDrveta_paket.Items.Add(value);
            foreach (string value in listaKlasaDrveta) cmbox_klasaDrveta_paket.Items.Add(value);

            string query = "SELECT naziv FROM skladista where broj like '3%'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows) cmbox_odaberiteSkladiste_paket.Items.Add(row.ItemArray[0].ToString());


            btn_IsprintajMarkicuPaketa.Enabled = false;

            rtb_markicaPaketa_susara.Text = "";
            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is ComboBox) c.Text = "";
        }
        private void btn_spremiPaketUSkladiste_Click(object sender, EventArgs e)
        {
            Paket pak = new Paket();
            int rb = 0;
            string query = "select max(rb) from paketi";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                if(row.ItemArray[0].ToString() is null)
                {
                    row.ItemArray[0] = "0";
                }
                Int32.TryParse(row.ItemArray[0].ToString(), out rb);
            }          
            rb = rb + 1;
            pak.SetRb(rb);

            query = "select broj from skladista where naziv = '" + cmbox_odaberiteSkladiste_paket.SelectedItem.ToString() + "'";
            dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows) pak.SetBrojSKladista(row.ItemArray[0].ToString()); 
            

            pak.SetSirinaProizvoda(Convert.ToInt32(tb_sirinaDaske.Text));
            pak.SetVisinaProizvoda(Convert.ToInt32(tb_debljinaDaske.Text));
            pak.SetDuljinaProizvoda(Convert.ToInt32(tb_duljinaDaske.Text));
            pak.SetKolicinaProizvoda(Convert.ToInt32(tb_količinaDasaka.Text));
            pak.SetTezina(Math.Round(float.Parse(tb_težinaPaketa.Text), 2));

            pak.SetVrstaDrveta(cmbox_vrstaDrveta_paket.SelectedItem.ToString());
            pak.SetKlasaDrveta(cmbox_klasaDrveta_paket.SelectedItem.ToString());
            //O = v*d*š
            float num = pak.GetVisinaProizvoda() *0.01f* pak.GetDuljinaProizvoda() * 0.01f * pak.GetSirinaProizvoda() * 0.01f * pak.GetKolicinaProizvoda();
            pak.SetObujam(Math.Round(num, 2));
            pak.SetOibRadnika(TrenutnoPrijavljenKorisnik.GetOib());

            if(TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Radnik na vilicaru")
            {
                pak.SetStanjeDrveta("SIROVO");
                query = "insert into paketi (obujam,duljinaProizvoda, sirinaProizvoda, visinaProizvoda, kolicinaProizvoda, tezina, vrstaDrveta, klasaDrveta, stanjeDrveta, brojSkladista, OIBRadnika, datumUpisaSusara, vrijemeUpisaSusara) VALUES('" + pak.GetObujam() + "','" + pak.GetDuljinaProizvoda() + "','" + pak.GetSirinaProizvoda() + "','" + pak.GetVisinaProizvoda() + "','" + pak.GetKolicinaProizvoda() + "','" + pak.GetTezina() + "','" + pak.GetVrstaDrveta() + "','" + pak.GetKlasaDrveta() + "','" + pak.GetStanjeDrveta() + "','" + pak.GetBrojSKladista() + "','" + pak.GetOibRadnika() + "','" + getDanašnjiDatum() + "','" + getSadašnjeVrijeme() + "' )   ";
                izvršiUpitBezPovrata(query, 1);
            }
            else
            {
                pak.SetStanjeDrveta("OBRADENO");
                query = "insert into paketi (obujam,duljinaProizvoda, sirinaProizvoda, visinaProizvoda, kolicinaProizvoda, tezina, vrstaDrveta, klasaDrveta, stanjeDrveta, brojSkladista, OIBRadnika, datumUpisaSklGotoveRobe, vrijemeUpisaSklGotoveRobe) VALUES('" + pak.GetObujam() + "','" + pak.GetDuljinaProizvoda() + "','" + pak.GetSirinaProizvoda() + "','" + pak.GetVisinaProizvoda() + "','" + pak.GetKolicinaProizvoda() + "','" + pak.GetTezina() + "','" + pak.GetVrstaDrveta() + "','" + pak.GetKlasaDrveta() + "','" + pak.GetStanjeDrveta() + "','" + pak.GetBrojSKladista() + "','" + pak.GetOibRadnika() + "','" + getDanašnjiDatum() + "','" + getSadašnjeVrijeme() + "' )   ";
                izvršiUpitBezPovrata(query, 1);
            }


      
            rtb_markicaPaketa_susara.Text = "BROJ: "+pak.GetRb().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += pak.GetObujam().ToString()+" m3" + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += pak.GetVrstaDrveta().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += pak.GetKlasaDrveta().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += pak.GetStanjeDrveta().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += "DIM: "+pak.GetDuljinaProizvoda().ToString()+"x"+pak.GetSirinaProizvoda().ToString()+"x"+pak.GetVisinaProizvoda().ToString()+ Environment.NewLine;
            rtb_markicaPaketa_susara.Text += "KOL: "+pak.GetKolicinaProizvoda().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += "SKL:"+pak.GetBrojSKladista().ToString() + Environment.NewLine;
            rtb_markicaPaketa_susara.Text += getDanašnjiDatum() + Environment.NewLine;

            btn_IsprintajMarkicuPaketa.Enabled = true;

            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is ComboBox) c.Text = "";

        }
        private void btn_IsprintajMarkicuPaketa_Click(object sender, EventArgs e)
        {
            if (printDialog_MarkicaPaketa_Susara.ShowDialog() == DialogResult.OK)
            {
                printDocument_MarkicaPaketa_Susara.Print();
            }
            rtb_markicaPaketa_susara.Text = "";
        }

      


        //******** POSLOVODA ELEMENTARE

        //Napiši radni nalog
        private void btn_IZB_NapisiRadniNalog_Elementara_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_napisiRadniNalog.Visible = true;
            pnl_RP_napisiRadniNalog.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_nalogPozicijaRada);
            isprazniCmbox(cmbox_nalogRadnik);
            tb_RadniNalogHead.Text = "";
            tb_radniNalogTekst.Text = "";

            if(TrenutnoPrijavljenKorisnik.GetPozicijaRada() == "Poslovoda pilane")
            {
                foreach (string val in listaPozRadaPoslovodaPilane) cmbox_nalogPozicijaRada.Items.Add(val);
            }
            else
            {
                foreach (string val in listaPozRadaPoslovodaElementare) cmbox_nalogPozicijaRada.Items.Add(val);
            }
            datePicker_nalogZaDatum.MinDate = DateTime.Now;
        }
     
        //Prikaži radni nalog
        private void btn_IZB_prikaziRadniNalogPoslovodaElementare_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_poslovodaPrikaziRadniNalog.Visible = true;
            pnl_RP_poslovodaPrikaziRadniNalog.Location = radnaPlohaPoint;

            btn_poslovodaIsprintajRN.Enabled = false;

            isprazniCmbox(cmbox_poslovodaRNPozicijaRada);
            isprazniCmbox(cmbox_poslovodaRNRadnik);
            isprazniCmbox(cmbox_vrijemePisanjaRN);

            rtb_RadniNalogTxt.Text = "";

            
            foreach (string val in listaPozRadaPoslovodaElementare) cmbox_poslovodaRNPozicijaRada.Items.Add(val);
            cmbox_poslovodaRNPozicijaRada.Items.Add("Poslovoda elementare");

        }

        //Napiši popratnicu
        private void btn_NapisiPopratnicu_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_napisiPopratnicu.Visible = true;
            pnl_RP_napisiPopratnicu.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_odaberiteKamion_popratnica);
            isprazniCmbox(cmbox_odaberitePrikolicu_popratnica);
            foreach (Control c in gb_podaciOPopratnici_popratnica.Controls) if (c is TextBox) c.Text = "";
            dgv_brojPaketa_popratnica.Rows.Clear();
            dgv_brojPaketa_popratnica.Refresh();
            rtb_izgledPopratnice.Text = "";

            string query = "SELECT regOznakaKamiona FROM kamioni_u_tvrtki WHERE datumIzlaska IS NULL";

            DataTable dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberiteKamion_popratnica.Items.Add(row.ItemArray[0].ToString());
            }

        }
        private void cmbox_odaberiteKamion_popratnica_SelectedIndexChanged(object sender, EventArgs e)
        {

            isprazniCmbox(cmbox_odaberitePrikolicu_popratnica);

            string query = "SELECT regOznakaPrikolice FROM kamioni_u_tvrtki WHERE datumIzlaska IS NULL AND regOznakaKamiona = '"+cmbox_odaberiteKamion_popratnica.SelectedItem.ToString()+"'";

            DataTable dt = izvršiUpitSPovratomDT(query, 0);

            foreach (DataRow row in dt.Rows)
            {
                cmbox_odaberitePrikolicu_popratnica.Items.Add(row.ItemArray[0].ToString());
            }
        }
        private void btn_spremiNovuPopratnicu_Click(object sender, EventArgs e)
        {

            Popratnica pop = new Popratnica();

            int broj = 0;
            string query = "select max(broj) from popratnice";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                if (row.ItemArray[0].ToString() is null)
                {
                    row.ItemArray[0] = "0";
                }
                Int32.TryParse(row.ItemArray[0].ToString(), out broj);
            }
            broj = broj + 1;
            pop.SetBroj(broj);
            pop.SetDatumOtpremanja(getDanašnjiDatum());
            pop.SetVrijemeOtpremanja(getSadašnjeVrijeme());
            pop.SetOIBOtpremnika(TrenutnoPrijavljenKorisnik.GetOib());
            pop.SetRegOznakaKamiona(cmbox_odaberiteKamion_popratnica.SelectedItem.ToString());
            pop.SetRegOznakaPrikolice(cmbox_odaberitePrikolicu_popratnica.SelectedItem.ToString());

            query = "select imeVozaca, prezimeVozaca from kamioni_u_tvrtki where datumIzlaska is null and regOznakaKamiona = '"+pop.GetRegOznakaKamiona()+"'";
            dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                pop.SetImePrezVozaca(row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString());
            }
            pop.SetMjestoUtovara(tb_mjestoUtovara_popratnica.Text);
            pop.SetMjestoIstovara(tb_mjestoIstovara_popratnica.Text);
            pop.SetKupac(tb_kupac_popratnica.Text);
            pop.SetUplatnica_nalog(tb_uplatnicaNalog_popratnica.Text);


            bool err = false;

            for (int i = 0; i < dgv_brojPaketa_popratnica.Rows.Count - 1; i++)
            {
                query = "select count(rb) from paketi where rb = '" + dgv_brojPaketa_popratnica.Rows[i].Cells[0].Value.ToString() + "' and brojPopratnice is null and stanjeDrveta = 'OBRADENO'";
                dt = izvršiUpitSPovratomDT(query, 0);
                foreach(DataRow row in dt.Rows)
                {
                    if (row.ItemArray[0].ToString() == "0") err = true;
                }
            }

            if(err == true)
            {
                MessageBox.Show("Unesli ste krive brojeve paketa", "Pogreška");
            }
            else
            {

                query = "INSERT INTO popratnice (datumOtpremanja, vrijemeOtpremanja, OIBOtpremnika, RegOznakaKamiona, RegOznakaPrikolice, ImePrezVozaca, MjestoUtovara, MjestoIstovara, Kupac, Uplatnica_nalog) VALUES('" + pop.GetDatumOtpremanja() + "','" + pop.GetVrijemeOtpremanja() + "','" + pop.GetOIBOtpremnika() + "','" + pop.GetRegOznakaKamiona() + "','" + pop.GetRegOznakaPrikolice() + "','" + pop.GetImePrezVozaca() + "','" + pop.GetMjestoUtovara() + "','" + pop.GetMjestoIstovara() + "','" + pop.GetKupac() + "','" + pop.GetUplatnica_nalog() + "')    ";
                izvršiUpitBezPovrata(query, 1);

                for (int i = 0; i < dgv_brojPaketa_popratnica.Rows.Count - 1; i++)
                {
                    query = "UPDATE paketi set brojPopratnice = '" + pop.GetBroj() + "' WHERE rb = '" + dgv_brojPaketa_popratnica.Rows[i].Cells[0].Value.ToString() + "'";
                    izvršiUpitBezPovrata(query, 1);
                }

                rtb_izgledPopratnice.Text = "POPRATNICA ZA OBRAĐENO DRVO" + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "DATUM: " + pop.GetDatumOtpremanja() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "VRIJEME: " + pop.GetVrijemeOtpremanja() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "" + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "MJESTO UTOVARA: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetMjestoUtovara() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "MJESTO ISTOVARA: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetMjestoIstovara() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "KUPAC: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetKupac() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "REGISTARSKA OZNAKA: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetRegOznakaKamiona() + " / " + pop.GetRegOznakaPrikolice() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "VOZAČ: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetImePrezVozaca() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "UPLATNICA/NALOG: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "   " + pop.GetUplatnica_nalog() + Environment.NewLine;
                rtb_izgledPopratnice.Text += "_____________________________________________" + Environment.NewLine;
                rtb_izgledPopratnice.Text += "===========================================" + Environment.NewLine;
                rtb_izgledPopratnice.Text += "BR.    DIM.    OBUJAM  KOL.   TEZINA  VRSTA" + Environment.NewLine;

                int kom = 0, kol = 0;
                double obujam = 0, tezina = 0;

                for (int i = 0; i < dgv_brojPaketa_popratnica.Rows.Count - 1; i++)
                {
                    Paket pak = new Paket();

                    query = "SELECT rb, obujam, duljinaProizvoda, visinaProizvoda, sirinaProizvoda, kolicinaProizvoda, tezina, vrstaDrveta from paketi where rb = '" + dgv_brojPaketa_popratnica.Rows[i].Cells[0].Value.ToString() + "'";
                    dt = izvršiUpitSPovratomDT(query, 0);
                    foreach (DataRow row in dt.Rows)
                    {
                        pak.SetRb(Convert.ToInt32(row.ItemArray[0].ToString()));
                        pak.SetObujam(Math.Round(float.Parse(row.ItemArray[1].ToString()), 2));
                        pak.SetDuljinaProizvoda(Convert.ToInt32(row.ItemArray[2].ToString()));
                        pak.SetVisinaProizvoda(Convert.ToInt32(row.ItemArray[3].ToString()));
                        pak.SetSirinaProizvoda(Convert.ToInt32(row.ItemArray[4].ToString()));
                        pak.SetKolicinaProizvoda(Convert.ToInt32(row.ItemArray[5].ToString()));
                        pak.SetTezina(Math.Round(float.Parse(row.ItemArray[6].ToString()), 2));
                        pak.SetVrstaDrveta(row.ItemArray[7].ToString());
                    }
                    kom++;
                    obujam += pak.GetObujam();
                    kol += pak.GetKolicinaProizvoda();
                    tezina += pak.GetTezina();

                    rtb_izgledPopratnice.Text += pak.GetRb() + "     " + pak.GetDuljinaProizvoda() + "x" + pak.GetSirinaProizvoda() + "x" + pak.GetVisinaProizvoda() + "     " + pak.GetObujam() + "     " + pak.GetKolicinaProizvoda() + "      " + pak.GetTezina() + "     " + pak.GetVrstaDrveta() + Environment.NewLine;
                }
                obujam = Math.Round(obujam, 2);
                tezina = Math.Round(tezina, 2);

                rtb_izgledPopratnice.Text += "===========================================" + Environment.NewLine;
                rtb_izgledPopratnice.Text += "REKAPITULACIJA: " + Environment.NewLine;
                rtb_izgledPopratnice.Text += "KOM.  OBUJAM     KOL.    TEZINA" + Environment.NewLine;
                rtb_izgledPopratnice.Text += "===========================================" + Environment.NewLine;
                rtb_izgledPopratnice.Text += kom + "\t" + obujam + "\t" + kol + "\t" + tezina + Environment.NewLine;
                rtb_izgledPopratnice.Text += "===========================================" + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "ŽIG/FAKSIMIL: " + Environment.NewLine;
                for (int i = 0; i < 6; i++) rtb_izgledPopratnice.Text += Environment.NewLine;
                rtb_izgledPopratnice.Text += "POTPIS OTPREMNIKA: " + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "___________________________________________" + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "PREVOZI: " + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "___________________________________________" + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "PRIMIO: " + Environment.NewLine + Environment.NewLine;
                rtb_izgledPopratnice.Text += "___________________________________________" + Environment.NewLine + Environment.NewLine;

                query = "update popratnice set tekst = '" + rtb_izgledPopratnice.Text + "' where broj = '" + pop.GetBroj() + "'";
                izvršiUpitBezPovrata(query, 0);


            }
           
        }
        private void btn_ispisiPopratnicu_Click(object sender, EventArgs e)
        {
            if(printDialog_Popratnica.ShowDialog() == DialogResult.OK)
            {
                printDocument_popratnica.Print();
            }
        }

        //Pretraži skladište
        private void btn_IZB_pretraziSkladiste_elem_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_pretraziSkladiste.Visible = true;
            pnl_RP_pretraziSkladiste.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_PertraziPo_pp);
            isprazniCmbox(cmbox_prog_skla_spec);
            tb_prog_nazivi.Text = "";
            cmbox_pretraziPo.Text = "";
            cmbox_prog_skla_spec.Text = "";
            dgv_prog_rezultati.DataSource = null;

            foreach (string val in listaPretraziSkladisteElem) cmbox_PertraziPo_pp.Items.Add(val);
        }

        //Pretraži popratnice
        private void btn_IZB_prikaziPopratnice_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_pretraziPopratnice.Visible = true;
            pnl_RP_pretraziPopratnice.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_prikaziPopratnicu_datumUpisa);
            rtb_prikaziPopratnice.Text = "";

            string query = "select broj, RegOznakaKamiona, RegOznakaPrikolice, Kupac from popratnice where datumOtpremanja = '" + datePicker_prikaziPopratnicu.Value.ToShortDateString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows)
            {
                cmbox_prikaziPopratnicu_datumUpisa.Items.Add("Br " + row.ItemArray[0].ToString() + ", " + row.ItemArray[1].ToString() + " / " + row.ItemArray[2].ToString() + " - " + row.ItemArray[3].ToString());
            }
        }




        //******** RADNIK NA VIŠELISNOM REZAČU

        //Upiši paket
        private void btn_IZB_upisiPaket_Elementara_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisiPaket_RadnikNaViselisnom.Visible = true;
            pnl_RP_upisiPaket_RadnikNaViselisnom.Location = radnaPlohaPoint;

            isprazniCmbox(cmbox_odaberiteSkladiste_viselisni);

            string query = "SELECT naziv FROM skladista where broj like '4%'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows) cmbox_odaberiteSkladiste_viselisni.Items.Add(row.ItemArray[0].ToString());

        }
        private void btn_upisiPaket_viselisni_Click(object sender, EventArgs e)
        {
            string skl = "";
            string query = "select broj from skladista where naziv = '" + cmbox_odaberiteSkladiste_viselisni.SelectedItem.ToString() + "'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows) skl = row.ItemArray[0].ToString();

            query = "UPDATE paketi SET brojSkladista = '" + skl + "', stanjeDrveta = 'OSUSENO', datumUpisaElementara = '"+getDanašnjiDatum()+"', vrijemeUpisaElementara = '"+getSadašnjeVrijeme()+"'  WHERE rb = '"+tb_brojPaketa_viselisni.Text+"'";
            izvršiUpitBezPovrata(query, 1);

            tb_brojPaketa_viselisni.Text = "";
            cmbox_odaberiteSkladiste_viselisni.Text = "";
        }

        //Prikaži radni nalog
        private void btn_IZB_prikaziRadniNalog_Viselisni_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;
        }

      

        //******** RADNIK S GOTOVOM ROBOM

        //Upiši paket
        private void btn_IZB_upisiPaket_GotovaRoba_Click(object sender, EventArgs e)
        {
            sakrijRadnePlohe();

            pnl_RP_upisiPaketUSkladiste.Visible = true;
            pnl_RP_upisiPaketUSkladiste.Location = radnaPlohaPoint;


            isprazniCmbox(cmbox_klasaDrveta_paket);
            isprazniCmbox(cmbox_vrstaDrveta_paket);
            isprazniCmbox(cmbox_odaberiteSkladiste_paket);

            foreach (string value in listaVrstaDrveta) cmbox_vrstaDrveta_paket.Items.Add(value);
            foreach (string value in listaKlasaDrveta) cmbox_klasaDrveta_paket.Items.Add(value);

            string query = "SELECT naziv FROM skladista where broj like '5%'";
            DataTable dt = izvršiUpitSPovratomDT(query, 0);
            foreach (DataRow row in dt.Rows) cmbox_odaberiteSkladiste_paket.Items.Add(row.ItemArray[0].ToString());


            btn_IsprintajMarkicuPaketa.Enabled = false;

            rtb_markicaPaketa_susara.Text = "";
            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is TextBox) c.Text = "";
            foreach (Control c in gb_podaciOPaketu_susara.Controls) if (c is ComboBox) c.Text = "";

        }   

        //Prikaži radni nalog
        private void btn_IZB_PrikaziRadniNalog_GotovaRoba_Click(object sender, EventArgs e)
        {
            pnl_RP_upisiPaketUSkladiste.Visible = false;

            rtb_txtRN_SAMO_RN.Text = "";
            cmbox_vrijemePisanjaRN_SAMO_RN.Text = "";
            isprazniCmbox(cmbox_vrijemePisanjaRN_SAMO_RN);

            pnl_RP_PrikaziRadniNalogSAMO_RN.Visible = true;
            pnl_RP_PrikaziRadniNalogSAMO_RN.Location = radnaPlohaPoint;
        }

        
    }

    public class Zaposlenik
    {
        private string ime;
        private string prezime;
        private string oib;
        private string adresa;
        private string tel;
        private string mob;
        private string email;
        private string spol;
        private string pozicijaRada;
        private string korIme;
        private string lozinka;



        public Zaposlenik(string _ime, string _prezime, string _oib, string _adresa, string _tel, string _mob, string _email, string _spol, string _pozicijaRada, string _korIme, string _lozinka)
        {
            SetIme(_ime);
            SetPrezime(_prezime);
            SetOib(_oib);
            SetAdresa(_adresa);
            SetTel(_tel);
            SetMob(_mob);
            SetEmail(_email);
            SetSpol(_spol);
            SetPozicijaRada(_pozicijaRada);
            SetKorIme(_korIme);
            SetLozinka(_lozinka);
        }
        public Zaposlenik()
        {
            SetIme("");
            SetPrezime("");
            SetOib("");
            SetAdresa("");
            SetTel("");
            SetMob("");
            SetEmail("");
            SetSpol("");
            SetPozicijaRada("");
            SetKorIme("");
            SetLozinka("");
        }

        ~Zaposlenik() { }


        public string GetIme()
        {
            return ime;
        }
        public void SetIme(string value)
        {
            ime = value;
        }

        public string GetPrezime()
        {
            return prezime;
        }
        public void SetPrezime(string value)
        {
            prezime = value;
        }

        public string GetOib()
        {
            return oib;
        }
        public void SetOib(string value)
        {
            oib = value;
        }

        public string GetAdresa()
        {
            return adresa;
        }
        public void SetAdresa(string value)
        {
            adresa = value;
        }

        public string GetTel()
        {
            return tel;
        }
        public void SetTel(string value)
        {
            tel = value;
        }

        public string GetMob()
        {
            return mob;
        }
        public void SetMob(string value)
        {
            mob = value;
        }

        public string GetEmail()
        {
            return email;
        }
        public void SetEmail(string value)
        {
            email = value;
        }

        public string GetSpol()
        {
            return spol;
        }
        public void SetSpol(string value)
        {
            spol = value;
        }

        public string GetPozicijaRada()
        {
            return pozicijaRada;
        }
        public void SetPozicijaRada(string value)
        {
            pozicijaRada = value;
        }

        public string GetKorIme()
        {
            return korIme;
        }
        public void SetKorIme(string value)
        {
            korIme = value;
        }

        public string GetLozinka()
        {
            return lozinka;
        }
        public void SetLozinka(string value)
        {
            lozinka = value;
        }


    }

    public class MojServer
    {
        private string server;
        private string server2;
        private string bazaPod;
        private string user;
        private string password;
        private string conString;
        private string port;
        private bool local;

        public string GetServer2()
        {
            return server2;
        }

        public void SetServer2(string value)
        {
            server2 = value;
        }

        public bool GetLocal()
        {
            return local;
        }

        public void SetLocal(bool value)
        {
            local = value;
        }

        public string GetPort()
        {
            return port;
        }

        public void SetPort(string value)
        {
            port = value;
        }

        public string GetServer()
        {
            return server;
        }
        public void SetServer(string value)
        {
            server = value;
        }

        public string GetBazaPod()
        {
            return bazaPod;
        }
        public void SetBazaPod(string value)
        {
            bazaPod = value;
        }

        public string GetConString()
        {
            return conString;
        }
        public void SetConString(string value)
        {
            conString = value;
        }

        public string GetPassword()
        {
            return password;
        }
        public void SetPassword(string value)
        {
            password = value;
        }

        public string GetUser()
        {
            return user;
        }
        public void SetUser(string value)
        {
            user = value;
        }

        public MojServer(string server, string bazaPod, string user, string password, string conString, string port)
        {
            this.SetServer(server);
            this.SetBazaPod(bazaPod);
            this.SetUser(user);
            this.SetPassword(password);
            this.SetConString(conString);
            this.SetPort(port);
        }

        public MojServer(string server, string server2, string bazaPod, string user, string password, string conString, string port, bool local) : this(server, server2, bazaPod, user, password, conString)
        {
            this.port = port;
            this.local = local;
        }

        
        public MojServer()
        {
        }
    }

    public class Kamion
    {
        private string regOznakaKamiona;
        private string regOznakaPrikolice;
        private string imeVozača;
        private string prezimeVozača;
        private string OIBVozača;
        private string datumUlaska;
        private string datumIzlaska;
        private string vrijemeUlaska;
        private string vrijemeIzlaska;
        private string OIBPortira;

        public string GetregOznakaKamiona()
        {
            return regOznakaKamiona;
        }
        public void SetregOznakaKamiona(string value)
        {
            regOznakaKamiona = value;
        }

        public string GetregOznakaPrikolice()
        {
            return regOznakaPrikolice;
        }
        public void SetregOznakaPrikolice(string value)
        {
            regOznakaPrikolice = value;
        }

        public string GetimeVozača()
        {
            return imeVozača;
        }
        public void SetimeVozača(string value)
        {
            imeVozača = value;
        }

        public string GetPrezimeVozača()
        {
            return prezimeVozača;
        }
        public void SetPrezimeVozača(string value)
        {
            prezimeVozača = value;
        }

        public string GetOIBVozača()
        {
            return OIBVozača;
        }
        public void SetOIBVozača(string value)
        {
            OIBVozača = value;
        }

        public string GetDatumUlaska()
        {
            return datumUlaska;
        }
        public void SetDatumUlaska(string value)
        {
            datumUlaska = value;
        }

        public string GetDatumIzlaska()
        {
            return datumIzlaska;
        }
        public void SetDatumIzlaska(string value)
        {
            datumIzlaska = value;
        }

        public string GetVrijemeUlaska()
        {
            return vrijemeUlaska;
        }
        public void SetVrijemeUlaska(string value)
        {
            vrijemeUlaska = value;
        }

        public string GetVrijemeIzlaska()
        {
            return vrijemeIzlaska;
        }
        public void SetVrijemeIzlaska(string value)
        {
            vrijemeIzlaska = value;
        }

        public string GetOIBPortira()
        {
            return OIBPortira;
        }
        public void SetOIBPortira(string value)
        {
            OIBPortira = value;
        }


        public Kamion(string regOznakaKamiona, string regOznakaPrikolice, string imeVozača, string prezimeVozača, string oIBVozača, string datumUlaska, string datumIzlaska, string vrijemeUlaska, string vrijemeIzlaska, string oIBPortira)
        {
            this.SetregOznakaKamiona(regOznakaKamiona);
            this.SetregOznakaPrikolice(regOznakaPrikolice);
            this.SetimeVozača(imeVozača);
            this.SetPrezimeVozača(prezimeVozača);
            SetOIBVozača(oIBVozača);
            this.SetDatumUlaska(datumUlaska);
            this.SetDatumIzlaska(datumIzlaska);
            this.SetVrijemeUlaska(vrijemeUlaska);
            this.SetVrijemeIzlaska(vrijemeIzlaska);
            SetOIBPortira(oIBPortira);
        }

        public Kamion()
        {
        }
    }

    public class Upiti
    {
        private int neKorisnikoviUkupniUpiti;
        private int neKorisnikoviIzvršeniUpiti;
        private int korisnikoviUkupniUpiti;
        private int korisnikoviIzvršeniUpiti;

        public void povecajKorisnikoveUkupneUpite() { korisnikoviUkupniUpiti++; }
        public void povecajNeKorisnikoveUkupneUpite() { neKorisnikoviUkupniUpiti++; }
        public void povecajKorisnikoveIzvršeneUpite() { korisnikoviIzvršeniUpiti++; }
        public void povecajNeKorisnikoveIzvršeneUpite() { neKorisnikoviIzvršeniUpiti++; }
        public void resetirajUpite()
        {
            neKorisnikoviUkupniUpiti = 0;
            neKorisnikoviIzvršeniUpiti = 0;
            korisnikoviUkupniUpiti = 0;
            korisnikoviIzvršeniUpiti = 0;
        }

        public int GetNeKorisnikoviUkupniUpiti()
        {
            return neKorisnikoviUkupniUpiti;
        }
        public void SetNeKorisnikoviUkupniUpiti(int value)
        {
            neKorisnikoviUkupniUpiti = value;
        }

        public int GetNeKorisnikoviIzvršeniUpiti()
        {
            return neKorisnikoviIzvršeniUpiti;
        }
        public void SetNeKorisnikoviIzvršeniUpiti(int value)
        {
            neKorisnikoviIzvršeniUpiti = value;
        }

        public int GetKorisnikoviUkupniUpiti()
        {
            return korisnikoviUkupniUpiti;
        }
        public void SetKorisnikoviUkupniUpiti(int value)
        {
            korisnikoviUkupniUpiti = value;
        }

        public int GetKorisnikoviIzvršeniUpiti()
        {
            return korisnikoviIzvršeniUpiti;
        }
        public void SetKorisnikoviIzvršeniUpiti(int value)
        {
            korisnikoviIzvršeniUpiti = value;
        }

        public Upiti(int neKorisnikoviUkupniUpiti, int neKorisnikoviIzvršeniUpiti, int korisnikoviUkupniUpiti, int korisnikoviIzvršeniUpiti)
        {
            this.neKorisnikoviUkupniUpiti = neKorisnikoviUkupniUpiti;
            this.neKorisnikoviIzvršeniUpiti = neKorisnikoviIzvršeniUpiti;
            this.korisnikoviUkupniUpiti = korisnikoviUkupniUpiti;
            this.korisnikoviIzvršeniUpiti = korisnikoviIzvršeniUpiti;
        }

        public Upiti()
        {
        }
    }

    public class Klade
    {
        private string brojPlocice;

        public string GetBrojPlocice()
        {
            return brojPlocice;
        }

        public void SetBrojPlocice(string value)
        {
            brojPlocice = value;
        }

        private string sumarija;

        public string GetSumarija()
        {
            return sumarija;
        }

        public void SetSumarija(string value)
        {
            sumarija = value;
        }

        private string mjestoUtovara;

        public string GetMjestoUtovara()
        {
            return mjestoUtovara;
        }

        public void SetMjestoUtovara(string value)
        {
            mjestoUtovara = value;
        }

        private string regKamiona;

        public string GetRegKamiona()
        {
            return regKamiona;
        }

        public void SetRegKamiona(string value)
        {
            regKamiona = value;
        }

        private string imePrezOtpremnika;

        public string GetImePrezOtpremnika()
        {
            return imePrezOtpremnika;
        }

        public void SetImePrezOtpremnika(string value)
        {
            imePrezOtpremnika = value;
        }

        private string datumOtpremanja;

        public string GetDatumOtpremanja()
        {
            return datumOtpremanja;
        }

        public void SetDatumOtpremanja(string value)
        {
            datumOtpremanja = value;
        }

        private string vrijemeOtpremanja;

        public string GetVrijemeOtpremanja()
        {
            return vrijemeOtpremanja;
        }

        public void SetVrijemeOtpremanja(string value)
        {
            vrijemeOtpremanja = value;
        }

        private string oIBposlovode;

        public string GetOIBposlovode()
        {
            return oIBposlovode;
        }

        public void SetOIBposlovode(string value)
        {
            oIBposlovode = value;
        }

        private string datumUpisa;

        public string GetDatumUpisa()
        {
            return datumUpisa;
        }

        public void SetDatumUpisa(string value)
        {
            datumUpisa = value;
        }

        private string vrijemeUpisa;

        public string GetVrijemeUpisa()
        {
            return vrijemeUpisa;
        }

        public void SetVrijemeUpisa(string value)
        {
            vrijemeUpisa = value;
        }

        public Klade(string brojPlocice, string sumarija, string mjestoUtovara, string regKamiona, string imePrezOtpremnika, string datumOtpremanja, string vrijemeOtpremanja, string oIBposlovode, string datumUpisa, string vrijemeUpisa)
        {
            this.brojPlocice = brojPlocice;
            this.sumarija = sumarija;
            this.mjestoUtovara = mjestoUtovara;
            this.regKamiona = regKamiona;
            this.imePrezOtpremnika = imePrezOtpremnika;
            this.datumOtpremanja = datumOtpremanja;
            this.vrijemeOtpremanja = vrijemeOtpremanja;
            this.oIBposlovode = oIBposlovode;
            this.datumUpisa = datumUpisa;
            this.vrijemeUpisa = vrijemeUpisa;
        }

        public Klade()
        {
        }
    }

    public class Paket
    {
        private int rb;
        private double obujam;
        private int duljinaProizvoda;
        private int sirinaProizvoda;
        private int visinaProizvoda;
        private int kolicinaProizvoda;
        private double tezina;
        private string vrstaDrveta;
        private string klasaDrveta;
        private string stanjeDrveta;
        private string brojSKladista;
        private string oibRadnika;
        private string datumUpisa;
        private string vrijemeUpisa;
        private string datumIspisa;
        private string vrijemeIspisa;

        public int GetRb()
        {
            return rb;
        }

        public void SetRb(int value)
        {
            rb = value;
        }

        public double GetObujam()
        {
            return obujam;
        }

        public void SetObujam(double value)
        {
            obujam = value;
        }

        public int GetDuljinaProizvoda()
        {
            return duljinaProizvoda;
        }

        public void SetDuljinaProizvoda(int value)
        {
            duljinaProizvoda = value;
        }

        public int GetSirinaProizvoda()
        {
            return sirinaProizvoda;
        }

        public void SetSirinaProizvoda(int value)
        {
            sirinaProizvoda = value;
        }

        public int GetVisinaProizvoda()
        {
            return visinaProizvoda;
        }

        public void SetVisinaProizvoda(int value)
        {
            visinaProizvoda = value;
        }

        public int GetKolicinaProizvoda()
        {
            return kolicinaProizvoda;
        }

        public void SetKolicinaProizvoda(int value)
        {
            kolicinaProizvoda = value;
        }

        public double GetTezina()
        {
            return tezina;
        }

        public void SetTezina(double value)
        {
            tezina = value;
        }

        public string GetVrstaDrveta()
        {
            return vrstaDrveta;
        }

        public void SetVrstaDrveta(string value)
        {
            vrstaDrveta = value;
        }

        public string GetKlasaDrveta()
        {
            return klasaDrveta;
        }

        public void SetKlasaDrveta(string value)
        {
            klasaDrveta = value;
        }

        public string GetStanjeDrveta()
        {
            return stanjeDrveta;
        }

        public void SetStanjeDrveta(string value)
        {
            stanjeDrveta = value;
        }

        public string GetBrojSKladista()
        {
            return brojSKladista;
        }

        public void SetBrojSKladista(string value)
        {
            brojSKladista = value;
        }

        public string GetOibRadnika()
        {
            return oibRadnika;
        }

        public void SetOibRadnika(string value)
        {
            oibRadnika = value;
        }

        public string GetDatumUpisa()
        {
            return datumUpisa;
        }

        public void SetDatumUpisa(string value)
        {
            datumUpisa = value;
        }

        public string GetVrijemeUpisa()
        {
            return vrijemeUpisa;
        }

        public void SetVrijemeUpisa(string value)
        {
            vrijemeUpisa = value;
        }

        public string GetDatumIspisa()
        {
            return datumIspisa;
        }

        public void SetDatumIspisa(string value)
        {
            datumIspisa = value;
        }

        public string GetVrijemeIspisa()
        {
            return vrijemeIspisa;
        }

        public void SetVrijemeIspisa(string value)
        {
            vrijemeIspisa = value;
        }

        public Paket(int rb, double obujam, int duljinaProizvoda, int sirinaProizvoda, int visinaProizvoda, int kolicinaProizvoda, float tezina, string vrstaDrveta, string klasaDrveta, string stanjeDrveta, string brojSKladista, string oibRadnika, string datumUpisa, string vrijemeUpisa, string datumIspisa, string vrijemeIspisa)
        {
            this.rb = rb;
            this.obujam = obujam;
            this.duljinaProizvoda = duljinaProizvoda;
            this.sirinaProizvoda = sirinaProizvoda;
            this.visinaProizvoda = visinaProizvoda;
            this.kolicinaProizvoda = kolicinaProizvoda;
            this.tezina = tezina;
            this.vrstaDrveta = vrstaDrveta;
            this.klasaDrveta = klasaDrveta;
            this.stanjeDrveta = stanjeDrveta;
            this.brojSKladista = brojSKladista;
            this.oibRadnika = oibRadnika;
            this.datumUpisa = datumUpisa;
            this.vrijemeUpisa = vrijemeUpisa;
            this.datumIspisa = datumIspisa;
            this.vrijemeIspisa = vrijemeIspisa;
        }

        public Paket()
        {
        }
    }

    public class Popratnica
    {

        private int broj;
        private string datumOtpremanja;
        private string vrijemeOtpremanja;
        private string OIBOtpremnika;
        private string regOznakaKamiona;
        private string regOznakaPrikolice;
        private string ImePrezVozaca;
        private string MjestoUtovara;
        private string MjestoIstovara;
        private string Kupac;
        private string Uplatnica_nalog;

        public int GetBroj()
        {
            return broj;
        }

        public void SetBroj(int value)
        {
            broj = value;
        }

        public string GetDatumOtpremanja()
        {
            return datumOtpremanja;
        }

        public void SetDatumOtpremanja(string value)
        {
            datumOtpremanja = value;
        }

        public string GetVrijemeOtpremanja()
        {
            return vrijemeOtpremanja;
        }

        public void SetVrijemeOtpremanja(string value)
        {
            vrijemeOtpremanja = value;
        }

        public string GetOIBOtpremnika()
        {
            return OIBOtpremnika;
        }

        public void SetOIBOtpremnika(string value)
        {
            OIBOtpremnika = value;
        }

        public string GetRegOznakaKamiona()
        {
            return regOznakaKamiona;
        }

        public void SetRegOznakaKamiona(string value)
        {
            regOznakaKamiona = value;
        }

        public string GetRegOznakaPrikolice()
        {
            return regOznakaPrikolice;
        }

        public void SetRegOznakaPrikolice(string value)
        {
            regOznakaPrikolice = value;
        }

        public string GetImePrezVozaca()
        {
            return ImePrezVozaca;
        }

        public void SetImePrezVozaca(string value)
        {
            ImePrezVozaca = value;
        }

        public string GetMjestoUtovara()
        {
            return MjestoUtovara;
        }

        public void SetMjestoUtovara(string value)
        {
            MjestoUtovara = value;
        }

        public string GetMjestoIstovara()
        {
            return MjestoIstovara;
        }

        public void SetMjestoIstovara(string value)
        {
            MjestoIstovara = value;
        }

        public string GetKupac()
        {
            return Kupac;
        }

        public void SetKupac(string value)
        {
            Kupac = value;
        }

        public string GetUplatnica_nalog()
        {
            return Uplatnica_nalog;
        }

        public void SetUplatnica_nalog(string value)
        {
            Uplatnica_nalog = value;
        }

        public Popratnica(int broj, string datumOtpremanja, string vrijemeOtpremanja, string oIBOtpremnika, string regOznakaKamiona, string regOznakaPrikolice, string imePrezVozaca, string mjestoUtovara, string mjestoIstovara, string kupac, string uplatnica_nalog)
        {
            this.broj = broj;
            this.datumOtpremanja = datumOtpremanja;
            this.vrijemeOtpremanja = vrijemeOtpremanja;
            OIBOtpremnika = oIBOtpremnika;
            this.regOznakaKamiona = regOznakaKamiona;
            this.regOznakaPrikolice = regOznakaPrikolice;
            ImePrezVozaca = imePrezVozaca;
            MjestoUtovara = mjestoUtovara;
            MjestoIstovara = mjestoIstovara;
            Kupac = kupac;
            Uplatnica_nalog = uplatnica_nalog;
        }

        public Popratnica()
        {
        }
    }
}
