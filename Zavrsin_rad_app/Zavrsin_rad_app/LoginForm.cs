﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;

namespace Zavrsin_rad_app
{
    public partial class LoginForm : Form
    {

        MojServer connectionServer = new MojServer();
        MojServer def = new MojServer();
        MojServer temp = new MojServer();


        public LoginForm()
        {
            InitializeComponent();
            this.Width = 560;
            this.Height = 304;

            pb_login_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button;
            pb_minimizeLogin.Image = Zavrsin_rad_app.Properties.Resources.minimize_button2;
            pb_settings.Image = Zavrsin_rad_app.Properties.Resources.settings_button2;
            pb_icon.Image = Zavrsin_rad_app.Properties.Resources.wood_icon;

            def.SetServer("192.168.0.107");
            def.SetServer2("DESKTOP-EHCSP3M\\BERNARDSQL");
            def.SetPort("49172");
            def.SetBazaPod("BazaPodatakaZavrsniRad");
            def.SetLocal(false);

            temp.SetServer(def.GetServer());
            temp.SetServer2(def.GetServer2());
            temp.SetPort(def.GetPort());
            temp.SetBazaPod(def.GetBazaPod());
            temp.SetLocal(def.GetLocal());

            connectionServer.SetServer("tcp:" + def.GetServer() + "," + def.GetPort());
            connectionServer.SetBazaPod(def.GetBazaPod());
            connectionServer.SetLocal(def.GetLocal());
        }  

        private void btn_prijaviSe_Click(object sender, EventArgs e)
        {
            connectionServer.SetUser(tb_korImePrijava.Text);
            connectionServer.SetPassword(tb_lozinkaPrijava.Text);
            connectionServer.SetConString("Data Source=" + connectionServer.GetServer() + ";Initial Catalog=" + connectionServer.GetBazaPod() + ";User ID=" + connectionServer.GetUser() + ";Password=" + connectionServer.GetPassword()+"");

            try
            {
                SqlConnection conect = new SqlConnection(connectionServer.GetConString());
                conect.Open();
                conect.Close();

                this.Hide();
                Form_Administrator newForm = new Form_Administrator();
                newForm.loginConString = connectionServer.GetConString();                    
                newForm.Show();
            }
            catch (Exception)
            {           
                MessageBox.Show("Provjerite korisničko ime i lozinku, te internet vezu i postavke povezivanja.","Neuspješan pokušaj povezivanja sa serverom");
                tb_lozinkaPrijava.Text = "";
            }
            
        }

        private void pb_login_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void pb_minimizeLogin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void pb_settings_Click(object sender, EventArgs e)
        {
            pnl_login.Visible = false;

            pnl_settings.Visible = true;
            pnl_settings.Location = new Point(0, 54);
            pb_settings.Visible = false;

            this.Height = 391;


            if (connectionServer.GetLocal() == false)
            {
                rb_internet.Checked = true;
                rb_lokal.Checked = false;

                lb_txt_server_int.Enabled = true;
                lb_txt_port_int.Enabled = true;
                lb_txt_baza_int.Enabled = true;
                tb_internet_server.Enabled = true;
                tb_internet_port.Enabled = true;
                tb_internet_bazaPod.Enabled = true;

                lb_txt_server_local.Enabled = false;
                lb_txt_baza_local.Enabled = false;
                tb_lokal_server.Enabled = false;
                tb_lokal_bazaPod.Enabled = false;

            }
            else
            {
                rb_lokal.Checked = true;
                rb_internet.Checked = false;

                lb_txt_server_int.Enabled = false;
                lb_txt_port_int.Enabled = false;
                lb_txt_baza_int.Enabled = false;
                tb_internet_server.Enabled = false;
                tb_internet_port.Enabled = false;
                tb_internet_bazaPod.Enabled = false;

                lb_txt_server_local.Enabled = true;
                lb_txt_baza_local.Enabled = true;
                tb_lokal_server.Enabled = true;
                tb_lokal_bazaPod.Enabled = true;
            }

            tb_internet_server.Text = temp.GetServer();
            tb_internet_port.Text = temp.GetPort();
            tb_internet_bazaPod.Text = temp.GetBazaPod();

            tb_lokal_server.Text = temp.GetServer2();
            tb_lokal_bazaPod.Text = temp.GetBazaPod();
        }

        private void pb_login_exit_MouseEnter(object sender, EventArgs e)
        {
            pb_login_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button4;
        }
        private void pb_login_exit_MouseLeave(object sender, EventArgs e)
        {
            pb_login_exit.Image = Zavrsin_rad_app.Properties.Resources.exit_button;
        }

        private void pb_minimizeLogin_MouseEnter(object sender, EventArgs e)
        {
            pb_minimizeLogin.Image = Zavrsin_rad_app.Properties.Resources.minimize_button;

        }
        private void pb_minimizeLogin_MouseLeave(object sender, EventArgs e)
        {
            pb_minimizeLogin.Image = Zavrsin_rad_app.Properties.Resources.minimize_button2;

        }


        //SETTINGS

        private void pb_settings_MouseEnter(object sender, EventArgs e)
        {
            pb_settings.Image = Zavrsin_rad_app.Properties.Resources.settings_button;

        }
        private void pb_settings_MouseLeave(object sender, EventArgs e)
        {
            pb_settings.Image = Zavrsin_rad_app.Properties.Resources.settings_button2;

        }

        private void btn_vratiPočetnePostavke_Click(object sender, EventArgs e)
        {          
            connectionServer.SetServer("tcp:" + def.GetServer() + "," + def.GetPort());
            connectionServer.SetBazaPod(def.GetBazaPod());
            connectionServer.SetLocal(def.GetLocal());

            tb_internet_server.Text = def.GetServer();
            tb_internet_port.Text = def.GetPort();
            tb_internet_bazaPod.Text = def.GetBazaPod();

            tb_lokal_server.Text = def.GetServer2();
            tb_lokal_bazaPod.Text = def.GetBazaPod();

            rb_internet.Checked = true;
            rb_lokal.Checked = false;
        }
        private void btn_spremiSettings_Click(object sender, EventArgs e)
        {
            if (connectionServer.GetLocal() == false)
            {
                connectionServer.SetServer("tcp:"+tb_internet_server.Text+","+tb_internet_port.Text);
                connectionServer.SetBazaPod(tb_internet_bazaPod.Text);
            }
            else
            {
                connectionServer.SetServer(tb_lokal_server.Text);
                connectionServer.SetBazaPod(tb_lokal_bazaPod.Text);
            }

            temp.SetServer(tb_internet_server.Text);
            temp.SetPort(tb_internet_port.Text);
            temp.SetBazaPod(tb_internet_bazaPod.Text);
            temp.SetServer2(tb_lokal_server.Text);
            if (rb_lokal.Checked == true) temp.SetLocal(true);
            else temp.SetLocal(false);

            pnl_settings.Visible = false;
            pnl_login.Visible = true;
            
            pb_settings.Visible = true;
            this.Height = 304;

        }
        private void btn_odustaniLogin_Click(object sender, EventArgs e)
        {
            pnl_settings.Visible = false;
            pnl_login.Visible = true;

            pb_settings.Visible = true;
            this.Height = 304;
        }     

        private void rb_internet_Click(object sender, EventArgs e)
        {
            connectionServer.SetLocal(false);

            lb_txt_server_int.Enabled = true;
            lb_txt_port_int.Enabled = true;
            lb_txt_baza_int.Enabled = true;
            tb_internet_server.Enabled = true;
            tb_internet_port.Enabled = true;
            tb_internet_bazaPod.Enabled = true;

            lb_txt_server_local.Enabled = false;
            lb_txt_baza_local.Enabled = false;
            tb_lokal_server.Enabled = false;
            tb_lokal_bazaPod.Enabled = false;
        }
        private void rb_lokal_Click(object sender, EventArgs e)
        {
            connectionServer.SetLocal(true);

            lb_txt_server_int.Enabled = false;
            lb_txt_port_int.Enabled = false;
            lb_txt_baza_int.Enabled = false;
            tb_internet_server.Enabled = false;
            tb_internet_port.Enabled = false;
            tb_internet_bazaPod.Enabled = false;

            lb_txt_server_local.Enabled = true;
            lb_txt_baza_local.Enabled = true;
            tb_lokal_server.Enabled = true;
            tb_lokal_bazaPod.Enabled = true;
        }

   
    }
}
